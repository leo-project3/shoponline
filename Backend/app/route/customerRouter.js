const express = require("express")
const router = express.Router();

const {
  createCustomerMiddleware,
  createCustomerNoOrderMiddleware,
  getCustomerMiddleware,
  getCustomerByIdMiddleware,
  updateCustomerMiddleware,
  deleteCustomerMiddleware
} = require("../middleware/customerMiddleware")

const {
  getAllCustomer,
  createCustomer,
  getOrderCustomer,
  getCustomerById,
  updateCustomerById,
  createCustomerNoOrder,
  deleteCustomerById
} = require("../controller/customerController")

const { verifyToken, checkUser } = require("../middleware/authMiddleware");

router.get("/customer", getCustomerMiddleware, getAllCustomer)
router.get("/order/customer/:order", getCustomerByIdMiddleware, getOrderCustomer)
router.post("/customer", createCustomerMiddleware, createCustomer)
router.post("/customernoorder", createCustomerNoOrderMiddleware, createCustomerNoOrder)
router.get("/customer/:id", getCustomerByIdMiddleware, getCustomerById)
router.put("/customer/:id", [updateCustomerMiddleware], updateCustomerById)
router.delete("/customer/:id", [deleteCustomerMiddleware, verifyToken, checkUser], deleteCustomerById)

module.exports = router