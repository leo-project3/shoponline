import { Container, Grid } from "@mui/material";
import FooterContent from "./footercontent/footerContent";
const Footer = () => {
  return (
    <Grid container className="bg-light" sx={{ marginTop: { xs: '100px', sm: "200px" } }}>
      <div className="container p-5">
        <Grid spacing={6} container paddingTop="50px">
          <FooterContent />
        </Grid>
      </div>
    </Grid>
  );
};

export default Footer;
