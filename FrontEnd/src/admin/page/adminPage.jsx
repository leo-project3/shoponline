import { AdminTable } from "./adminTablePage";
import { OrderManagementPage } from "./orderManagementPage";
import { ProductManagementPage } from "./productManagementPage";
import { DashboardPage } from "./dashboardPage";
import { CreatePage } from "./createPage";
import { useState } from "react";
import { Navigate, Route, Routes } from "react-router-dom";
import { CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "../../theme";
import Sidebar from "../component/global/sideBar";
import Topbar from "../component/global/topBar";
export const AdminPage = ({ admin }) => {
  const [theme, colorMode] = useMode();
  const [isSidebar, setIsSidebar] = useState(true);
  const ProtectedRoute = ({ children }) => {
    if (admin == false) {
      return <Navigate to="/" />;
    } else {
      return children;
    }
  };
  return (
    <>
      <ColorModeContext.Provider value={colorMode}>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <div className="app">
            <Sidebar isSidebar={isSidebar} />
            <main className="content">
              <Topbar setIsSidebar={setIsSidebar} />
              <Routes>
                <Route
                  path="dashboard"
                  element={
                    <ProtectedRoute>
                      <DashboardPage />
                    </ProtectedRoute>
                  }
                />

                <Route
                  path="customer"
                  element={
                    <ProtectedRoute>
                      <AdminTable />
                    </ProtectedRoute>
                  }
                />

                <Route
                  path="order"
                  element={
                    <ProtectedRoute>
                      <OrderManagementPage />
                    </ProtectedRoute>
                  }
                />

                <Route
                  path="product"
                  element={
                    <ProtectedRoute>
                      <ProductManagementPage />
                    </ProtectedRoute>
                  }
                />

                <Route
                  path="create"
                  element={
                    <ProtectedRoute>
                      <CreatePage />
                    </ProtectedRoute>
                  }
                />
              </Routes>
            </main>
          </div>
        </ThemeProvider>
      </ColorModeContext.Provider>
    </>
  );
};
