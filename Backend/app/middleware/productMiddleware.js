const createProductMiddleware = (req, res, next) => {
  console.log("Create Product Middleware")
  next();
}
const getProductMiddleware = (req, res, next) => {
  console.log("Get Product Middleware")
  next();
}
const getProductByIdMiddleware = (req, res, next) => {
  console.log("Get Product by Id Middleware")
  next();
}
const updateProductMiddleware = (req, res, next) => {
  console.log("Update Product Middleware")
  next();
}
const deleteProductMiddleware = (req, res, next) => {
  console.log("Delete Product Middleware")
  next();
}

module.exports = {
  createProductMiddleware,
  getProductMiddleware,
  getProductByIdMiddleware,
  updateProductMiddleware,
  deleteProductMiddleware
}