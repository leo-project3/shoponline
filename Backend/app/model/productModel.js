const mongoose = require("mongoose")
const Schema = mongoose.Schema;

const productSchema = new Schema({
  _id: mongoose.Types.ObjectId,
  name: {
    type: String,
    unique: true,
    required: true
  },
  description: {
    type: String,
    required: false
  },
  type: {
    type: mongoose.Types.ObjectId,
    type: String,
    ref: "productType"
  },
  imageUrl:{
    type: mongoose.Types.ObjectId,
    ref: "image",
    required: true
  },
  buyPrice: {
    type: Number,
    required: true
  },
  promotionPrice: {
    type: Number,
    required: true
  },
  amount: {
    type: Number,
    default: 0
  }, 
  isDelete: {
    type: Boolean,
    default: false
  }
}, {timestamps: true})

module.exports = mongoose.model("product", productSchema)