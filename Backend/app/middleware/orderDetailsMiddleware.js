const createOrderDetailsMiddleware = (req, res, next) => {
  console.log("Create OrderDetail Middleware")
  next();
}
const getOrderDetailsMiddleware = (req, res, next) => {
  console.log("Get OrderDetail Middleware")
  next();
}
const getOrderDetailsByIdMiddleware = (req, res, next) => {
  console.log("Get OrderDetail by Id Middleware")
  next();
}
const updateOrderDetailsMiddleware = (req, res, next) => {
  console.log("Update OrderDetail Middleware")
  next();
}
const deleteOrderDetailsMiddleware = (req, res, next) => {
  console.log("Delete OrderDetail Middleware")
  next();
}

module.exports = {
  createOrderDetailsMiddleware,
  getOrderDetailsMiddleware,
  getOrderDetailsByIdMiddleware,
  updateOrderDetailsMiddleware,
  deleteOrderDetailsMiddleware
}