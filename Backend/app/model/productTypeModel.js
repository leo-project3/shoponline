const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const productTypeSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    name: {
      type: String,
      unique: true,
      required: true,
    },
    description: {
      type: String,
      required: false
    }
}, { timestamps: true })

module.exports = mongoose.model("productType", productTypeSchema)

