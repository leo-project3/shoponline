const express = require("express")
const router = express.Router();

const { verifyToken, checkUser } = require("../middleware/authMiddleware");
const { getImage, createImage } = require("../controller/imageController");

router.get("/upload-image/", getImage)
router.post("/upload-image", createImage)

module.exports = router