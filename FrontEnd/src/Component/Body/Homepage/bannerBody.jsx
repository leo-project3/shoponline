import { Grid, Typography } from "@mui/material";
import LocalShippingIcon from "@mui/icons-material/LocalShipping";
import MoneyOffIcon from "@mui/icons-material/MoneyOff";
import AutorenewIcon from "@mui/icons-material/Autorenew";

//Banner for homepage
export const Banner = () => {
  return (
    <Grid
      container
      sx={{
        color: "White",
        display: "flex",
        justifyContent: "space-around",
        alignItems: "center",
      }}
    >
      <Grid
        item
        xs={12}
        md={3}
        sx={{ display: "flex", alignItems: "center", justifyContent: "center" }}
      >
        <LocalShippingIcon sx={{ margin: "5px" }} fontSize="large" />
        <Typography variant="body" fontWeight="bold" component="div">
          Deliver within 7 days
        </Typography>
      </Grid>
      <Grid
        item
        xs={12}
        md={5}
        sx={{ display: "flex", alignItems: "center", justifyContent: "center" }}
      >
        <MoneyOffIcon sx={{ margin: "5px" }} fontSize="large" />
        <Typography
          variant="body"
          fontWeight="bold"
          component="div"
          align="center"
        >
          Free Delivery with order above 100.000.000VND
        </Typography>
      </Grid>
      <Grid
        item
        xs={12}
        md={4}
        sx={{ display: "flex", alignItems: "center", justifyContent: "center" }}
      >
        <AutorenewIcon sx={{ margin: "5px" }} fontSize="large" />
        <Typography variant="body" fontWeight="bold" component="div">
          Do not like it ? Return in 30 days
        </Typography>
      </Grid>
    </Grid>
  );
};
