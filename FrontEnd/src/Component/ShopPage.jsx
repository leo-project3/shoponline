import Header from "./Header/header";
import Body from "./Body/Homepage/MainBody";
import Footer from "./Footer/footer";

export const ShopPage = () => {
  return (
    <div>
      <Header />
      <Body />
      <Footer />
    </div>
  );
};
