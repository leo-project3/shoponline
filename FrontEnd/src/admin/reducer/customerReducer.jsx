import { ERROR } from "../../contants/data";
import { DELETE } from "../contants/data";

const initialState = {
  remove: 0,
  error: 0,
};

export const customerReducer = (state = initialState, action) => {
  switch (action.type) {
    case DELETE:
      state.remove = action.status;
      break;
    case ERROR:
      state.error = action.error;
      break;
    default:
      break;
  }
  return { ...state };
};
