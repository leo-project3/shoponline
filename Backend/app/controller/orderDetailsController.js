const orderDetailsModel = require("../model/orderDetailsModel");
const mongoose = require("mongoose")
const orderModel = require("../model/orderModel");
const productModel = require("../model/productModel");

const createOrderDetails = async (req, res) => {
  //const orderId = req.params.orderId
  const {
    productId,
    quantity
  } = req.body
  try {
    const response = await orderDetailsModel.create({
      _id: new mongoose.Types.ObjectId,
      product: productId,
      quantity: quantity
    })
    return res.status(201).json({
      message: "Create successfully",
      orderdetails: response
    })
  } catch (error) {
    res.status(500).json({
      message: "Error" + error.message
    })
  }
}

const getAllOrderDetails = async (req, res) => {
  try {
    const result = await orderDetailsModel.find();
    return res.status(200).json({
      message: "Get order detail successfully",
      orderDetails: result
    })
  } catch (error) {
    return res.status(500).json({
      message: "Error" + error.message
    })
  }
}

const getOrderDetailsById = async (req, res) => {
  const id = req.params.id
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(404).json({
      message: "Id is not valid"
    })
  }
  try {
    const result = await orderDetailsModel.findById(id)
    if (result) {
      return res.status(200).json({
        message: `Get orderdetail by id ${id}`,
        orderDetails: result
      })
    } else {
      return res.status(400).json({
        message: "Id is not found"
      })
    }
  } catch (error) {
    return res.status(500).json({
      message: "Error" + error.message
    })
  }
}

const updateOrderDetailsById = async (req, res) => {
  const id = req.params.id
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(404).json({
      message: "Id is not valid"
    })
  }
  const {
    quantity
  } = req.body

  try {
    var updateOrderDetails = {};
    if (quantity) {
      updateOrderDetails.quantity = quantity
    }
    updateOrderDetails.expires = null
    const result = await orderDetailsModel.findByIdAndUpdate(id, updateOrderDetails)
    if (result) {
      return res.status(200).json({
        message: `Update orderdetail by id ${id}`,
        orderDetails: result
      })
    } else {
      return res.status(400).json({
        message: "Id is not found"
      })
    }

  } catch (error) {
    res.status(500).json({
      message: "Error" + err.message
    })
  }
}

const deleteOrderDetailsById = async (req, res) => {
  const id = req.params.id;
  //  const orderId = req.params.orderId
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(404).json({
      message: "Id is not valid"
    })
  }
  /*  if(!mongoose.Types.ObjectId.isValid(orderId)){
      return res.status(404).json({
        message: "Id is not valid"
      })
    }*/
  try {
    const result = await orderDetailsModel.findByIdAndDelete(id);
    if (result) {
      // await orderModel.findByIdAndUpdate(orderId, {
      // $pull: { orderDetails: id }
      //})
      return res.status(200).json({
        message: `Delete orderdetail by id ${id}`
      })
    } else {
      return res.status(400).json({
        message: "Id is not found"
      })
    }
  } catch (error) {
    return res.status(500).json({
      message: "Error" + error.message
    })
  }
}
module.exports = {
  createOrderDetails,
  getAllOrderDetails,
  getOrderDetailsById,
  updateOrderDetailsById,
  deleteOrderDetailsById
}