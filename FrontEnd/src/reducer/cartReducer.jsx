import { ADD_TO_CART } from "../contants/data";

const initialState = {
  cart: [],
  cartItemQuantity: 0,
};

//Control product in cart
export const cartReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TO_CART:
      state.cart = action.payload;
      state.cartItemQuantity = action.payload.length;
      break;
    default:
      break;
  }
  return { ...state };
};
