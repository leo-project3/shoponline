import {
  Container,
  Grid,
  TextField,
  Typography,
  Button,
  Select,
  InputLabel,
} from "@mui/material";
import axios from "axios";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { SelectLocation } from "../Global/locationSelect";
import { getExistedUser } from "../../../action/getExistedUser";
export const CheckOutInformation = ({ setOpen, setErrorMsg, setOpenToast }) => {
  const dispatch = useDispatch();
  const { order, customer, orderDetail } = useSelector(
    (reduxData) => reduxData.checkOutReducer,
  );
  const user = sessionStorage.getItem("User");
  useEffect(() => {
    dispatch(getExistedUser(user));
  }, []);
  const [fullname, setName] = useState("");
  const [phone, setPhone] = useState("");
  const [email, setEmail] = useState("");
  const [address, setAddress] = useState("");
  const [city, setCity] = useState("");
  const [district, setDistrict] = useState("");
  const [ward, setWard] = useState("");
  const [error, setError] = useState({});
  const validateError = {};
  //Validate data for new customer
  const validateData = () => {
    const isValidEmail = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g.test(email);
    const isValidPhone = /(84|0[3|5|7|8|9])+([0-9]{8})\b/g.test(phone);
    //Validate input value
    if (!fullname.trim()) {
      validateError.fullname = "Fullname is required";
    }
    if (!email.trim()) {
      validateError.email = "Email is required";
    } else if (!isValidEmail) {
      validateError.email = "Email is invalid";
    }
    if (!phone.trim() || isNaN(phone)) {
      validateError.phone = "Phone number is required and must be the number";
    } else if (!isValidPhone) {
      validateError.phone = "Phone is invalid";
    }
    if (!address.trim()) {
      validateError.address = "Address is required";
    }
    if (!city) {
      validateError.city = "City is required";
    }
    if (!district) {
      validateError.district = "District is required";
    }
    if (!ward) {
      validateError.ward = "Ward is required";
    }
    setError(validateError);
  };
  //Check out function with new customer
  const onBtnCheckOutClick = async () => {
    validateData();
    const customerObj = {
      fullName: fullname,
      phone: phone,
      email: email,
      address: address,
      city: city,
      district: district,
      ward: ward,
      userId: user,
      orders: order,
    };
    if (Object.keys(validateError).length === 0) {
      try {
        const response = await axios.post(
          process.env.REACT_APP_FETCH_URL + "/api/customer",
          customerObj,
        );
        const data = await response.data;
        if (response.status == 201) {
          setOpen(true);
        }
      } catch (error) {
        setErrorMsg(error.response.data.message);
        setOpenToast(true);
      }
    }
  };
  //Check out function with existed customer
  const onBtnCheckOutClick2 = async () => {
    const customerObj = {
      fullName: customer[0].fullName,
      phone: customer[0].phone,
      email: customer[0].email,
      address: customer[0].address,
      city: customer[0].city,
      district: customer[0].district,
      ward: customer[0].ward,
      userId: user,
      orders: order,
    };
    if (Object.keys(validateError).length === 0) {
      try {
        const response = await axios.post(
          process.env.REACT_APP_FETCH_URL + "/api/customer",
          customerObj,
        );
        const data = await response.data;
        if (response.status == 201) {
          console.log(data)
          setOpen(true);
        }
      } catch (error) {
        setErrorMsg(error.response.data.message);
        setOpenToast(true);
      }
    }
  };
  return (
    <Grid
      item
      sx={{ marginTop: { xs: "10px" }, width: { md: '35%', xs: "100%" }, }}
      style={{
        display: "flex",
        flexDirection: "Column",
        backgroundColor: "white",
        maxWidth: "600px",
        minWidth: "320px",
        borderRadius: "20px",
        padding: "10px",
      }}
    >
      <Typography align="center" variant="h5" component="div">
        <b>Check Out Information</b>
      </Typography>
      {Object.keys(customer).length != 0 ? (
        <Container
          style={{
            textAlign: "center",
            maxWidth: "70%",
            minWidth: "250px",
          }}
        >
          <TextField
            disabled
            value={customer[0].fullName}
            sx={{ mt: 1, mb: 1 }}
            fullWidth
          ></TextField>
          <TextField disabled value={customer[0].phone} fullWidth></TextField>
          <TextField
            disabled
            value={customer[0].email}
            sx={{ mt: 1, mb: 1 }}
            fullWidth
          ></TextField>
          <TextField
            disabled
            value={customer[0].address}
            sx={{ mb: 1 }}
            fullWidth
          ></TextField>
          <TextField
            disabled
            value={customer[0].city}
            sx={{ mb: 1 }}
            fullWidth
          ></TextField>
          <TextField
            disabled
            value={customer[0].district}
            sx={{ mb: 1 }}
            fullWidth
          ></TextField>
          <TextField
            disabled
            value={customer[0].ward}
            sx={{ mb: 1 }}
            fullWidth
          ></TextField>
          <Button
            variant="contained"
            onClick={onBtnCheckOutClick2}
            color="success"
          >
            Check Out
          </Button>
        </Container>
      ) : (
        <Container
          style={{
            textAlign: "center",
            maxWidth: "70%",
            minWidth: "250px",
          }}
        >
          <TextField
            label="Full Name"
            value={fullname || ""}
            onChange={(e) => setName(e.target.value)}
            sx={{ mt: 1, mb: 1 }}
            fullWidth
          ></TextField>
          {error.fullname && (
            <span style={{ color: "red" }}>{error.fullname}</span>
          )}
          <TextField
            label="Phone"
            value={phone || ""}
            onChange={(e) => setPhone(e.target.value)}
            fullWidth
          ></TextField>
          {error.phone && <span style={{ color: "red" }}>{error.phone}</span>}
          <TextField
            label="Email"
            value={email || ""}
            onChange={(e) => setEmail(e.target.value)}
            sx={{ mt: 1, mb: 1 }}
            fullWidth
          ></TextField>
          {error.email && <span style={{ color: "red" }}>{error.email}</span>}
          <TextField
            label="Address"
            value={address || ""}
            onChange={(e) => setAddress(e.target.value)}
            fullWidth
          ></TextField>
          {error.address && (
            <span style={{ color: "red" }}>{error.address}</span>
          )}
          <SelectLocation
            error={error}
            setCity={setCity}
            setDistrict={setDistrict}
            city={city}
            ward={ward}
            setWard={setWard}
            district={district}
          />
          <Button
            variant="contained"
            onClick={onBtnCheckOutClick}
            color="success"
          >
            Check Out
          </Button>
        </Container>
      )}
    </Grid>
  );
};
