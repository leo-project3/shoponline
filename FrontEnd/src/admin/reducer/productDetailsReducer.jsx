import { ERROR } from "../../contants/data";
import { DELETE, GET_PRODUCT_DETAILS, PENDING } from "../contants/data";

const initialState = {
  product: [],
  orderDetails: [],
  productsDetails: [],
};

export const productDetailsReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_PRODUCT_DETAILS:
      state.product = action.product;
      state.orderDetails = action.orderDetails;
      break;
    default:
      break;
  }
  return { ...state };
};
