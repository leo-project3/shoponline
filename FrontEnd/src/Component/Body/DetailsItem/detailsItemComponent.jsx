import {
  Button,
  Grid,
  Rating,
  Typography,
  OutlinedInput,
  IconButton,
  InputAdornment,
  Fade,
} from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import Box from "@mui/material/Box";
import Snackbar from "@mui/material/Snackbar";
import AddIcon from "@mui/icons-material/Add";
import Alert from "@mui/material/Alert";
import RemoveIcon from "@mui/icons-material/Remove";
import { addToCart } from "../../../action/addToCartAction";
import { useLocation, useNavigate } from "react-router-dom";
import { ShopToast } from "../../../ToastMessagePopup/toastShop";
export const DetailItem = () => {
  const navigate = useNavigate();
  const { item } = useSelector((reduxData) => reduxData.detailsReducer);
  const [quantity, setQuantity] = useState(1);
  const [open, setOpen] = useState(false);
  const [messageSuccess, setMessageSuccess] = useState("");
  const [messageError, setMessageError] = useState("");
  const [image, setImage] = useState(0)

  const dispatch = useDispatch();
  //update quantity when click on + - symbols
  const onBtnAdd = () => {
    if (quantity >= item.amount) {
      return;
    }
    setQuantity(quantity + 1);
  };
  const onBtnMinus = () => {
    if (quantity <= 0) {
      return;
    }
    setQuantity(quantity - 1);
  };

  //Add to cart function
  const onBtnClickAddtoCart = (item) => {
    let item2;
    if (typeof location.state == "object") {
      item2 = location.state;
    } else {
      item2 = item;
    }
    if (sessionStorage.getItem("User")) {
      dispatch(addToCart(item2, quantity));
      setMessageSuccess("Add Product to Cart");
      setOpen(true);
    } else {
      setMessageError("Please Login");
      setOpen(true);
      setTimeout(() => navigate("/login"), 3000);
    }
    /* let productCart = JSON.parse(localStorage.getItem("cart"))
    let productQuantity
    
      if(!productCart){
        productQuantity = quantity
        item.quantity = productQuantity
        cart.push(item)
        localStorage.setItem("cart", JSON.stringify(cart))
     }else if (productCart.length != 0){
     
      productCart.map((product) => {
        if (product._id == item._id){
        productQuantity = product.quantity + quantity
        product.quantity = productQuantity
        localStorage.setItem("cart", JSON.stringify(productCart))

      } else if(product._id !== item._id){
        productQuantity = quantity
        item.quantity = productQuantity
        productCart.push(item)
        localStorage.setItem("cart", JSON.stringify(productCart))
      }
      })
    }
    //
    }*/
  };

  const location = useLocation();
  return (
    <>
      <Box sx={{ width: 500 }}>
        <ShopToast
          messageSuccess={messageSuccess}
          messageError={messageError}
          setOpen={setOpen}
          open={open}
        />
      </Box>
      <Grid
        container
        sx={{
          display: "flex",
          justifyContent: "space-around",
          alignItems: "center",
        }}
      >
        <Grid
          item
          md={6}
          sx={{
            marginTop: "20px",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            flexDirection: "column"
          }}
        >
          <img
            src={
              typeof location.state == "object"
                ? process.env.REACT_APP_FETCH_URL + "/upload/" + location.state.imageUrl.image[image]
                : item.length != 0
                  ? process.env.REACT_APP_FETCH_URL + "/upload/" + item.imageUrl.image[image]
                  : null
            }
            alt="product-img"
            style={{ width: "100%", maxWidth: "400px", minWidth: "275px", borderRadius: "10px", height: '100%', maxHeight: "370px", minHeight: "200px" }}
          />
          <Box textAlign='center'>
            {(item.length != 0 && typeof location.state != "object") ? item.imageUrl.image.map((img, index) => (
              <img key={img} onClick={() => setImage(index)} src={process.env.REACT_APP_FETCH_URL + "/upload/" + img} style={{ margin: "3px", boxShadow: "2px 2px 5px black" }} width='70px' height='70px' />
            )) : null}
            {typeof location.state == "object" ? location.state.imageUrl.image.map((img, index) => (
              <img key={img} onClick={() => setImage(index)} src={process.env.REACT_APP_FETCH_URL + "/upload/" + img} style={{ margin: "3px", boxShadow: "2px 2px 5px black" }} width='70px' height='70px' />
            )) : null}
          </Box>
        </Grid>
        <Grid
          item
          md={6}
          rowGap="0.2rem"
          sx={{
            display: "flex",
            justifyContent: "center",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Typography fontSize={25}>
            <b>
              {typeof location.state == "object"
                ? location.state.name
                : item.name}
            </b>
          </Typography>
          <Rating value={5} readOnly />
          <Typography>
            Category:{" "}
            {typeof location.state == "object"
              ? location.state.type
              : item.type}
          </Typography>
          <Typography>
            Price:{" "}
            {typeof location.state == "object"
              ? location.state.promotionPrice.toLocaleString()
              : item.length != 0
                ? item.promotionPrice.toLocaleString()
                : null}{" "}
            VND
          </Typography>
          <Typography>
            <b>
              {item.amount > 0
                ? `Stock: ${typeof location.state == "object" ? location.state.amount : item.amount}`
                : "Out out Stock"}
            </b>
          </Typography>
          <OutlinedInput
            sx={{ textAlign: "center", width: "142px" }}
            size="small"
            type="text"
            onChange={(e) => setQuantity(e.target.value)}
            value={quantity}
            endAdornment={
              <InputAdornment position="end">
                <IconButton onClick={onBtnAdd}>
                  <AddIcon />
                </IconButton>
              </InputAdornment>
            }
            startAdornment={
              <InputAdornment position="start">
                <IconButton onClick={onBtnMinus}>
                  <RemoveIcon />
                </IconButton>
              </InputAdornment>
            }
          />
          <Button
            variant="contained"
            color="error"
            sx={{ marginTop: "10px" }}
            onClick={() => onBtnClickAddtoCart(item)}
            disabled={item.amount == 0}
          >
            Add to Cart
          </Button>
        </Grid>
      </Grid>
    </>
  );
};
