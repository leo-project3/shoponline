import {
  CircularProgress,
  Container,
  Grid,
  Typography,
  TableCell,
  TableRow,
  TableContainer,
  Table,
  TableHead,
  TableBody,
  Button,
  Paper,
  Box,
  Tooltip,
} from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { getProduct, getProductId } from "../../action/getOrderAction";
import { useEffect, useState } from "react";
import EditNoteIcon from "@mui/icons-material/EditNote";
import DeleteIcon from "@mui/icons-material/DeleteOutlined";
import SaveIcon from "@mui/icons-material/Save";
import CancelIcon from "@mui/icons-material/Close";
import EditIcon from "@mui/icons-material/Edit";
import AddIcon from "@mui/icons-material/Add";
import { tokens } from "../../../theme";
import {
  DataGrid,
  GridToolbar,
  GridActionsCellItem,
  GridRowModes,
  GridRowEditStopReasons,
  GridToolbarContainer,
} from "@mui/x-data-grid";
import { useTheme } from "@mui/material";
import { DeleteProductModal } from "../modal/deleteModal";
import { UpdateProductModal } from "../modal/updateModal";
import { updateProduct } from "../../action/updateAction";
import { CreateProductModal } from "../modal/createModal";
import Header from "../global/header";
import axios from "axios";
import { ShopToast } from "../../../ToastMessagePopup/toastShop"

function EditToolbar(props) {
  const { openCreateModal, setOpenCreateModal } = props;

  const handleClick = () => {
    setOpenCreateModal(true);
  };

  return (
    <GridToolbarContainer>
      <Button color="primary" startIcon={<AddIcon />} onClick={handleClick}>
        Add Product
      </Button>
    </GridToolbarContainer>
  );
}

export const ProductManagement = () => {
  const theme = useTheme();
  const colors = tokens(theme.palette.mode);

  const { products, pending, page, noPage, limit } = useSelector(
    (reduxData) => reduxData.tableReducer,
  );

  const [openDeleteModal, setOpenDeleteModal] = useState(false);
  const [openUpdateModal, setOpenUpdateModal] = useState(false);
  const [openCreateModal, setOpenCreateModal] = useState(false);
  const [messageError, setMessageError] = useState("")
  const [messageSuccess, setMessageSuccess] = useState("")

  const [paramProduct, setPProduct] = useState("");
  const [product, setProduct] = useState({});
  const [rowModesModel, setRowModesModel] = useState({});
  const [openToast, setOpenToast] = useState(false);

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getProduct());
  }, []);
  const onBtnDeleteClick = (index) => {
    setPProduct(products[index]);
    setOpenDeleteModal(true);
  };
  const handleFullyUpdateClick = (id) => () => {
    //dispatch(getProductId(id))
    /* setOpenUpdateModal(true) */
    axios(process.env.REACT_APP_FETCH_URL + "/api/product/" + id).then(
      (data) => {
        /*  setValues({
         name: data.data.product.name
        }) */
        setProduct(data.data.product);
      },
    );
    setOpenUpdateModal(true);
    // }
  };
  const handleRowEditStop = (params, event) => {
    if (params.reason === GridRowEditStopReasons.rowFocusOut) {
      event.defaultMuiPrevented = true;
    }
  };

  const handleEditClick = (id) => () => {
    setRowModesModel({ ...rowModesModel, [id]: { mode: GridRowModes.Edit } });
  };

  const handleCancelClick = (id) => () => {
    setRowModesModel({
      ...rowModesModel,
      [id]: { mode: GridRowModes.View, ignoreModifications: true },
    });
  };

  const handleSaveClick = (id) => () => {
    setRowModesModel({ ...rowModesModel, [id]: { mode: GridRowModes.View } });
    setOpenToast(true);
  };

  const handleDeleteClick = (id) => () => {
    setPProduct(id);
    setOpenDeleteModal(true);
  };

  const handleRowModesModelChange = (newRowModesModel) => {
    setRowModesModel(newRowModesModel);
  };

  const processRowUpdate = (newRow) => {
    const updatedRow = { ...newRow, isNew: false };

    dispatch(updateProduct(updatedRow._id, updatedRow, setMessageError, setMessageSuccess, setOpenToast));

    return updatedRow;
  };

  const columns = [
    { field: "_id", headerName: "Id", width: 150, hide: true },
    { field: "name", headerName: "Product Name", flex: 1, editable: true },
    {
      field: "imageUrl",
      headerName: "Image",
      flex: 0.5,
      align: "center",
      renderCell: (params) => (
        <img
          src={process.env.REACT_APP_FETCH_URL + "/upload/" + params.row.imageUrl.image[0]}
          style={{ borderRadius: "10px" }}
          width="100px"
          height="100px"
        />
      ),
    },
    { field: "type", headerName: "Type", editable: true },
    {
      field: "description",
      headerName: "Description",
      flex: 1,
      editable: true,
    },
    {
      field: "buyPrice",
      headerName: "Original Price",
      type: "number",
      flex: 0.5,
      editable: true,
      renderCell: (params) => params.row.buyPrice.toLocaleString(),
    },
    {
      field: "promotionPrice",
      headerName: "Discount Price",
      type: "number",
      flex: 0.5,
      editable: true,
      renderCell: (params) => params.row.promotionPrice.toLocaleString(),
    },
    {
      field: "amount",
      headerName: "Stock",
      flex: 0.5,
      type: "number",
      editable: true,
    },
    {
      field: "actions",
      type: "actions",
      headerName: "Actions",
      flex: 1,
      cellClassName: "actions",
      getActions: ({ id }) => {
        const isInEditMode = rowModesModel[id]?.mode === GridRowModes.Edit;

        if (isInEditMode) {
          return [
            <GridActionsCellItem
              icon={<SaveIcon />}
              label="Save"
              color="inherit"
              onClick={handleSaveClick(id)}
            />,
            <GridActionsCellItem
              icon={<CancelIcon />}
              label="Cancel"
              className="textPrimary"
              onClick={handleCancelClick(id)}
              color="inherit"
            />,
          ];
        }

        return [
          <Tooltip title="Partial Update" placement="top">
            <GridActionsCellItem
              icon={<EditIcon />}
              label="Edit"
              className="textPrimary"
              onClick={handleEditClick(id)}
              color="inherit"
            />
          </Tooltip>,
          <Tooltip title="Fully Update" placement="top">
            <GridActionsCellItem
              icon={<EditNoteIcon />}
              label="Edit All"
              className="textPrimary"
              onClick={handleFullyUpdateClick(id)}
              color="inherit"
            />
          </Tooltip>,
          <Tooltip title="Delete" placement="top">
            <GridActionsCellItem
              icon={<DeleteIcon />}
              label="Delete"
              onClick={handleDeleteClick(id)}
              color="inherit"
            />
          </Tooltip>,
        ];
      },
    },
  ];

  return (
    <>
      {pending ? (
        <Grid item lg={12} md={12} sm={12} xs={12} textAlign="center">
          <CircularProgress color="secondary" />
        </Grid>
      ) : (
        <>
          <Box m="20px">
            <Header title="Product" subtitle="Managing the Products" />
            <Box
              m="0 0 0 0"
              height="75vh"
              sx={{
                "& .MuiDataGrid-root": {
                  border: "none",
                },
                "& .MuiDataGrid-cell": {
                  borderBottom: "none",
                },
                "& .MuiDataGrid-row": {
                  padding: '5px 0 5px 0',
                },
                "& .name-column--cell": {
                  color: colors.greenAccent[300],
                },
                "& .MuiDataGrid-columnHeaders": {
                  backgroundColor: colors.blueAccent[700],
                  borderBottom: "none",
                },
                "& .MuiDataGrid-cellContent": {
                  textWrap: "nowrap"
                },
                "& .MuiDataGrid-virtualScroller": {
                  backgroundColor: colors.primary[400],
                },
                "& .MuiDataGrid-footerContainer": {
                  borderTop: "none",
                  backgroundColor: colors.blueAccent[700],
                },
                "& .MuiCheckbox-root": {
                  color: `${colors.greenAccent[200]} !important`,
                },
                "& .MuiDataGrid-toolbarContainer .MuiButton-text": {
                  color: `${colors.grey[100]} !important`,
                },
                "& .MuiTablePagination-displayedRows": {
                  margin: 0,
                },
                "& .MuiTablePagination-selectLabel": {
                  margin: 0,
                },
              }}
            >
              {/* <Button startIcon={<AddIcon />} color="success">Add category</Button> */}
              <DataGrid
                getRowHeight={() => "auto"}
                columnVisibilityModel={{
                  _id: false,
                }}
                initialState={{
                  pagination: { paginationModel: { pageSize: 5 } },
                }}
                pageSizeOptions={[5, 10, 25]}
                processRowUpdate={processRowUpdate}
                //onCellClick={onBtnCellClick}
                editMode="row"
                getRowId={(products) => products._id}
                rows={products}
                columns={columns}
                rowModesModel={rowModesModel}
                onRowModesModelChange={handleRowModesModelChange}
                onRowEditStop={handleRowEditStop}
                slots={{ toolbar: EditToolbar }}
                slotProps={{
                  toolbar: { openCreateModal, setOpenCreateModal },
                }}
              />
              <UpdateProductModal
                openUpdateModal={openUpdateModal}
                setOpenUpdateModal={setOpenUpdateModal}
                product={product}
                setProduct={setProduct}
                setOpenToast={setOpenToast}
                setMessageError={setMessageError}
                setMessageSuccess={setMessageSuccess}
              />
              <DeleteProductModal
                paramProduct={paramProduct}
                openDeleteModal={openDeleteModal}
                setOpenDeleteModal={setOpenDeleteModal}
                setOpenToast={setOpenToast}
                setMessageError={setMessageError}
                setMessageSuccess={setMessageSuccess}
              />
              <CreateProductModal
                setOpenToast={setOpenToast}
                setMessageError={setMessageError}
                setMessageSuccess={setMessageSuccess}
                openCreateModal={openCreateModal}
                setOpenCreateModal={setOpenCreateModal}
              />
              <ShopToast
                open={openToast}
                setOpen={setOpenToast}
                messageError={messageError}
                messageSuccess={messageSuccess}
              />
            </Box>
          </Box>
        </>
      )}
    </>
  );
};

{
  /*  <>
        <Grid item xs={12} style={{textAlign:'center'}}>
          <Typography variant="h3">
            Product Management
          </Typography>
          </Grid>
          <SelectLimit />
          <Grid item xs={12} mt={4}>
            <TableContainer component={Paper}>
              <Table
              sx={{minWidth: 650}}>
                <TableHead>
                  <TableRow>
                <TableCell>Image</TableCell>
                <TableCell>Name</TableCell>
                <TableCell>Type</TableCell>
                <TableCell>Price</TableCell>
                <TableCell>Stock</TableCell>
                <TableCell>Description</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                {products.map((product, index) => {
                  return (
                    <TableRow key={index}>
                    <TableCell>
                      <img src={product.imageUrl} style={{width: "60px", height:"60px"}} alt="product-img"/>
                      </TableCell>
                  <TableCell>{product.name}</TableCell>
                  <TableCell>{product.type}</TableCell>
                  <TableCell>{product.promotionPrice}</TableCell>
                  <TableCell>{product.amount}</TableCell>
                  <TableCell>
                    <Typography sx={{textOverflow: "ellipsis", width: "200px", overflow: "hidden",}} noWrap>{product.description}</Typography>
                   </TableCell>
                  <TableCell>
                    <EditNoteIcon onClick={() => onBtnUpdateModal(index)} />
                    <UpdateProductModal product={paramProduct} openUpdateModal={openUpdateModal} setOpenUpdateModal={setOpenUpdateModal}/>
                    <DeleteIcon onClick={() => onBtnDeleteClick(index)}/>
                    <DeleteProductModal product={paramProduct} openDeleteModal={openDeleteModal} setOpenDeleteModal={setOpenDeleteModal}/>
                  </TableCell>
                    </TableRow>
                  )
                })}
              
                </TableBody>
              </Table>
            </TableContainer>
           <PaginateComponent onBtnChangePage={onBtnChangePage} page={page} noPage={noPage} />
          </Grid>
      </>*/
}
