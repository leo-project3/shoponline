const mongoose = require("mongoose")
const Schema = mongoose.Schema;

const today = new Date(Date.now())
const orderDetailsSchema = new Schema({
  _id: mongoose.Types.ObjectId,
  product: {
    type: mongoose.Types.ObjectId,
    ref: "product"
  },
  quantity: {
    type: Number,
    default: 0
  },
})
module.exports = mongoose.model("orderDetails", orderDetailsSchema)