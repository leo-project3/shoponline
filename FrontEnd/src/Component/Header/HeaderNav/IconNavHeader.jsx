import NotificationsIcon from "@mui/icons-material/Notifications";
import PersonIcon from "@mui/icons-material/Person";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import { Badge } from "@mui/material";
import React, { useEffect, useState } from "react";
import { NavItem, Nav } from "react-bootstrap";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { AvatarHeader } from "./avatarHeader";
import { IconButton } from '@mui/material';
const IconHeader = () => {
  const navigate = useNavigate();
  const [anchorElUser, setAnchorElUser] = React.useState(null);

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };
  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };
  //const [cartQuantity, setCartQuantity] = useState()
  const { cartItemQuantity } = useSelector(
    (reduxData) => reduxData.cartReducer,
  );
  const cart = JSON.parse(localStorage.getItem("cart"));
  let cartLength;
  if (cart != null) {
    cartLength = cart.length;
  }
  const onBtnClickCart = () => {
    navigate("/cart");
  };
  return (
    <Nav
      className="m-auto"
      style={{ fontSize: 16, display: "flex", alignItems: "center" }}
    >
      <NavItem href="/" className="px-2 custom-text-primary">
        <NotificationsIcon
          sx={{ display: { xs: "none", sm: "inline-flex" } }}
        />
        <IconButton sx={{ marginRight: { xs: "-5px" }, }}>
          <Badge
            color="error"
            variant="standard"
            badgeContent={cart != null || cartLength != 0 ? cartLength : 0}
          >

            <ShoppingCartIcon
              sx={{
                color: "white"
              }}
              onClick={onBtnClickCart}
            />
          </Badge>
        </IconButton>
      </NavItem>
      <AvatarHeader
        handleCloseUserMenu={handleCloseUserMenu}
        anchorElUser={anchorElUser}
        setAnchorElUser={setAnchorElUser}
        handleOpenUserMenu={handleOpenUserMenu}
      />
    </Nav>
  );
};

export default IconHeader;
