import {
  Container,
  Grid,
  TextField,
  Typography,
  Button,
  Select,
  InputLabel,
  InputAdornment
} from "@mui/material";
import axios from "axios";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { SelectLocation } from "../Global/locationSelect";
import {
  getCustomerOrder,
  getExistedUser,
} from "../../../action/getExistedUser";
import { EditProfile } from "./editProfileComponent";
import { ShopToast } from "../../../ToastMessagePopup/toastShop";

export const ProfileInforComponent = () => {
  let { order, customer } = useSelector(
    (reduxData) => reduxData.checkOutReducer,
  );
  const user = sessionStorage.getItem("User");
  const dispatch = useDispatch();



  const [edit, setEdit] = useState(false);
  const [customers, setCustomers] = useState({
    fullName: customer[0]?.fullName,
    phone: customer[0]?.phone,
    email: customer[0]?.email,
    address: customer[0]?.address,
    city: customer[0]?.city,
    district: customer[0]?.district,
    ward: customer[0]?.ward,
    userId: user,
  })

  const [openSnack, setOpenSnack] = useState(false);
  const [messageSuccess, setMessageSuccess] = useState("");
  const [messageError, setMessageError] = useState("");
  const [fullname, setName] = useState("");
  const [phone, setPhone] = useState("");
  const [email, setEmail] = useState("");
  const [address, setAddress] = useState("");
  const [city, setCity] = useState("");
  const [district, setDistrict] = useState("");
  const [ward, setWard] = useState("");
  const [error, setError] = useState({});
  const [updateObj, setUpdateObj] = useState({})
  const validateError = {};
  //Get existed customer to auto fill to information box
  useEffect(() => {
    dispatch(getExistedUser(user))
  }, []);
  //Validate information without orders
  const validateData = () => {
    const isValidEmail = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g.test(email);
    const isValidPhone = /(84|0[3|5|7|8|9])+([0-9]{8})\b/g.test(phone);

    if (!fullname.trim()) {
      validateError.fullName = "Fullname is required";
    }
    if (!email.trim()) {
      validateError.email = "Email is required";
    } else if (!isValidEmail) {
      validateError.email = "Email is invalid";
    }
    if (!phone.trim() || isNaN(phone)) {
      validateError.phone = "Phone number is required and must be the number";
    } else if (!isValidPhone) {
      validateError.phone = "Phone is invalid";
    }
    if (!address.trim()) {
      validateError.address = "Address is required";
    }
    if (!city) {
      validateError.city = "City is required";
    }
    if (!district) {
      validateError.district = "District is required";
    }
    if (!ward) {
      validateError.ward = "Ward is required";
    }
    setError(validateError);
  };
  //validate update information
  const validateUpdateData = () => {
    const isValidEmail = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g.test(customers.email);
    const isValidPhone = /(84|0[3|5|7|8|9])+([0-9]{8})\b/g.test(customers.phone);
    //Validate input value
    if (!customers.fullName && customers.fullName != undefined) {
      validateError.fullName = "Fullname is required";
    }
    if (!customers.email && customers.email != undefined) {
      validateError.email = "Email is required";
    } else if (!isValidEmail && customers.email != undefined) {
      validateError.email = "Email is invalid";
    }
    if ((isNaN(phone)) && customers.phone != undefined) {
      validateError.phone = "Phone number is required and must be the number";
    } else if (!isValidPhone && customers.phone != undefined) {
      validateError.phone = "Phone is invalid";
    }
    if (!customers.address && customers.address != undefined) {
      validateError.address = "Address is required";
    }
    setError(validateError);
  }
  // Create information without any orders
  const onBtnCreateInformationClick = async () => {
    validateData();
    const customerObj = {
      fullName: fullname,
      phone: phone,
      email: email,
      address: address,
      city: city,
      district: district,
      ward: ward,
      userId: user,
    };
    if (Object.keys(validateError).length === 0) {
      try {
        const response = await axios.post(
          process.env.REACT_APP_FETCH_URL + "/api/customernoorder",
          customerObj,
        );
        const data = await response.data;
        if (response.status == 201) {
          setMessageError("");
          setOpenSnack(true);
          setMessageSuccess(data.message);
          setTimeout(() => window.location.reload(), 3000);
        }
      } catch (error) {
        setOpenSnack(true);

        setMessageError(error.response.data.message);
      }

    }
  };
  //update information
  const onBtnUpdateInformationClick = async () => {
    validateUpdateData()

    if (Object.keys(validateError).length === 0) {
      try {
        const response = await axios.put(
          process.env.REACT_APP_FETCH_URL + "/api/customer/" + customer[0]._id,
          customers,
        );
        const data = await response.data;

        if (response.status == 200) {
          setMessageError("");
          setOpenSnack(true);
          setMessageSuccess(data.message);
          setTimeout(() => window.location.reload(), 3000);
        }
      } catch (error) {
        setOpenSnack(true);

        setMessageError(error.response.data.message);
      }
    }
  };

  return (
    <>
      <Grid
        item
        md={3}
        xs={12}
        style={{
          display: "flex",
          flexDirection: "Column",
          backgroundColor: "white",
          width: "35%",
          maxWidth: "450px",
          minWidth: "300px",
          borderRadius: "20px",
          padding: "10px",
        }}
        sx={{
          marginTop: { xs: "10px", md: 0 }
        }}
      >
        <Typography align="center" variant="h5" component="div">
          <b>Customer Information</b>
        </Typography>
        {Object.keys(customer).length != 0 ? (
          <>
            <Container
              style={{
                textAlign: "center",
                maxWidth: "70%",
                minWidth: "250px",
              }}
            >
              <TextField
                variant="standard"
                label="Full Name"
                value={customers.fullName}
                defaultValue={customer[0]?.fullName}
                onChange={(e) => setCustomers({ ...customers, fullName: e.target.value })}
                sx={{ mt: 1, mb: 1 }}
                fullWidth
              ></TextField>
              {error.fullname && (
                <span style={{ color: "red" }}>{error.fullname}</span>
              )}
              <TextField
                variant="standard"
                label="Phone"
                onChange={(e) => setCustomers({ ...customers, phone: e.target.value })}
                defaultValue={customer[0]?.phone}
                value={customers.phone}
                fullWidth
              ></TextField>
              {error.phone && <span style={{ color: "red" }}>{error.phone}</span>}

              <TextField
                variant="standard"
                label="Email"
                onChange={(e) => setCustomers({ ...customers, email: e.target.value })}
                defaultValue={customer[0]?.email}
                value={customers.email}
                sx={{ mt: 1, mb: 1 }}
                fullWidth
              ></TextField>
              {error.email && <span style={{ color: "red" }}>{error.email}</span>}

              <TextField
                variant="standard"
                label="Address"
                onChange={(e) => setCustomers({ ...customers, address: e.target.value })}
                value={customers.address}
                defaultValue={customer[0].address}
                sx={{ mt: 1, mb: 1 }}
                fullWidth
              ></TextField>
              {error.address && (
                <span style={{ color: "red" }}>{error.address}</span>
              )}
              <TextField
                disabled
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <Typography color="error" sx={{ textDecoration: 'underline' }} onClick={() => setEdit(true)}>Change address</Typography>
                    </InputAdornment>)
                }}
                variant="standard"
                label="City"

                value={customer[0].city}
                sx={{ mt: 1, mb: 1 }}
                fullWidth
              ></TextField>
              <TextField
                disabled
                InputProps={{
                  readOnly: true,
                }}
                variant="standard"
                label="District"

                value={customer[0].district}
                sx={{ mt: 1, mb: 1 }}
                fullWidth
              ></TextField>
              <TextField
                disabled
                InputProps={{
                  readOnly: true,
                }}
                variant="standard"
                label="Ward"

                value={customer[0].ward}
                sx={{ mt: 1, mb: 1 }}
                fullWidth
              ></TextField>
              <Button
                variant="contained"
                color="success"

                onClick={() => onBtnUpdateInformationClick()}
              >
                Edit Customer
              </Button>
            </Container>

            <EditProfile customers={customers} setCustomers={setCustomers} edit={edit} setEdit={setEdit} id={customer[0]._id} />
          </>
        ) : (
          <Container
            style={{
              textAlign: "center",
              maxWidth: "70%",
              minWidth: "250px",
            }}
          >
            <TextField
              label="Full Name"
              value={fullname || ""}
              onChange={(e) => setName(e.target.value)}
              sx={{ mt: 1, mb: 1 }}
              fullWidth
            ></TextField>
            {error.fullname && (
              <span style={{ color: "red" }}>{error.fullname}</span>
            )}
            <TextField
              label="Phone"
              value={phone || ""}
              onChange={(e) => setPhone(e.target.value)}
              fullWidth
            ></TextField>
            {error.phone && <span style={{ color: "red" }}>{error.phone}</span>}
            <TextField
              label="Email"
              value={email || ""}
              onChange={(e) => setEmail(e.target.value)}
              sx={{ mt: 1, mb: 1 }}
              fullWidth
            ></TextField>
            {error.email && <span style={{ color: "red" }}>{error.email}</span>}
            <TextField
              label="Address"
              value={address || ""}
              onChange={(e) => setAddress(e.target.value)}
              fullWidth
            ></TextField>
            {error.address && (
              <span style={{ color: "red" }}>{error.address}</span>
            )}
            <SelectLocation
              error={error}
              setCity={setCity}
              setDistrict={setDistrict}
              city={city}
              ward={ward}
              setWard={setWard}
              district={district}
            />
            <Button
              variant="contained"
              color="success"
              onClick={() => onBtnCreateInformationClick()}
            >
              Update Customer Information
            </Button>
          </Container>
        )}
      </Grid >
      <ShopToast
        open={openSnack}
        setOpen={setOpenSnack}
        messageError={messageError}
        messageSuccess={messageSuccess}
      />
    </>
  );
};
