import axios from "axios";
import { ERROR, GET_CITY } from "../contants/data";

export const getCity = () => {
  return async (dispatch) => {
    try {
      const response = await axios(
        "https://raw.githubusercontent.com/kenzouno1/DiaGioiHanhChinhVN/master/data.json",
      );
      const data = await response.data;
      return dispatch({
        type: GET_CITY,
        payload: data,
      });
    } catch (error) {
      return dispatch({
        type: ERROR,
      });
    }
  };
};
