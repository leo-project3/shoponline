const mongoose = require("mongoose");
const customerModel = require("../model/customerModel");
const orderModel = require("../model/orderModel");
const orderDetailsModel = require("../model/orderDetailsModel");
const productModel = require("../model/productModel");

const createCustomer = async (req, res) => {
  const {
    userId,
    fullName,
    phone,
    email,
    address,
    city,
    district,
    ward,
    orders
  } = req.body
  if (fullName == "") {
    return res.status(400).json({
      message: "Full name cant be blank"
    })
  }
  if (phone == "") {
    return res.status(400).json({
      message: "Phone cant be blank"
    })
  }
  if (email == "") {
    return res.status(400).json({
      message: "Email cant be blank"
    })
  }
  try {
    const existedCustomer = await customerModel.findOne({ userId: userId })
    if (!existedCustomer) {
      let condition = {}
      if (req.body.phone) {
        condition.phone = req.body.phone
      }
      if (req.body.email) {
        condition.email = req.body.email
      }
      const result = await customerModel.findOne({ phone: req.body.phone })
      if (!result) {
        console.log("null")
      } else {

        if (result.phone == req.body.phone) {
          return res.status(404).json({
            message: "Phone number has been used"
          })
        }

        if (result.email == req.body.email) {
          return res.status(404).json({
            message: "Email has been used"
          })
        }

      }
      const result2 = await customerModel.findOne({ email: req.body.email })
      if (!result2) {
        console.log("null")
      } else {
        if (result2.email == req.body.email) {
          return res.status(404).json({
            message: "Email has been used"
          })
        }
      }
      const newCustomer = await customerModel.create({
        _id: new mongoose.Types.ObjectId,
        userId: userId,
        fullName: fullName,
        phone: phone,
        email: email,
        address: address,
        city: city,
        district: district,
        ward: ward,
        orders: orders
      })
      const order = await orderModel.findOneAndUpdate({ _id: orders }, { isDelete: false })
      let orderDetail = await order.orderDetails
      let orderDetails = await orderDetailsModel.find({ _id: { $in: orderDetail } })
      for (const product of orderDetails) {
        let productId = await productModel.findOneAndUpdate({ _id: product.product }, {
          $inc: { amount: (0 - product.quantity) }
        })
      }
      return res.status(201).json({
        message: "Create new customer",
        Customer: newCustomer
      })
    }
    else {
      const customer = await customerModel.findOneAndUpdate({ userId: userId }, {
        $push: { orders: orders }
      })
      const order = await orderModel.findOneAndUpdate({ _id: orders }, { isDelete: false })
      let orderDetail = await order.orderDetails
      let orderDetails = await orderDetailsModel.find({ _id: { $in: orderDetail } })
      for (const product of orderDetails) {
        let productId = await productModel.findOneAndUpdate({ _id: product.product }, {
          $inc: { amount: (0 - product.quantity) }
        })
      }

      return res.status(201).json({
        message: "New order to existed customer",
        Customer: customer
      })
    }
  } catch (error) {
    return res.status(500).json({
      message: "Error" + error.message
    })
  }
}

const createCustomerNoOrder = async (req, res) => {
  const {
    userId,
    fullName,
    phone,
    email,
    address,
    city,
    district,
    ward
  } = req.body
  if (fullName == "") {
    return res.status(400).json({
      message: "Full name cant be blank"
    })
  }
  if (phone == "") {
    return res.status(400).json({
      message: "Phone cant be blank"
    })
  }
  if (email == "") {
    return res.status(400).json({
      message: "Email cant be blank"
    })
  }

  try {
    const newCustomer = await customerModel.create({
      _id: new mongoose.Types.ObjectId,
      userId: userId,
      fullName: fullName,
      phone: phone,
      email: email,
      address: address,
      city: city,
      district: district,
      ward: ward,
      order: []
    })
    return res.status(201).json({
      message: "Create New Customer"
    })
  } catch (error) {
    return res.status(500).json({
      message: "Error" + error.message
    })
  }
}

const getAllCustomer = async (req, res) => {
  let condition = {}
  if (req.body.orders) {
    condition.orders = { $in: [req.body.orders] }
  }
  condition.isDelete = false
  try {
    const result = await customerModel.find(condition).populate("orders", "status").populate("userId", ["username", "isAdmin"])/* .skip(req.query.skip).limit(req.query.limit) */;
    return res.status(200).json({
      message: "Get customer successfully",
      Customer: result
    })
  } catch (error) {
    return res.status(500).json({
      message: "Error" + error.message
    })
  }
}
const getCustomerById = async (req, res) => {
  const id = req.params.id;
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(404).json({
      message: "Id is not valid"
    })
  }

  try {
    const result = await customerModel.find({ $or: [{ _id: id }, { userId: id }] }).populate("orders", "orderDate");
    if (result) {
      return res.status(200).json({
        message: "get customer successfully",
        Customer: result
      })
    } else {
      return res.status(404).json({
        message: "Id cant be found"
      })
    }
  } catch (error) {
    return res.status(500).json({
      message: "Error" + error.message
    })
  }
}

const getOrderCustomer = async (req, res) => {
  const order = req.params.order;
  try {
    const result = await customerModel.find({ orders: { $in: [order] } });
    if (result) {
      return res.status(200).json({
        message: "get customer successfully",
        Customer: result
      })
    } else {
      return res.status(400).json({
        message: "Id cant be found"
      })
    }
  } catch (error) {
    return res.status(500).json({
      message: "Error" + error.message
    })
  }
}

const updateCustomerById = async (req, res) => {
  const id = req.params.id;
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(404).json({
      message: "Id is not valid"
    })
  }
  const {
    fullName,
    phone,
    email,
    address,
    city,
    district,
    ward
  } = req.body
  /*   if (fullName == "") {
      return res.status(400).json({
        message: "Full name cant be blank"
      })
    }
    if (phone == "") {
      return res.status(400).json({
        message: "Phone cant be blank"
      })
    }
    if (email == "") {
      return res.status(400).json({
        message: "Email cant be blank"
      })
    } */
  try {
    var updateCustomer = {};
    if (fullName) {
      updateCustomer.fullName = fullName;
    }
    if (phone) {
      updateCustomer.phone = phone;
    }
    if (email) {
      updateCustomer.email = email;
    }
    if (address) {
      updateCustomer.address = address;
    }
    if (city) {
      updateCustomer.city = city;
    }
    if (district) {
      updateCustomer.district = district;
    }
    if (ward) {
      updateCustomer.ward = ward;
    }

    const result = await customerModel.findByIdAndUpdate(id, updateCustomer);
    if (result) {
      return res.status(200).json({
        message: "Update customer successfully",
        customer: result
      })
    } else {
      return res.status(400).json({
        message: "No id found"
      })
    }
  } catch (error) {
    return res.status(500).json({
      message: "Error" + error.message
    })

  }
}

const deleteCustomerById = async (req, res) => {
  const id = req.params.id
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(404).json({
      message: "Id is not valid"
    })
  }
  try {
    const result = await customerModel.findByIdAndUpdate(id, { isDelete: true })
    if (result) {
      return res.status(200).json({
        message: "Delete customer successfully"
      })
    } else {
      return res.status(404).json({
        message: "No customer id founded"
      })
    }
  } catch (error) {
    return res.status(500).json({
      message: "Error" + err.message
    })
  }
}
module.exports = {
  createCustomer,
  createCustomerNoOrder,
  getAllCustomer,
  getOrderCustomer,
  getCustomerById,
  updateCustomerById,
  deleteCustomerById
}