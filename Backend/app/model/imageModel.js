const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const imageSchema = new Schema({
  name: {
    type: String,
    required: true,
    unique: true
  },
  image: [{
    type: String,
    require: true
  }]
}, { timestamp: true })
module.exports = mongoose.model("image", imageSchema)