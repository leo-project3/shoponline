const mongoose = require("mongoose")
const Schema = mongoose.Schema;

const refreshTokenSchema = new Schema({
  token: {
    type: String,
    required: true
  },
  user: {
    type: mongoose.Types.ObjectId,
    ref: "user"
  },
  expiredDate: {
    type: Date,
    required: true
  }
}, {timestamps: true })

module.exports = mongoose.model("refreshToken", refreshTokenSchema)