const express = require("express")
const router = express.Router();

const userController = require("../controller/userController")

const { verifyToken, checkUser } = require("../middleware/authMiddleware");

router.post("/signup", userController.SignUp)
router.post("/signin", userController.SignIn)
router.put('/changepassword', userController.updatePassword)
router.post("/refreshToken", userController.refreshToken)
router.post("/registeradmin", [verifyToken, checkUser], userController.SignUpAdmin)
router.get("/user/:id", userController.GetUser)
router.get("/user", userController.GetUserByName)
module.exports = router