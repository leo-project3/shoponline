import { applyMiddleware, createStore } from "redux";
import { thunk } from "redux-thunk";
import { rootReducer } from "../reducer/dataIndex";

export const store = createStore(rootReducer, applyMiddleware(thunk));
