import { Box, Modal, Typography } from "@mui/material";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "30%",
  minWidth: "300px",
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};


//This will be displayed when order successfully
export const SuccessOrderModal = ({ open, setOpen }) => {
  const { orderCode } = useSelector((reduxData) => reduxData.checkOutReducer);
  const navigate = useNavigate();
  const handlerClose = () => {
    localStorage.clear();
    setOpen(false);
    navigate("/profile");
  };
  return (
    <Modal
      open={open}
      onClose={() => handlerClose()}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={style}>
        <Typography
          textAlign="center"
          id="modal-modal-title"
          variant="h6"
          component="h2"
        >
          Order Successfully
        </Typography>
        <Typography
          textAlign="center"
          id="modal-modal-description"
          sx={{ mt: 2 }}
        >
          Thank you for choosing us !!!
        </Typography>
      </Box>
    </Modal>
  );
};
