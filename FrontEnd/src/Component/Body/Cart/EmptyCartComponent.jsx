import { Container, Typography } from "@mui/material";
import { Link } from "react-router-dom";

export const EmptyCart = () => {
  //Display when no product in cart
  return (
    <>
      <Container
        className="container bg-white p-4 rounded-4"
        sx={{
          marginTop: "120px",
          textAlign: "center",
          display: "flex",
          alignItems: "center",
          flexDirection: "column",
        }}
      >
        <Typography component="div" variant="h3">
          Your Cart Is Empty
        </Typography>
        <Link
          style={{ textDecoration: "none", color: "darkred" }}
          to="/products"
        >
          Go Back To Buy Some Products
        </Link>
      </Container>
    </>
  );
};
