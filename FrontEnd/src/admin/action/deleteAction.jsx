import axios from "axios";
import { ERROR } from "../../contants/data";
import { DELETE, GET_PRODUCT, GET_ORDER, GET_CUSTOMER } from "../contants/data";

//soft delete customer
export const deleteCustomer = (id, setMessageError, setMessageSuccess, setOpenToast) => {
  return async (dispatch) => {
    try {
      const response = await axios.delete(
        process.env.REACT_APP_FETCH_URL + "/api/customer/" + id,
        {
          headers: {
            "x-access-token": sessionStorage.getItem("AccessToken"),
            "x-refresh-token": sessionStorage.getItem("RefreshToken"),
          },
        },
      );

      const response2 = await axios(
        process.env.REACT_APP_FETCH_URL + "/api/customer",
      );
      const data = await response2.data.Customer;
      const validCustomer = await data.filter((cus) => cus.isDelete !== true);
      setMessageError("")
      setMessageSuccess(response.data.message)
      setOpenToast(true)
      dispatch({
        type: GET_CUSTOMER,
        payload: validCustomer,
      });
    } catch (error) {
      setMessageSuccess("")
      setMessageError(error.response.data.message)
      setOpenToast(true)
      if (
        error.response.data.message == "Unauthourized" ||
        error.response.data.message == "Please Login" ||
        error.response.data.message == "Please Login Again"
      ) {
        sessionStorage.clear();
        setTimeout(
          () =>
            (window.location.href = process.env.REACT_APP_MAIN_URL + "/login"),
          4000,
        );
      }
    }
  };
};
//Soft delete order
export const deleteOrder = (id, setMessageError, setMessageSuccess, setOpenToast) => {
  return async (dispatch) => {
    try {
      const response = await axios.delete(
        process.env.REACT_APP_FETCH_URL + "/api/order/" + id,
        {
          headers: {
            "x-access-token": sessionStorage.getItem("AccessToken"),
            "x-refresh-token": sessionStorage.getItem("RefreshToken"),
          },
          body: {
            userId: sessionStorage.getItem("User"),
          },
        },
      );
      const response2 = await axios(
        process.env.REACT_APP_FETCH_URL + "/api/order",
      );
      const data2 = await response2.data.orders;
      const validOrders = await data2.filter((cus) => cus.isDelete !== true);
      setMessageError("")
      setMessageSuccess(response.data.message)
      setOpenToast(true)
      dispatch({
        type: GET_ORDER,
        orderData: validOrders,
      });
    } catch (error) {
      setMessageSuccess("")
      setMessageError(error.response.data.message)
      setOpenToast(true)
      if (
        error.response.data.message == "Unauthourized" ||
        error.response.data.message == "Please Login" ||
        error.response.data.message == "Please Login Again"
      ) {
        sessionStorage.clear();
        setTimeout(
          () =>
            (window.location.href = process.env.REACT_APP_MAIN_URL + "/login"),
          4000,
        );
      }
    }
  };
};

//Soft delete product
export const deleteProduct = (id, setMessageError, setMessageSuccess, setOpenToast) => {
  return async (dispatch) => {
    try {
      const response = await axios.delete(
        process.env.REACT_APP_FETCH_URL + "/api/product/" + id,
        {
          headers: {
            "x-access-token": sessionStorage.getItem("AccessToken"),
            "x-refresh-token": sessionStorage.getItem("RefreshToken"),
          },
        },
      );
      const response2 = await axios(
        process.env.REACT_APP_FETCH_URL + "/api/product",
      );
      const data2 = await response2.data.products;
      const validProduct = await data2.filter((cus) => cus.isDelete !== true);
      setMessageError("")
      setMessageSuccess(response.data.message)
      setOpenToast(true)
      dispatch({
        type: GET_PRODUCT,
        payload: validProduct,
      });

    } catch (error) {
      setMessageSuccess("")
      setMessageError(error.response.data.message)
      setOpenToast(true)
      if (
        error.response.data.message == "Unauthourized" ||
        error.response.data.message == "Please Login" ||
        error.response.data.message == "Please Login Again"
      ) {
        sessionStorage.clear();
        setTimeout(
          () =>
            (window.location.href = process.env.REACT_APP_MAIN_URL + "/login"),
          4000,
        );
      }
    }
  };
};
