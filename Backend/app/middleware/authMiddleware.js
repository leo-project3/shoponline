const jwt = require("jsonwebtoken")
const userModel = require("../model/userModel")
const { refreshToken } = require("../controller/userController")
const { default: axios } = require("axios")

const verifyToken = async (req, res, next) => {
  try {
    let token = req.headers["x-access-token"]
    let refresh = req.headers['x-refresh-token']
    if (!token) {
      return res.status(401).json({
        message: "Please Login"
      })
    }
    const secretKey = process.env.JWT_SECRET
    const verify = await jwt.verify(token, secretKey, function (err, decoded) {

      if (!err) {
        return decoded

      } else if (err.message == "jwt expired") {

        return axios.post("http://localhost:8080/api/refreshToken", {
          refreshToken: refresh
        })
          .then((response) => {
            token = response.data.accesToken
            const newVerify = jwt.verify(token, secretKey, function (err, decoded) {
              if (!err) {
                return decoded
              }
            })
            return newVerify
          })
      }

    })
    const user = await userModel.findById(verify.id)
    req.user = user.isAdmin
    next();
  } catch (error) {
    return res.status(500).json({
      message: "Please Login Again"
    })
  }
}

const checkUser = async (req, res, next) => {
  try {
    const userRole = req.user;

    if (userRole == true) {
      next();
      return
    }

    return res.status(401).json({
      message: "Unauthourized"
    })
  } catch (error) {
    return res.status(500).json({
      message: "Interal server error" + error.message
    })
  }
}

module.exports = {
  verifyToken,
  checkUser
}