const createOrderMiddleware = (req, res, next) => {
  console.log("Create Order Middleware")
  next();
}
const getOrderMiddleware = (req, res, next) => {
  console.log("Get Order Middleware")
  next();
}
const getOrderByIdMiddleware = (req, res, next) => {
  console.log("Get Order by Id Middleware")
  next();
}
const updateOrderMiddleware = (req, res, next) => {
  console.log("Update Order Middleware")
  next();
}
const deleteOrderMiddleware = (req, res, next) => {
  console.log("Delete Order Middleware")
  next();
}

module.exports = {
  createOrderMiddleware,
  getOrderMiddleware,
  getOrderByIdMiddleware,
  updateOrderMiddleware,
  deleteOrderMiddleware
}