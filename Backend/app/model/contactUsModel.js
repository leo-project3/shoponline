const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const contactUsSchema = new Schema({
  email: {
    type: String,
    required: true
  },
  message:{
    type: String
  },
  isDelete: {
    type: Boolean,
    default: false
  }
  
}, {timestamps: true})
module.exports = mongoose.model("contactUs", contactUsSchema)