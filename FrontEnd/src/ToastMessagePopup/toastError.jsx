import * as React from "react";
import Button from "@mui/material/Button";
import Snackbar from "@mui/material/Snackbar";
import Alert from "@mui/material/Alert";
import { useSelector } from "react-redux";

export const ErrorToast = ({ open, setOpen }) => {
  const { success, error, create, remove } = useSelector(
    (reduxData) => reduxData.productReducer,
  );
  const handleClick = () => {
    setOpen(true);
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  return (
    <div>
      <Snackbar
        open={open}
        autoHideDuration={6000}
        onClose={() => setOpen(false)}
        anchorOrigin={{ vertical: "top", horizontal: "right" }}
      >
        <Alert
          onClose={handleClose}
          severity={success || create || remove ? "success" : "error"}
          sx={{ width: "100%" }}
        >
          {remove && remove}
          {success && success}
          {error && error}
          {create && create}
        </Alert>
      </Snackbar>
    </div>
  );
};
