import Paper from "@mui/material/Paper";
import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";
import { TotalPriceContainer } from "./totalPriceComponent";
import {
  Grid,
  OutlinedInput,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  Stack,
  Divider,
  Box,
  TableRow,
  Typography,
  InputAdornment,
  IconButton,
  Container,
} from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
export const CartComponent = ({ onBtnMinus, onBtnAdd, onBtnDelete }) => {
  //get product from localstorage
  const cartItem = JSON.parse(localStorage.getItem("cart"));
  //Product card for sm screen (>600px)
  const card = cartItem.map((product, index) => {
    return (
      <TableRow key={product._id} sx={{ display: { sm: "table-row", xs: "none" } }}>
        <TableCell sx={{ display: "flex", alignItems: "center" }}>
          <img
            src={process.env.REACT_APP_FETCH_URL + "/upload/" + product.imageUrl.image[0]}
            alt="product-img"
            style={{ width: "95px", height: "90px" }}
          />
          <Typography sx={{ marginLeft: "50px" }}>{product.name}</Typography>
        </TableCell>
        <TableCell align="center" sx={{ color: "red" }}>
          <b>{product.promotionPrice.toLocaleString()} VND</b>
        </TableCell>
        <TableCell align="center">
          <OutlinedInput
            sx={{ textAlign: "center", width: "100%", minWidth: "95px", maxWidth: "105px", height: "30px", padding: '2px' }}
            inputProps={{ min: 0, style: { textAlign: "center" } }}
            size="small"
            type="text"
            value={product.quantity}
            endAdornment={
              <InputAdornment position="end">
                <IconButton sx={{ width: "20px" }} onClick={() => onBtnAdd(product)}>
                  <Divider orientation="vertical" flexItem sx={{ borderColor: "black" }} />
                  <AddIcon fontSize="small" />
                </IconButton>
              </InputAdornment>
            }
            startAdornment={
              <InputAdornment position="start">
                <IconButton sx={{ width: "20px" }} onClick={() => onBtnMinus(product)}>
                  <RemoveIcon fontSize="small" />
                  <Divider orientation="vertical" flexItem sx={{ borderColor: "black" }} />
                </IconButton>

              </InputAdornment>
            }
          />
        </TableCell>
        <TableCell align="center" sx={{ color: "" }}>
          <b>{(product.promotionPrice * product.quantity).toLocaleString()} VND</b>
        </TableCell>
        <TableCell>
          <DeleteIcon onClick={() => onBtnDelete(product)} />
        </TableCell>
      </TableRow>
    );
  });
  //Product card for xs screen (<600px)
  const smallCard = cartItem.map((product, index) => {
    return (
      <TableRow key={product._id} sx={{ display: { xs: "flex", sm: "none" }, justifyContent: "space-around" }} >
        <TableCell sx={{ display: "flex", alignItems: "center" }}>
          <img
            src={process.env.REACT_APP_FETCH_URL + "/upload/" + product.imageUrl.image[0]}
            alt="product-img"
            style={{ width: "100px", height: "90px" }}
          />
        </TableCell>
        <TableCell sx={{ display: "flex", flexDirection: "column", alignItems: "start", justifyContent: "center" }}>
          <Typography>{product.name}</Typography>
          <b>{product.promotionPrice.toLocaleString()} VND</b>
          <Container sx={{ display: "flex", justifyContent: "space-between", alignItems: "center", padding: 0, margin: 0 }}>
            <OutlinedInput
              sx={{ textAlign: "center", width: "60%", minWidth: "95px", maxWidth: "105px", height: "30px", padding: '2px' }}
              inputProps={{ min: 0, style: { textAlign: "center" } }}
              size="small"
              type="text"
              value={product.quantity}
              endAdornment={
                <InputAdornment position="end">
                  <Divider orientation="vertical" flexItem sx={{ borderColor: "black" }} />
                  <IconButton sx={{ width: "20px" }} onClick={() => onBtnAdd(product)}>
                    <AddIcon fontSize="small" />
                  </IconButton>
                </InputAdornment>
              }
              startAdornment={
                <InputAdornment position="start">
                  <IconButton sx={{ width: "20px" }} onClick={() => onBtnMinus(product)}>
                    <RemoveIcon fontSize="small" />
                    <Divider orientation="vertical" flexItem sx={{ borderColor: "black" }} />
                  </IconButton>
                </InputAdornment>
              }
            />
            <DeleteIcon onClick={() => onBtnDelete(product)} />
          </Container>
        </TableCell>

      </TableRow >
    );
  });

  return (
    <>
      <Container sx={{
        marginTop: "120px", width: "100%",
        minWidth: "320px",
      }} >
        <Grid container spacing={1} alignItems='flex-start' justifyContent='flex-end' sx={{ padding: 0, }}>
          <Grid item xs={12} sx={{ backgroundColor: "white", padding: "10px" }}>
            <Typography variant='h5' component="div">Cart</Typography>
          </Grid>

          <Grid item md={8} xs={12} sx={{
            width: "100%",
            minWidth: "320px"
          }}>
            <TableContainer component={Paper} className="bg-white container" sx={{
              width: '100%',
              maxHeight: "500px", padding: 0, overflowX: 'hidden',
              '&::-webkit-scrollbar': {
                width: '0.5em',
              },
              '&::-webkit-scrollbar-track': {
                boxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)',
                borderRadius: "3px",
              },
              '&::-webkit-scrollbar-thumb': {
                backgroundColor: 'rgba(0,0,0,.1)',
                borderRadius: "3px"

              }
            }}>
              <Table>
                <TableHead sx={{ display: { xs: "none", sm: "table-header-group" } }}>
                  <TableRow>
                    <TableCell>Product</TableCell>
                    <TableCell align="center">Price</TableCell>
                    <TableCell align="center">Quantity</TableCell>
                    <TableCell align="center">Total</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {card}
                  {smallCard}
                </TableBody>
              </Table>
            </TableContainer>
          </Grid>
          <Grid item md={4} xs={12} sx={{ padding: 0, display: "flex", justifyContent: "flex-end" }}>
            <TotalPriceContainer />
          </Grid>
        </Grid>
      </Container >
    </>
  );
};
