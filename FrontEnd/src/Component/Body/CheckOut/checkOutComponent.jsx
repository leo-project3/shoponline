import { Grid, Container } from "@mui/material";
import { useLocation, useNavigate } from "react-router-dom";
import { CheckOutPreview } from "./checkOutProductPreview";
import { CheckOutInformation } from "./checkOutInformation";
import { useEffect, useState } from "react";
import { SuccessOrderModal } from "../../../Modal/orderSuccessModal";
import { ShopToast } from "../../../ToastMessagePopup/toastShop";

export const CheckOut = () => {
  const location = useLocation();
  const navigate = useNavigate();
  let totalPrice;
  let shippingFee;
  //get data form location state react
  if (location.state != null) {
    totalPrice = location.state.totalPrice.toLocaleString();
    shippingFee = location.state.shippingFee.toLocaleString();
  }
  const [open, setOpen] = useState(false);
  const [openToast, setOpenToast] = useState(false);
  const [errorMsg, setErrorMsg] = useState("");
  return (
    <>
      <Container maxWidth>
        <Grid
          container
          sx={{
            marginTop: "150px",
            marginBottom: '100px',
            display: "flex",
            justifyContent: "space-around",
            alignItems: "center",
            flexDirection: { xs: "column-reverse", md: "row" },
          }}
        >
          <SuccessOrderModal open={open} setOpen={setOpen} />
          <CheckOutInformation
            open={open}
            setOpen={setOpen}
            totalPrice={totalPrice}
            setOpenToast={setOpenToast}
            setErrorMsg={setErrorMsg}
          />
          <CheckOutPreview shippingFee={shippingFee} totalPrice={totalPrice} />
          <ShopToast
            messageError={errorMsg}
            open={openToast}
            setOpen={setOpenToast}
          />
        </Grid>
      </Container>
    </>
  );
};
