const express = require("express")
const router = express.Router();

const {
  createProductMiddleware,
  getProductMiddleware,
  getProductByIdMiddleware,
  updateProductMiddleware,
  deleteProductMiddleware
} = require("../middleware/productMiddleware")

const {
  getAllProduct,
  createProduct,
  getProductById,
  updateProductById,
  deleteProductById
} = require ("../controller/productController");
const { verifyToken, checkUser } = require("../middleware/authMiddleware");

router.get("/product",getProductMiddleware, getAllProduct)
router.post("/product", [createProductMiddleware, verifyToken, checkUser], createProduct)
router.get("/product/:id",getProductByIdMiddleware, getProductById)
router.put("/product/:id", [createProductMiddleware, verifyToken, checkUser], updateProductMiddleware, updateProductById)
router.delete("/product/:id", [createProductMiddleware, verifyToken, checkUser], deleteProductMiddleware,deleteProductById)

module.exports = router