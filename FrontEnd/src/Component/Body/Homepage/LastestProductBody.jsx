import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getDataItem } from "../../../action/dataListAction";
import { Link } from "react-router-dom";
import {
  Card,
  Grid,
  Typography,
  CardContent,
  CardMedia,
  Button,
  CircularProgress,
} from "@mui/material";
const Product = () => {
  const dispatch = useDispatch();
  /*  useEffect(() => {
      fetch("http://localhost:8080/api/product?limit=8")
      .then((res) => res.json())
      .then((data) => setData(data.products))
    }, [])*/
  const { pending, items, page, limit, itemsFilter } = useSelector(
    (reduxData) => reduxData.dataReducer,
  );
  useEffect(() => {
    //If itemsFilter == 0 => this action will be triggered
    if (itemsFilter.length == 0) {
      dispatch(getDataItem(page, limit));
    }
  }, [page, limit]);

  return (
    <Grid
      container
      align={"center"}
      spacing={3}
      sx={{ alignItems: "center", justifyContent: "center" }}
    >
      {pending ? (
        <Grid item sx={{ justifyContent: "center" }}>
          <CircularProgress color="error" />
        </Grid>
      ) : (
        items.map((item, index) => (
          <Grid item md={4} lg={3} xs={12} sm={6} key={item._id} className="my-2">
            <Link
              to={`/products/${item._id}`}
              style={{
                textDecoration: "none",
              }}
              state={item.type}
            >
              <Card sx={{ maxWidth: 345, padding: "10px" }}>
                <CardMedia
                  sx={{ height: "20rem" }}
                  image={process.env.REACT_APP_FETCH_URL + "/upload/" + item.imageUrl.image[0]}
                  title="green iguana"
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="div">
                    {item.name}
                  </Typography>
                  <Typography variant="body2" color="text.secondary">
                    Price:{" "}
                    <b style={{ color: "red" }}>
                      {item.promotionPrice.toLocaleString() + "VND"}
                    </b>
                  </Typography>
                </CardContent>
              </Card>
            </Link>
          </Grid>
        ))
      )}
    </Grid>
  );
};
export default Product;
