import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import {
  Grid,
  InputLabel,
  TextField,
  Select,
  MenuItem,
  Button,
  Typography,
  FormControl,
} from "@mui/material";
import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { createProduct } from "../../action/createAction";
import { InputImage } from "../modal/inputImage";
import { ShopToast } from "../../../ToastMessagePopup/toastShop";
const style = {
  width: "100%",
  borderRadius: "10px",
  bgcolor: "background.paper",
  border: "1px solid #000",
  boxShadow: 24,
  p: "50px",
  marginTop: "10px",
};

export const CreateProductTab = () => {
  const dispatch = useDispatch();
  const [success, setSuccess] = useState(false);
  const [openToast, setOpenToast] = useState(false);
  const [messageSuccess, setMessageSuccess] = useState("");
  const [messageError, setMessageError] = useState("");
  const [data, setData] = useState([]);
  const [name, setName] = useState();
  const [type, setType] = useState();
  const [description, setDescript] = useState();
  const [price, setPrice] = useState(0);
  const [promoPrice, setPromoPrice] = useState(0);
  const [stock, setStock] = useState(0);
  const [image, setImage] = useState("");
  const [error, setError] = useState({});

  useEffect(() => {
    fetch(process.env.REACT_APP_FETCH_URL + "/api/producttype")
      .then((res) => res.json())
      .then((data) => setData(data.productType));
  }, []);
  const typeI = data.map((item) => item.type);
  const filterItem = typeI.filter(
    (item, index) => typeI.indexOf(item) === index,
  );
  const typeItem = data.map((item, index) => {
    return (
      <MenuItem key={item._id} value={item}>
        {item.name}
      </MenuItem>
    );
  });
  const validateError = {};
  const validateInput = () => {
    if (!name) {
      validateError.name = "Product Name is required";
    }
    if (!type) {
      validateError.type = "Product Type is required";
    }
    if (price <= 0 && !isNaN(price)) {
      validateError.price = "Origin Price must greater than 0";
    }
    if (promoPrice <= 0 && !isNaN(promoPrice)) {
      validateError.promoPrice = "Promo Price must greater than 0";
    } else if (parseInt(promoPrice) > parseInt(price)) {
      validateError.promoPrice = "Promo Price must be less than Origin Price";
    }
    /*  if(!image){
      validateError.image = "Image is required"
    } else if(!/\.(jpg|jpeg|png|webp|avif|gif|svg)$/.test(image)){
      validateError.image = "Image is invalid"
    } */
    setError(validateError);
  };
  const onBtnCreateClick = () => {
    validateInput();
    const product = {
      name: name,
      description: description,
      type: type.name,
      imageUrl: image,
      buyPrice: price,
      promotionPrice: promoPrice,
      amount: stock,
    };

    if (Object.keys(validateError).length === 0) {
      dispatch(createProduct(product, setOpenToast, setMessageSuccess, setMessageError,));
    }
  };

  return (
    <div>
      <Box sx={style}>
        <Typography variant="h4" mb={5} align="center">
          Create New Product
        </Typography>
        <Grid container spacing={2}>
          <Grid item sm={6}>
            <TextField
              label="Name"
              value={name || ""}
              fullWidth
              onChange={(e) => setName(e.target.value)}
              sx={{
                "& .Mui-focused": {
                  color: "white",
                },
              }}
            />
            {error.name && <span style={{ color: "red" }}>{error.name}</span>}
          </Grid>
          <Grid item sm={6}>
            <InputImage product={{ name: name }} error={error} image={image} setImage={setImage} />
          </Grid>
        </Grid>
        <Grid container spacing={2} sx={{ mt: 2 }}>
          <Grid item sm={6}>
            <FormControl fullWidth>
              <InputLabel
                id="demo-simple-select-label"
                sx={{
                  "&.Mui-focused": {
                    color: "White",
                  },
                }}
              >
                Type
              </InputLabel>
              <Select
                label="Type"
                value={type || ""}
                fullWidth
                onChange={(e) => setType(e.target.value)}
              >
                {typeItem}
              </Select>
            </FormControl>
            {error.type && <span style={{ color: "red" }}>{error.type}</span>}
          </Grid>
          <Grid item sm={6}>
            <TextField
              label="Stock"
              value={stock || ""}
              fullWidth
              onChange={(e) => setStock(e.target.value)}
              sx={{
                "& .Mui-focused": {
                  color: "White",
                },
              }}
            />
          </Grid>
        </Grid>
        <Grid container spacing={2} sx={{ mt: 2 }}>
          <Grid item sm={6}>
            <TextField
              label="Price"
              type="number"
              value={price || ""}
              fullWidth
              onChange={(e) => setPrice(e.target.value)}
              sx={{
                "& .Mui-focused": {
                  color: "White",
                },
              }}
            />
            {error.price && <span style={{ color: "red" }}>{error.price}</span>}
          </Grid>
          <Grid item sm={6}>
            <TextField
              label="Promo Price"
              type="number"
              value={promoPrice || ""}
              fullWidth
              onChange={(e) => setPromoPrice(e.target.value)}
              sx={{
                "& .Mui-focused": {
                  color: "White",
                },
              }}
            />
            {error.promoPrice && (
              <span style={{ color: "red" }}>{error.promoPrice}</span>
            )}
          </Grid>
        </Grid>
        <Grid container sx={{ mt: 2 }}>
          <Grid item sm={12}>
            <TextField
              label="Description"
              multiline
              rows={2}
              value={description || ""}
              fullWidth
              onChange={(e) => setDescript(e.target.value)}
              sx={{
                "& .Mui-focused": {
                  color: "White",
                },
              }}
            />
          </Grid>
        </Grid>

        <Button
          variant="contained"
          color="secondary"
          sx={{ float: "right", marginTop: "10px" }}
          onClick={() => onBtnCreateClick()}
        >
          Create Product
        </Button>
      </Box>
      <ShopToast
        open={openToast}
        setOpen={setOpenToast}
        messageError={messageError}
        messageSuccess={messageSuccess}
      />
    </div>
  );
};

/* export const UpdateOrderModal = ({openUpdateModal, setOpenUpdateModal, order}) => {
  const dispatch = useDispatch()
  const [success, setSuccess] = useState(false)
  const [status, setStatus] = useState()
  const onBtnChangeStatus = (e) => {
  setStatus(e.target.value)  
 }

  return(
    <div>
    <Modal
      open={openUpdateModal}
      onClose={() => setOpenUpdateModal(false)}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={style}>
        <Grid container>
            <Grid item sm={4}>
              <InputLabel>Order Code</InputLabel>
            </Grid>
            <Grid item sm={8}>
              <TextField disabled value={order.orderCode || ""} fullWidth />
            </Grid>
        </Grid>
        <Grid container sx={{mt:2}}>
            <Grid item sm={4}>
              <InputLabel>Order Date</InputLabel>
            </Grid>
            <Grid item sm={8}>
              <TextField disabled value={order.orderDate || ""} fullWidth />
            </Grid>
        </Grid>
        <Grid container sx={{mt:2}}>
            <Grid item sm={4}>
              <InputLabel>Estimate Shipping Date</InputLabel>
            </Grid>
            <Grid item sm={8}>
              <TextField disabled value={order.shippedDate || ""} fullWidth />
            </Grid>
        </Grid>
        <Grid container sx={{mt:2}}>
            <Grid item sm={4}>
              <InputLabel>Total Price</InputLabel>
            </Grid>
            <Grid item sm={8}>
              <TextField disabled value={order.cost || ""} fullWidth />
            </Grid>
        </Grid>
        <Grid container sx={{mt:2}}>
            <Grid item sm={4}>
              <InputLabel>Status</InputLabel>
            </Grid>
            <Grid item sm={8}>
              <Select value={status ? status : order.status} onChange={(e) => setStatus(e.target.value)}          
              fullWidth>
                <MenuItem value="Pending">Pending</MenuItem>
                <MenuItem value="Canceled">Cancel</MenuItem>
                <MenuItem value="Confirmed">Confirmed</MenuItem>

              </Select>
            </Grid>
        </Grid>
        <Button variant='contained' color='success' sx={{float:"right", marginTop: "10px"}} onClick={() => setSuccess(true)}>Update Order Status</Button>
      </Box>
    </Modal>

    {success ? <Modal
        open={success}
        onClose={() => setSuccess(false)}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Grid container>
              <Grid item sm={12}>
               <Typography variant='h4'>Update Successfully</Typography>
              </Grid>
          </Grid>
          </Box>
      </Modal> : null}
  </div>
  )
} */
