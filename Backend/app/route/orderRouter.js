const express = require("express")
const router = express.Router();

const {
  createOrderMiddleware,
  getOrderMiddleware,
  getOrderByIdMiddleware,
  updateOrderMiddleware,
  deleteOrderMiddleware
} = require("../middleware/orderMiddleware")

const {
  getAllOrder,
  createOrder,
  getOrderById,
  updateOrderById,
  deleteOrderById
} = require ("../controller/orderController")

const { verifyToken, checkUser } = require("../middleware/authMiddleware");

router.get("/order",getOrderMiddleware, getAllOrder)
router.post("/order", createOrderMiddleware, createOrder)
router.get("/order/:id", getOrderByIdMiddleware, getOrderById)
router.put("/order/:id", [updateOrderMiddleware,  verifyToken, checkUser], updateOrderById)
router.delete("/order/:id", [deleteOrderMiddleware, verifyToken, checkUser],deleteOrderById)

module.exports = router