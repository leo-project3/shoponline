import { Box, Grid, Modal, Typography } from "@mui/material";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "55%",
  minWidth: "300px",
  borderRadius: "10px",
  bgcolor: "brown",
  border: "1px solid #000",
  boxShadow: 24,
  p: 4,
};

export const ContactModel = ({ open, setOpen, message, name }) => {
  return (
    <>
      <Modal open={open} onClose={() => setOpen(false)}>
        <Box sx={style}>
          <Grid container>
            <Grid item sm={12} textAlign="center">
              <Typography variant="h5">The Message of {name}</Typography>
            </Grid>
            <Grid item sm={12} textAlign="center" mt={2}>
              <Typography>{message}</Typography>
            </Grid>
          </Grid>
        </Box>
      </Modal>
    </>
  );
};
