import { DETAILS_ITEM, FETCH_DETAILS_ITEM } from "../contants/data";

const initialState = {
  id: "",
  item: [],
  type: "",
};

//Control details items
export const detailsReducer = (state = initialState, action) => {
  switch (action.type) {
    case DETAILS_ITEM:
      state.id = action.payload;
      break;
    case FETCH_DETAILS_ITEM:
      state.item = action.item;
      state.type = action.item.type;
      break;
    default:
      break;
  }
  return { ...state };
};
