import { ADD_TO_CART } from "../contants/data";

//Add to cart action from details product page.
export const addToCart = (product, quantity) => {
  return async (dispatch) => {
    const cart = localStorage.getItem("cart")
      ? JSON.parse(localStorage.getItem("cart"))
      : [];
    //Check duplicate product then +- quantity or push new product to localstorage
    const duplicate = cart.filter((cartItem) => cartItem._id === product._id);

    if (duplicate.length === 0) {
      const productToAdd = {
        ...product,
        quantity: quantity,
      };
      cart.push(productToAdd);
      localStorage.setItem("cart", JSON.stringify(cart));
    } else {
      cart.forEach((item) => {
        if (item._id === product._id) {
          if (quantity == 1) {
            item.quantity++;
          } else {
            item.quantity = item.quantity + quantity;
          }
        }
      });
      localStorage.setItem("cart", JSON.stringify(cart));
    }
    dispatch({
      type: ADD_TO_CART,
      payload: cart,
    });
  };
};
