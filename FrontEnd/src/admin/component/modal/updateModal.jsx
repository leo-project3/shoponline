import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import {
  Grid,
  InputLabel,
  TextField,
  Select,
  MenuItem,
  Button,
  Typography,
  InputAdornment,
  FormControl,
} from "@mui/material";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { InputImage } from "./inputImage";
import axios from "axios";
import { ShopToast } from "../../../ToastMessagePopup/toastShop";
import { GET_PRODUCT } from "../../contants/data";
import { EditProfile } from "../../../Component/Body/Profile/editProfileComponent";
const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 650,
  borderRadius: "10px",
  bgcolor: "background.paper",
  border: "1px solid #000",
  boxShadow: 24,
  p: 4,
};
const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

export const UpdateProductModal = ({
  openUpdateModal,
  setOpenUpdateModal,
  product,
  setProduct,
}) => {
  const dispatch = useDispatch();

  const [success, setSuccess] = useState(false);
  const [openSnack, setOpenSnack] = useState(false);
  const [messageSuccess, setMessageSuccess] = useState("");
  const [messageError, setMessageError] = useState("");
  const [data, setData] = useState([]);
  //const [product, setProduct] = useState({})
  const [name, setName] = useState();
  const [type, setType] = useState("");
  const [description, setDescript] = useState();
  const [price, setPrice] = useState(0);
  const [promoPrice, setPromoPrice] = useState(0);
  const [stock, setStock] = useState(0);
  const [image, setImage] = useState("");
  const [error, setError] = useState({});

  useEffect(() => {
    fetch(process.env.REACT_APP_FETCH_URL + "/api/producttype")
      .then((res) => res.json())
      .then((data) => setData(data.productType));
  }, []);
  const typeI = data.map((item) => item.name);
  const filterItem = typeI.filter(
    (item, index) => typeI.indexOf(item) === index,
  );
  const typeItem = data.map((item, index) => {
    return (
      <MenuItem key={item._id} value={item.name}>
        {item.name}
      </MenuItem>
    );
  });
  const validateError = {};
  const validateInput = () => {
    if (!product.name) {
      validateError.name = "Product Name is required";
    }
    if (!product.type) {
      validateError.type = "Product Type is required";
    }
    if (product.price <= 0 && !isNaN(product.price)) {
      validateError.price = "Origin Price must greater than 0";
    }
    if (product.promoPrice <= 0 && !isNaN(product.promoPrice)) {
      validateError.promoPrice = "Promo Price must greater than 0";
    } else if (parseInt(product.promoPrice) > parseInt(product.price)) {
      validateError.promoPrice = "Promo Price must be less than Origin Price";
    }
    setError(validateError);
  };

  const onBtnUpdateProduct = async () => {
    validateInput();
    const customerObj = {
      name: product.name,
      imageUrl: image,
      type: product.type,
      buyPrice: product.buyPrice,
      promotionPrice: product.promotionPrice,
      description: product.description,
      amount: product.amount,
    };
    if (Object.keys(validateError).length === 0) {
      try {
        const response = await axios.put(
          process.env.REACT_APP_FETCH_URL + "/api/product/" + product._id,
          customerObj,
          {
            headers: {
              "x-access-token": sessionStorage.getItem("AccessToken"),
              "x-refresh-token": sessionStorage.getItem("RefreshToken"),
            },
          },
        );
        const data = await response.data;
        const response2 = await axios(
          process.env.REACT_APP_FETCH_URL + "/api/product/",
        );
        const data2 = await response2.data.products;
        if (response.status == 200) {
          setMessageError("");
          setOpenSnack(true);
          setOpenUpdateModal(false);
          setMessageSuccess(data.message);
          dispatch({
            type: GET_PRODUCT,
            payload: data2,
            totalPage: data2.length,
          });
        }
      } catch (error) {
        setOpenSnack(true);
        setMessageError(error.response.data.message);
        if (
          error.response.data.message == "Unauthourized" ||
          error.response.data.message == "Please Login" ||
          error.response.data.message == "Please Login Again"
        ) {
          sessionStorage.clear();
          setTimeout(() => (window.location.href = "/login"), 4000);
        }
      }
    }
  };
  return (
    <div>
      <Modal
        open={openUpdateModal}
        onClose={() => setOpenUpdateModal(false)}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography variant="h4" mb={5} align="center">
            Create New Product
          </Typography>
          <Grid container>
            <Grid item sm={4}>
              <InputLabel>Name</InputLabel>
            </Grid>
            <Grid item sm={8}>
              <TextField
                value={product ? product.name : ""}
                fullWidth
                onChange={(e) =>
                  setProduct({ ...product, name: e.target.value })
                }
              />
              {error.name && <span style={{ color: "red" }}>{error.name}</span>}
            </Grid>
          </Grid>
          <Grid container sx={{ mt: 2 }}>
            <Grid item sm={4}>
              <InputLabel>Image</InputLabel>
            </Grid>
            <Grid item sm={8}>
              <InputImage
                error={error}
                image={
                  Object.keys(product).length != 0
                    ? product.imageUrl.image
                    : image
                }
                product={product}
                setImage={setImage}
              />
            </Grid>
          </Grid>
          <Grid container sx={{ mt: 2 }}>
            <Grid item sm={4}>
              <InputLabel>Type</InputLabel>
            </Grid>
            <Grid item sm={8}>
              <Select
                value={product.type || ""}
                fullWidth
                onChange={(e) =>
                  setProduct({ ...product, type: e.target.value })
                }
                MenuProps={MenuProps}
              >
                {typeItem}
              </Select>
              {error.type && <span style={{ color: "red" }}>{error.type}</span>}
            </Grid>
          </Grid>
          <Grid container sx={{ mt: 2 }}>
            <Grid item sm={4}>
              <InputLabel>Origin Price</InputLabel>
            </Grid>
            <Grid item sm={8}>
              <TextField
                type="number"
                value={product ? product.buyPrice : ""}
                fullWidth
                onChange={(e) =>
                  setProduct({ ...product, buyPrice: e.target.value })
                }
              />
              {error.price && (
                <span style={{ color: "red" }}>{error.price}</span>
              )}
            </Grid>
          </Grid>
          <Grid container sx={{ mt: 2 }}>
            <Grid item sm={4}>
              <InputLabel>Promotion Price</InputLabel>
            </Grid>
            <Grid item sm={8}>
              <TextField
                type="number"
                value={product ? product.promotionPrice : ""}
                fullWidth
                onChange={(e) =>
                  setProduct({ ...product, promotionPrice: e.target.value })
                }
              />
              {error.promoPrice && (
                <span style={{ color: "red" }}>{error.promoPrice}</span>
              )}
            </Grid>
          </Grid>
          <Grid container sx={{ mt: 2 }}>
            <Grid item sm={4}>
              <InputLabel>Stock</InputLabel>
            </Grid>
            <Grid item sm={8}>
              <TextField
                type="number"
                value={product ? product.amount : ""}
                fullWidth
                onChange={(e) =>
                  setProduct({ ...product, amount: e.target.value })
                }
              />
            </Grid>
          </Grid>
          <Grid container sx={{ mt: 2 }}>
            <Grid item sm={4}>
              <InputLabel>Description</InputLabel>
            </Grid>
            <Grid item sm={8}>
              <TextField
                multiline
                rows={5}
                value={product ? product.description : ""}
                fullWidth
                onChange={(e) =>
                  setProduct({ ...product, description: e.target.value })
                }
              />
            </Grid>
          </Grid>

          <Button
            variant="contained"
            color="secondary"
            sx={{ float: "right", marginTop: "10px" }}
            onClick={() => onBtnUpdateProduct()}
          >
            Update Product
          </Button>
        </Box>
      </Modal>

      {success ? (
        <Modal
          open={success}
          onClose={() => setSuccess(false)}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={style}>
            <Grid container>
              <Grid item sm={12}>
                <Typography variant="h4">Update Successfully</Typography>
              </Grid>
            </Grid>
          </Box>
        </Modal>
      ) : null}

      <ShopToast
        open={openSnack}
        setOpen={setOpenSnack}
        messageError={messageError}
        messageSuccess={messageSuccess}
      />
    </div>
  );
};

export const UpdateOrderModal = ({
  openUpdateModal,
  setOpenUpdateModal,
  order,
}) => {
  const dispatch = useDispatch();
  const [success, setSuccess] = useState(false);
  const [status, setStatus] = useState();
  const onBtnChangeStatus = (e) => {
    setStatus(e.target.value);
  };

  return (
    <div>
      <Modal
        open={openUpdateModal}
        onClose={() => setOpenUpdateModal(false)}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Grid container>
            <Grid item sm={4}>
              <InputLabel>Order Code</InputLabel>
            </Grid>
            <Grid item sm={8}>
              <TextField disabled value={order.orderCode || ""} fullWidth />
            </Grid>
          </Grid>
          <Grid container sx={{ mt: 2 }}>
            <Grid item sm={4}>
              <InputLabel>Order Date</InputLabel>
            </Grid>
            <Grid item sm={8}>
              <TextField disabled value={new Date(order.orderDate).toLocaleDateString("en-GB") || ""} fullWidth />
            </Grid>
          </Grid>
          <Grid container sx={{ mt: 2 }}>
            <Grid item sm={4}>
              <InputLabel>Estimate Shipping Date</InputLabel>
            </Grid>
            <Grid item sm={8}>
              <TextField disabled value={new Date(order.shippedDate).toLocaleDateString("en-GB") || ""} fullWidth />
            </Grid>
          </Grid>
          <Grid container sx={{ mt: 2 }}>
            <Grid item sm={4}>
              <InputLabel>Total Price</InputLabel>
            </Grid>
            <Grid item sm={8}>
              <TextField disabled value={order.cost || ""} fullWidth />
            </Grid>
          </Grid>
          <Grid container sx={{ mt: 2 }}>
            <Grid item sm={4}>
              <InputLabel>Status</InputLabel>
            </Grid>
            <Grid item sm={8}>
              <Select
                disabled={status == "Pending" ? false : true}
                value={status ? status : order.status}
                onChange={(e) => setStatus(e.target.value)}
                fullWidth
              >
                <MenuItem value="Pending">Pending</MenuItem>
                <MenuItem value="Canceled">Cancel</MenuItem>
                <MenuItem value="Confirmed">Confirmed</MenuItem>
              </Select>
            </Grid>
          </Grid>
          <Button
            variant="contained"
            color="success"
            sx={{ float: "right", marginTop: "10px" }}
            onClick={() => setSuccess(true)}
          >
            Update Order Status
          </Button>
        </Box>
      </Modal>

      {success ? (
        <Modal
          open={success}
          onClose={() => setSuccess(false)}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={style}>
            <Grid container>
              <Grid item sm={12}>
                <Typography variant="h4">Update Successfully</Typography>
              </Grid>
            </Grid>
          </Box>
        </Modal>
      ) : null}
    </div>
  );
};

export const UpdateCustomerModal = ({
  openUpdateModal,
  setOpenUpdateModal,
  customer
}) => {
  const dispatch = useDispatch();
  const [success, setSuccess] = useState(false);
  const [status, setStatus] = useState();
  const [edit, setEdit] = useState(false)
  const [openSnack, setOpenSnack] = useState(false);
  const [messageSuccess, setMessageSuccess] = useState("");
  const [messageError, setMessageError] = useState("");
  const [customers, setCustomers] = useState({
    fullName: customer?.fullName,
    phone: customer?.phone,
    email: customer?.email,
    address: customer?.address,
    city: customer?.city,
    district: customer?.district,
    ward: customer?.ward,
  })
  const [error, setError] = useState({});
  const validateError = {};

  const validateUpdateData = () => {
    const isValidEmail = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g.test(customers.email);
    const isValidPhone = /(84|0[3|5|7|8|9])+([0-9]{8})\b/g.test(customers.phone);
    //Validate input value
    if (!customers.fullName && customers.fullName != undefined) {
      validateError.fullName = "Fullname is required";
    }
    if (!customers.email && customers.email != undefined) {
      validateError.email = "Email is required";
    } else if (!isValidEmail && customers.email != undefined) {
      validateError.email = "Email is invalid";
    }
    if ((isNaN(customers.phone)) && customers.phone != undefined) {
      validateError.phone = "Phone number is required and must be the number";
    } else if (!isValidPhone && customers.phone != undefined) {
      validateError.phone = "Phone is invalid";
    }
    if (!customers.address && customers.address != undefined) {
      validateError.address = "Address is required";
    }
    setError(validateError);
  }

  const onBtnUpdateCustomer = async () => {
    validateUpdateData()
    if (Object.keys(validateError).length === 0) {
      try {
        const response = await axios.put(
          process.env.REACT_APP_FETCH_URL + "/api/customer/" + customer._id,
          customers,
        );
        const data = await response.data;

        if (response.status == 200) {
          setMessageError("");
          setOpenSnack(true);
          setMessageSuccess(data.message);
          //setTimeout(() => window.location.reload(), 3000);
        }
      } catch (error) {
        setOpenSnack(true);

        setMessageError(error.response.data.message);
      }
    }
  }
  return (
    <>
      <Modal
        open={openUpdateModal}
        onClose={() => setOpenUpdateModal(false)}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <TextField
            variant="standard"
            label="Full Name"
            value={customers.fullName}
            defaultValue={customer.fullName}
            onChange={(e) => setCustomers({ ...customers, fullName: e.target.value })}
            sx={{ mt: 1, mb: 1 }}
            fullWidth
          ></TextField>
          {error.fullname && (
            <span style={{ color: "red" }}>{error.fullname}</span>
          )}
          <TextField
            variant="standard"
            label="Phone"
            onChange={(e) => setCustomers({ ...customers, phone: e.target.value })}
            defaultValue={customer.phone}
            value={customers.phone}
            fullWidth
          ></TextField>
          {error.phone && <span style={{ color: "red" }}>{error.phone}</span>}

          <TextField
            variant="standard"
            label="Email"
            onChange={(e) => setCustomers({ ...customers, email: e.target.value })}
            defaultValue={customer.email}
            value={customers.email}
            sx={{ mt: 1, mb: 1 }}
            fullWidth
          ></TextField>
          {error.email && <span style={{ color: "red" }}>{error.email}</span>}

          <TextField
            variant="standard"
            label="Address"
            onChange={(e) => setCustomers({ ...customers, address: e.target.value })}
            value={customers.address}
            defaultValue={customer.address}
            sx={{ mt: 1, mb: 1 }}
            fullWidth
          ></TextField>
          {error.address && (
            <span style={{ color: "red" }}>{error.address}</span>
          )}
          <TextField
            disabled
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <Typography color="error" sx={{ textDecoration: 'underline' }} onClick={() => setEdit(true)}>Change address</Typography>
                </InputAdornment>)
            }}
            variant="standard"
            label="City"

            value={customer.city}
            sx={{ mt: 1, mb: 1 }}
            fullWidth
          ></TextField>
          <TextField
            disabled
            InputProps={{
              readOnly: true,
            }}
            variant="standard"
            label="District"

            value={customer.district}
            sx={{ mt: 1, mb: 1 }}
            fullWidth
          ></TextField>
          <TextField
            disabled
            InputProps={{
              readOnly: true,
            }}
            variant="standard"
            label="Ward"

            value={customer.ward}
            sx={{ mt: 1, mb: 1 }}
            fullWidth
          ></TextField>
          <Button
            variant="contained"
            color="success"

            onClick={() => onBtnUpdateCustomer()}
          >
            Edit Customer
          </Button>
        </Box>
        {/* 
        
      </Container> */}
      </Modal>

      <EditProfile customers={customers} setCustomers={setCustomers} edit={edit} setEdit={setEdit} id={customer._id} />
      <ShopToast
        open={openSnack}
        setOpen={setOpenSnack}
        messageError={messageError}
        messageSuccess={messageSuccess}
      />
    </>
  );
};