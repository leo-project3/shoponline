const customerModel = require("../model/customerModel");
const orderModel = require("../model/orderModel");

const createCustomerNoOrderMiddleware = async (req, res, next) => {
  console.log("Create Customer Middleware")
  try {
    let condition = {}
    if (req.body.phone) {
      condition.phone = req.body.phone
    }
    if (req.body.email) {
      condition.email = req.body.email
    }
    if (req.body.phone && isNaN(req.body.phone)) {
      return res.status(404).json({
        message: "Phone must be a number"
      })
    }
    const result = await customerModel.findOne({ phone: req.body.phone })
    if (!result) {
      console.log("null")
    } else {

      if (result.phone == req.body.phone) {
        return res.status(404).json({
          message: "Phone number has been used"
        })
      }

      if (result.email == req.body.email) {
        return res.status(404).json({
          message: "Email has been used"
        })
      }

    }
    const result2 = await customerModel.findOne({ email: req.body.email })
    if (!result2) {
      console.log("null")
    } else {
      if (result2.email == req.body.email) {
        return res.status(404).json({
          message: "Email has been used"
        })
      }
    }
    if (!result && !result2) {
      next()
    }
  } catch (error) {
    return res.status(500).json({
      message: "Internal Error" + error.message
    })
  }
  //next();
}
const createCustomerMiddleware = async (req, res, next) => {
  console.log("Create Customer Middleware")
  next();
}
const getCustomerMiddleware = async (req, res, next) => {
  console.log("Get Customer Middleware")
  const result = await customerModel.find()/* .skip(req.query.skip).limit(req.query.limit) */;
  if (result) {
    const cusOrder = await result.map((cus) => cus.orders)
    const checkOrder = await cusOrder.map((order) => order)
    const order = await Promise.all(checkOrder.map(async (order) => {
      const response = await Promise.all(order.map(async (order) => {
        const result = await orderModel.findById(order)
        if (result == null) {
          const response = await customerModel.findOneAndUpdate({ orders: { $in: [order] } }, {
            $pull: { orders: order }
          })
        }
      }))
    }))
  }
  next();
}
const getCustomerByIdMiddleware = async (req, res, next) => {
  console.log("Get Customer by Id Middleware")
  const result = await customerModel.find()/* .skip(req.query.skip).limit(req.query.limit) */;
  if (result) {
    const cusOrder = await result.map((cus) => cus.orders)
    const checkOrder = await cusOrder.map((order) => order)
    const order = await Promise.all(checkOrder.map(async (order) => {
      const response = await Promise.all(order.map(async (order) => {
        const result = await orderModel.findById(order)
        if (result == null) {
          const response = await customerModel.findOneAndUpdate({ orders: { $in: [order] } }, {
            $pull: { orders: order }
          })
        }
      }))
    }))
  }
  next();
}
const updateCustomerMiddleware = async (req, res, next) => {
  console.log("Update Customer Middleware")
  try {
    let condition = {}
    if (req.body.phone) {
      condition.phone = req.body.phone
    }
    if (req.body.email) {
      condition.email = req.body.email
    }
    if (req.body.phone && isNaN(req.body.phone)) {
      return res.status(404).json({
        message: "Phone must be a number"
      })
    }
    const result = await customerModel.findOne({ phone: req.body.phone })
    if (!result) {
      console.log("null")
    } else {

      if (result.phone == req.body.phone) {
        return res.status(404).json({
          message: "Phone number has been used"
        })
      }

      if (result.email == req.body.email) {
        return res.status(404).json({
          message: "Email has been used"
        })
      }

    }
    const result2 = await customerModel.findOne({ email: req.body.email })
    if (!result2) {
      console.log("null")
    } else {
      if (result2.email == req.body.email) {
        return res.status(404).json({
          message: "Email has been used"
        })
      }
    }
    if (!result && !result2) {
      next()
    }
  } catch (error) {
    console.log(error)
    return res.status(500).json({
      message: "Internal Error"
    })
  }
  //next();
}
const deleteCustomerMiddleware = (req, res, next) => {
  console.log("Delete Customer Middleware")
  next();
}

module.exports = {
  createCustomerNoOrderMiddleware,
  createCustomerMiddleware,
  getCustomerMiddleware,
  getCustomerByIdMiddleware,
  updateCustomerMiddleware,
  deleteCustomerMiddleware
}