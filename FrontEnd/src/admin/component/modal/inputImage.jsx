import { TextField } from "@mui/material";
import axios from "axios";
import { ShopToast } from "../../../ToastMessagePopup/toastShop";
import { useEffect, useState } from "react";

export const InputImage = ({ value, error, image, product, setImage }) => {

  const [open, setOpen] = useState(false);
  const [messageError, setMessageError] = useState("");
  const [messageSuccess, setMessageSuccess] = useState("");
  const [previewImg, setPreviewImg] = useState("");
  const postImage = async (e) => {
    const files = e.target.files;
    const formData = new FormData();
    formData.append("name", product?.name)
    for (let i = 0; i < files.length; i++) {
      formData.append(`files`, files[i], files.name)
    }
    /* formData.append("name", file ? file.name : "");
    formData.append("file", file); */

    //let option = Object.fromEntries(formData);

    try {
      const response = await axios.post(
        process.env.REACT_APP_FETCH_URL + "/api/upload-image",
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        },
      );
      const data = await response.data;
      const fetchImg = await data.img._id;
      const imgUrl = await data.img.image;

      setOpen(true);
      setMessageError("");
      setMessageSuccess(data.message);
      setPreviewImg(imgUrl);
      setImage(fetchImg);
    } catch (error) {

      setOpen(true);
      setMessageSuccess("");
      setMessageError(error.response.data.message);
    }
  };

  return (
    <>
      <TextField disabled={!product.name} type="file" fullWidth onChange={(e) => postImage(e)} inputProps={{
        multiple: true
      }} />
      {typeof image == "object" && image != "" ?
        image.map((img) => (
          <img width="100px"
            height="100px"
            style={{ margin: "2px" }}
            src={process.env.REACT_APP_FETCH_URL + "/upload/" + img} />
        ))
        : previewImg ?
          previewImg.map((img) => (
            <img width="100px"
              height="100px"
              style={{ margin: "2px" }}
              src={process.env.REACT_APP_FETCH_URL + "/upload/" + img} />
          )) : null}
      {error.image && <span style={{ color: "red" }}>{error.image}</span>}
      <ShopToast
        messageError={messageError}
        messageSuccess={messageSuccess}
        open={open}
        setOpen={setOpen}
      />
    </>
  );
};
