import { Grid, IconButton, Typography } from "@mui/material";
import FacebookIcon from "@mui/icons-material/Facebook";
import XIcon from "@mui/icons-material/X";
import YouTubeIcon from "@mui/icons-material/YouTube";
const FooterContent = () => {
  const TextFooter = ({ name }) => {
    return (
      <Typography
        sx={{
          "&:hover": {
            width: "100%",
            textDecoration: "underline",
            cursor: "Pointer",
          },
        }}
      >
        {name}
      </Typography>
    );
  };
  return (
    <Grid spacing={4} container padding="10px">
      <Grid item xs={12} sm={3}>
        <Typography variant="h6" component="div">
          <b>ShopOnline</b>
        </Typography>
        <IconButton
          onClick={() => window.open("https://www.facebook.com/", `_blank`)}
        >
          <FacebookIcon fontSize="small" />
        </IconButton>
        <IconButton
          onClick={() => window.open("https://www.twitter.com/", "_blank")}
        >
          <XIcon fontSize="small" />
        </IconButton>
        <IconButton
          onClick={() => window.open("https://www.youtube.com/", "_blank")}
        >
          <YouTubeIcon fontSize="small" />
        </IconButton>
      </Grid>
      <Grid item xs={12} sm={3}>
        <Typography variant="h6">
          <b>INFORMATION</b>
        </Typography>
        <TextFooter name="About Us" />
        <TextFooter name="Brand" />
        <TextFooter name="Product" />
      </Grid>
      <Grid item xs={12} sm={3}>
        <Typography variant="h6">
          <b>TERMS AND POLICY</b>
        </Typography>
        <TextFooter name="Goods Guarantee" />
        <TextFooter name="Delivery Policy" />
        <TextFooter name="Payment Method" />
      </Grid>
      <Grid item xs={12} sm={3}>
        <Typography variant="h6">
          <b>LOCATION</b>
        </Typography>
        <TextFooter name="111 Ave, Ward 12, ABV Dst" />
        <TextFooter name="(+84) 0901 222 444" />
        <TextFooter name="Sameple@devcampshop.com" />
      </Grid>
    </Grid>
  );
};

export default FooterContent;
