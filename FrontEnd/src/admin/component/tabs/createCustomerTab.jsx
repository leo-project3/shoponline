import { Alert, Box, Modal, Snackbar } from "@mui/material";
import {
  Container,
  Grid,
  TextField,
  Typography,
  Button,
  Select,
  MenuItem,
  InputLabel,
  FormControl
} from "@mui/material";
import axios from "axios";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { SelectLocation } from "../../../Component/Body/Global/locationSelect";
import { ShopToast } from "../../../ToastMessagePopup/toastShop";

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};
export const CreateCustomerTab = ({ edit, setEdit, id }) => {
  const style = {
    width: "100%",
    borderRadius: "10px",
    bgcolor: "background.paper",
    border: "1px solid #000",
    boxShadow: 24,
    p: "50px",
    marginTop: "10px",
  };


  const [openSnack, setOpenSnack] = useState(false);
  const [messageSuccess, setMessageSuccess] = useState("");
  const [messageError, setMessageError] = useState("");
  const [userNameArr, setUserNameArr] = useState([])
  const [userName, setUserName] = useState("")
  const [fullname, setName] = useState("");
  const [phone, setPhone] = useState("");
  const [email, setEmail] = useState("");
  const [address, setAddress] = useState("");
  const [city, setCity] = useState("");
  const [district, setDistrict] = useState("");
  const [ward, setWard] = useState("");
  const [error, setError] = useState({});
  const validateError = {};

  useEffect(() => {
    GetUser()
  }, [])
  const menuItem = userNameArr.map((userName) => {
    return (
      <MenuItem key={userName} value={userName}>{userName.username}</MenuItem>
    )
  })

  const validateData = () => {
    const isValidEmail = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g.test(email);
    const isValidPhone = /(84|0[3|5|7|8|9])+([0-9]{8})\b/g.test(phone);
    //Validate input value
    if (!fullname.trim()) {
      validateError.fullname = "Fullname is required";
    }
    if (!email.trim()) {
      validateError.email = "Email is required";
    } else if (!isValidEmail) {
      validateError.email = "Email is invalid";
    }
    if (!phone.trim() || isNaN(phone)) {
      validateError.phone = "Phone number is required and must be the number";
    } else if (!isValidPhone) {
      validateError.phone = "Phone is invalid";
    }
    if (!address.trim()) {
      validateError.address = "Address is required";
    }
    if (!city) {
      validateError.city = "City is required";
    }
    if (!district) {
      validateError.district = "District is required";
    }
    if (!ward) {
      validateError.ward = "Ward is required";
    }
    setError(validateError);
  };
  const onBtnUpdateInformationClick = async () => {
    validateData();
    const customerObj = {
      fullName: fullname,
      phone: phone,
      email: email,
      userId: userName,
      address: address,
      city: city,
      district: district,
      ward: ward,
    };
    if (Object.keys(validateError).length === 0) {
      try {
        const response = await axios.post(
          process.env.REACT_APP_FETCH_URL + "/api/customernoorder/",
          customerObj,
        );
        const data = await response.data;

        if (response.status == 201) {
          setMessageError("");
          setOpenSnack(true);
          setMessageSuccess(data.message);
          //setTimeout(() => window.location.reload(), 5000)
        }
      } catch (error) {
        setOpenSnack(true);

        setMessageError(error.response.data.message);
      }
    }
  };

  const GetUser = async (e) => {
    const user = await axios(process.env.REACT_APP_FETCH_URL + '/api/user/')
    const userId = await user.data.data
    const customer = await axios(process.env.REACT_APP_FETCH_URL + '/api/customer/')
    const allCus = await customer.data.Customer
    const response = await allCus.map((cus) => {
      const nonCusUser = userId.map((user) => {
        if (user._id == cus.userId._id) {
          let indexUser = userId.indexOf(user)
          let nonUserCus = userId.splice(indexUser, 1)
          setUserNameArr(userId)
        }
      }
      )
    })
  }

  return (
    <>
      <Box sx={style} style={{ minWidth: "300px", width: "100%" }}>
        <Typography
          variant="h4"
          component="div"
          textAlign="center"
          marginBottom={3}
        >
          Create Customer
        </Typography>

        <Grid container spacing={2}>
          <Grid item sm={6}>
            <TextField
              label="Full Name"
              value={fullname || ""}
              onChange={(e) => setName(e.target.value)}
              fullWidth
              sx={{
                "&& .Mui-focused": {
                  color: "White",
                },
              }}
            ></TextField>
            {error.fullname && (
              <span style={{ color: "red" }}>{error.fullname}</span>
            )}
          </Grid>
          <Grid item sm={6}>
            <TextField
              label="Phone"
              value={phone || ""}
              onChange={(e) => setPhone(e.target.value)}
              fullWidth
              sx={{
                "&& .Mui-focused": {
                  color: "White",
                },
              }}
            ></TextField>
            {error.phone && <span style={{ color: "red" }}>{error.phone}</span>}
          </Grid>
        </Grid>
        <Grid container spacing={2} sx={{ mt: 2 }}>
          <Grid item sm={6}>
            <TextField
              label="Email"
              value={email || ""}
              onChange={(e) => setEmail(e.target.value)}
              fullWidth
              sx={{
                "&& .Mui-focused": {
                  color: "White",
                },
              }}
            ></TextField>
            {error.email && <span style={{ color: "red" }}>{error.email}</span>}
          </Grid>
          <Grid item sm={6}>
            <TextField
              label="Address"
              value={address || ""}
              onChange={(e) => setAddress(e.target.value)}
              fullWidth
              sx={{
                "&& .Mui-focused": {
                  color: "White",
                },
              }}
            ></TextField>
            {error.address && (
              <span style={{ color: "red" }}>{error.address}</span>
            )}
          </Grid>
        </Grid>
        <Grid container spacing={2} sx={{ mt: 2, alignItems: "center" }}>
          <Grid item sm={6}>
            <SelectLocation
              error={error}
              setCity={setCity}
              setDistrict={setDistrict}
              city={city}
              ward={ward}
              setWard={setWard}
              district={district}
              color="white"
            />
          </Grid>
          <Grid item sm={6} sx={{ display: "flex", flexDirection: "column" }}>
            <FormControl>
              <InputLabel sx={{
                "&.Mui-focused": {
                  color: "White",
                }
              }}>User Name</InputLabel>
              <Select label="User Name" value={userName || ""} onChange={(e) => setUserName(e.target.value)} sx={{
                marginBottom: "20px",
              }} fullWidth MenuProps={MenuProps} >
                {menuItem}
              </Select>
            </FormControl>
            <Button
              size="large"
              variant="contained"
              color="success"
              onClick={() => onBtnUpdateInformationClick()}
            >
              Create Customer Information
            </Button>
          </Grid>
        </Grid>
      </Box>
      <ShopToast
        open={openSnack}
        setOpen={setOpenSnack}
        messageError={messageError}
        messageSuccess={messageSuccess}
      />
    </>
  );
};
