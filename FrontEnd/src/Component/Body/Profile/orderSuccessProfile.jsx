import {
  Grid,
  Typography,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableBody,
  TableCell,
  Paper,
  Button,
  Box,
  Container,
  Divider,
  TableFooter,
  Chip,
} from "@mui/material";
import axios from "axios"
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getCustomerOrder } from "../../../action/getExistedUser";
import { getDetailsProduct } from "../../../admin/action/getOrderAction";
import { ViewDetailsProduct, ViewDetailsProductShop } from "../../../admin/component/modal/viewDetailsProduct";
import { NoProductFound } from "../No Found Component/noProductFound";

export const OrderSuccess = () => {
  const dispatch = useDispatch();
  const { orderArr } = useSelector((reduxData) => reduxData.checkOutReducer);
  const [openToast, setOpenToast] = useState(false)
  const [openProductModal, setProductModal] = useState(false);
  const [order, setOrder] = useState()
  const [customer, setCustomer] = useState()
  const user = sessionStorage.getItem("User");
  useEffect(() => {
    dispatch(getCustomerOrder(user));
  }, []);
  //Get information to display on order details
  const getCustomerByUserId = async (order) => {
    const response = await axios(process.env.REACT_APP_FETCH_URL + "/api/customer/" + order.customerId)
    const data = await response.data.Customer
    setCustomer(data)

  }

  //Check details order
  const onBtnClickDetailsProduct = (orderCode) => {
    // dispatch(getDetailsProduct(orderCode, setOpenToast));
    setOrder(orderCode)
    getCustomerByUserId(orderCode)
    setProductModal(true);
  };
  //Order successfully created cards
  const card = orderArr.map((order, index) => {
    return (

      <Container key={order._id} sx={{ marginBottom: "5px", backgroundColor: "whitesmoke" }} onClick={() => onBtnClickDetailsProduct(order)}>
        <Box sx={{ padding: "5px" }}>
          <Chip

            color={
              order.status == "Pending"
                ? "primary"
                : order.status == "Confirmed"
                  ? "success"
                  : "error"
            }
            label={order.status}
          />
        </Box>
        <Divider sx={{ borderColor: "black" }} />
        <Container sx={{ display: "flex", justifyContent: "flex-start", alignItems: "center", margin: "0 0 5px 0", padding: "5px 0 5px 0" }}>
          <img loading="lazy" src={process.env.REACT_APP_FETCH_URL + '/upload/' + order.orderDetails[0].product.imageUrl.image[0]} alt="product-img" width="75px" height='75px' />
          <Box sx={{ width: "100%", padding: "0 0 0 5px" }}>
            <Typography fontWeight='bold'>{order.orderDetails[0].product.name}</Typography>
            <Box sx={{ display: "flex", justifyContent: "space-between" }}>
              <Typography component="div">Qty: {order.orderDetails[0].quantity}</Typography>
              <Typography component="div" align="center" sx={{ color: "" }}>
                <b>{order.cost.toLocaleString()} VND</b>
              </Typography>
            </Box>
            <Box sx={{ display: 'flex', alignItems: "center", justifyContent: "space-between" }}>
              {order.orderDetails.length > 1 ? <Typography>Total: {order.orderDetails.length} products</Typography> : null}
              {order.orderDetails.length > 1 ? <Button sx={{ padding: 0 }} onClick={() => onBtnClickDetailsProduct(order)}>View Details</Button> : null}
            </Box>
          </Box>
        </Container >
      </Container>
    );
  });
  return (
    <>
      <Grid
        item
        md={9}
        xs={12}
        style={{
          minWidth: "320px",
          maxHeight: "520px",
        }}
        sx={{
          display: "flex",
          flexDirection: "Column",
          backgroundColor: "white",
          borderRadius: "20px",
          padding: "10px",
          maxWidth: { xs: "470px", md: "600px", lg: "800px" },
        }}
      >
        <Typography align="center" variant="h5" component="div">
          Order History
        </Typography>
        {orderArr.length == 0 ? (
          <Box textAlign='center'>
            <Typography color='error' fontWeight='bold' variant="h5">
              No order has been found
            </Typography>
          </Box>
        ) : (<Box sx={{ overflowY: "scroll" }}>
          {card}
        </Box>)}

      </Grid>
      <ViewDetailsProductShop customer={customer} order={order} open={openProductModal} setOpen={setProductModal} />

    </>
  );
};
