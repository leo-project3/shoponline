const createProductTypeMiddleware = (req, res, next) => {
  console.log("Create ProductType Middleware")
  next();
}
const getProductTypeMiddleware = (req, res, next) => {
  console.log("Get ProductType Middleware")
  next();
}
const getProductTypeByIdMiddleware = (req, res, next) => {
  console.log("Get ProductType by Id Middleware")
  next();
}
const updateProductTypeMiddleware = (req, res, next) => {
  console.log("Update ProductType Middleware")
  next();
}
const deleteProductTypeMiddleware = (req, res, next) => {
  console.log("Delete ProductType Middleware")
  next();
}

module.exports = {
  createProductTypeMiddleware,
  getProductTypeMiddleware,
  getProductTypeByIdMiddleware,
  updateProductTypeMiddleware,
  deleteProductTypeMiddleware,
}