const express = require("express")
const router = express.Router();

const {
  createOrderDetailsMiddleware,
  getOrderDetailsMiddleware,
  getOrderDetailsByIdMiddleware,
  updateOrderDetailsMiddleware,
  deleteOrderDetailsMiddleware
} = require("../middleware/orderDetailsMiddleware")

const {
  getAllOrderDetails,
  createOrderDetails,
  getOrderDetailsById,
  updateOrderDetailsById,
  deleteOrderDetailsById
} = require ("../controller/orderDetailsController")

const { verifyToken, checkUser } = require("../middleware/authMiddleware");

router.get("/orderDetails",getOrderDetailsMiddleware, getAllOrderDetails)
router.post("/orderDetails", createOrderDetailsMiddleware, createOrderDetails)
router.get("/orderDetails/:id", getOrderDetailsByIdMiddleware, getOrderDetailsById)
router.put("/orderDetails/:id",updateOrderDetailsMiddleware, updateOrderDetailsById)
router.delete("/orderDetails/:id", [deleteOrderDetailsMiddleware, verifyToken, checkUser],deleteOrderDetailsById)

module.exports = router