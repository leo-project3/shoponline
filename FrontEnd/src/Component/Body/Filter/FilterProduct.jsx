import {
  Box,
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Typography,
  TextField,
  Grid,
} from "@mui/material";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchFilterData } from "../../../action/filterAction";
import IconButton from "@mui/material/IconButton";
import Menu from "@mui/material/Menu";
import MenuIcon from "@mui/icons-material/Menu";
import { getDataItem } from "../../../action/dataListAction";
import { RESET_PAGE_FILTER } from "../../../contants/data";
import RangeSlider from "./silder";

export const FilterBar = () => {
  const { pending, items, pageFilter, page, limit } = useSelector(
    (reduxData) => reduxData.dataReducer);
  const [data, setData] = useState([]);
  useEffect(() => {
    fetch(process.env.REACT_APP_FETCH_URL + "/api/product" + process.env.REACT_APP_LIMIT_PRODUCT)
      .then((res) => res.json())
      .then((data) => setData(data.products));
  }, []);
  const typeItem = data.map((item) => item.type);
  const filterItem = typeItem.filter(
    (item, index) => typeItem.indexOf(item) === index,
  );
  const filterItemMenu = filterItem.map((item, index) => {
    return (
      <MenuItem key={item} value={item}>
        {item}
      </MenuItem>
    );
  });

  const [anchorElNav, setAnchorElNav] = useState(null);
  const [anchorElUser, setAnchorElUser] = useState(null);
  const [value, setValue] = useState([0, 0]);
  //
  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const dispatch = useDispatch();
  const [product, setProduct] = useState("");
  const [type, setType] = useState("");
  const [mnPrice, setMnPrice] = useState("");
  const [mxPrice, setMxPrice] = useState("");

  //function filter product
  const onBtnFilterClick = async () => {
    if (!product && !type && !mnPrice && !mxPrice) {
      dispatch(getDataItem(page, limit))
    } else {
      await dispatch({
        type: RESET_PAGE_FILTER,
        pageFilter: 1
      })
      if (pageFilter == 1) {
        dispatch(
          fetchFilterData(product, type, mnPrice, mxPrice, pageFilter, limit));
      }
    }
  }

  //Trigger when filter page change
  useEffect(() => {
    dispatch(fetchFilterData(product, type, mnPrice, mxPrice, pageFilter, limit));
  }, [pageFilter])
  return (
    <>
      <Box
        sx={{
          flexGrow: 1,
          display: { xs: "flex", lg: "none" },
          padding: "0 0 0 0",
        }}
      >
        <IconButton
          size="large"
          aria-label="account of current user"
          aria-controls="menu-appbar"
          aria-haspopup="true"
          onClick={handleOpenNavMenu}
          color="inherit"
          style={{
            padding: 0,
            borderRadius: "15px",
          }}
        >
          <MenuIcon />
        </IconButton>
        <Menu
          id="menu-appbar"
          anchorEl={anchorElNav}
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "left",
          }}
          keepMounted
          transformOrigin={{
            vertical: "top",
            horizontal: "left",
          }}
          open={Boolean(anchorElNav)}
          onClose={handleCloseNavMenu}
          sx={{
            display: { xs: "block", lg: "none" },
            "& .MuiList-root": {
              padding: "0 0 0 0",
            },
            "& .MuiPaper-root": {
              borderRadius: "15px",
            },
          }}
        >
          <Box
            sx={{
              minWidth: 120,
              backgroundColor: "#F0F0F0",
              padding: "10px",
              borderRadius: "15px",
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            <Grid
              container
              spacing={3}
              sx={{
                display: "flex",
                alignItems: "flex-start",
                flexDirection: "column",
              }}
            >
              <Grid item>
                <FormControl>
                  <InputLabel id="demo-simple-select-label">Type</InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={type}
                    onChange={(e) => setType(e.target.value)}
                    label="Type"
                    sx={{ minWidth: "100px" }}
                  >
                    <MenuItem value="">Choose 1 Type</MenuItem>
                    {filterItemMenu}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item>
                <TextField
                  label="Product name"
                  value={product}
                  onChange={(e) => setProduct(e.target.value)}
                ></TextField>
              </Grid>
              <Grid
                item
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "flex-start",
                  margin: "auto",
                }}
              >
                {/*  <TextField label="min-price" type="number" value={mnPrice} onChange={(e) => setMnPrice(e.target.value)}></TextField> &nbsp;
       <TextField label="max-price" type="number" value={mxPrice} onChange={(e) => setMxPrice(e.target.value)}></TextField> */}
                <RangeSlider
                  value={value}
                  setMnPrice={setMnPrice}
                  setMxPrice={setMxPrice}
                  setValue={setValue}
                />
              </Grid>
              <Grid item>
                <Button
                  variant="contained"
                  size="large"
                  onClick={onBtnFilterClick}
                >
                  Filter
                </Button>
              </Grid>
            </Grid>
          </Box>
        </Menu>
      </Box>
      <Box sx={{ flexGrow: 1, display: { xs: "none", lg: "flex" } }}>
        <Box
          sx={{
            width: "100%",
            minWidth: 120,
            backgroundColor: "#F0F0F0",
            padding: "10px",
            borderRadius: "15px",
          }}
        >
          <Grid
            container
            spacing={3}
            sx={{ justifyContent: "space-around", alignItems: "center" }}
          >
            <Grid item>
              <FormControl>
                <InputLabel id="demo-simple-select-label">Type</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={type}
                  onChange={(e) => setType(e.target.value)}
                  label="Type"
                  sx={{ minWidth: "100px" }}
                >
                  <MenuItem value="">Choose 1 Type</MenuItem>
                  {filterItemMenu}
                </Select>
              </FormControl>
            </Grid>
            <Grid item width="20rem">
              <TextField
                fullWidth
                label="Product name"
                value={product}
                onChange={(e) => setProduct(e.target.value)}
              ></TextField>
            </Grid>
            <Grid
              item
              sx={{
                display: "flex",
                flexDirection: "row",
                alignItems: "Center",
              }}
            >
              {/* <TextField label="min-price" type="number" value={mnPrice} onChange={(e) => setMnPrice(e.target.value)}></TextField> &nbsp;
       <Typography> - </Typography> &nbsp;
       <TextField label="max-price" type="number" value={mxPrice} onChange={(e) => setMxPrice(e.target.value)}></TextField> */}
              <RangeSlider
                value={value}
                setMnPrice={setMnPrice}
                setMxPrice={setMxPrice}
                setValue={setValue}
              />
            </Grid>
            <Grid item>
              <Button
                variant="contained"
                size="large"
                onClick={onBtnFilterClick}
              >
                Filter
              </Button>
            </Grid>
          </Grid>
        </Box>
      </Box>
    </>
  );
};
