import { Alert, Box, IconButton, Modal, Snackbar } from "@mui/material";
import {
  Container,
  Grid,
  TextField,
  Typography,
  Button,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableBody,
  TableCell,
  Paper,
  Select,
  InputLabel,
} from "@mui/material";
import axios from "axios";
import { useEffect, useState } from "react";
import DeleteIcon from "@mui/icons-material/DeleteOutlined";
import { useDispatch, useSelector } from "react-redux";
import { SelectLocation } from "../../../Component/Body/Global/locationSelect";
import { ShopToast } from "../../../ToastMessagePopup/toastShop";
import { getProductType } from "../../action/getOrderAction";
import { red } from "@mui/material/colors";
import { GET_CATEGORY } from "../../contants/data";

export const CreateCategoryTab = () => {
  const style = {
    width: "100%",
    borderRadius: "10px",
    bgcolor: "background.paper",
    border: "1px solid #000",
    boxShadow: 24,
    p: "50px",
    marginTop: "10px",
  };
  const { categories } = useSelector((reduxData) => reduxData.tableReducer);
  const [openSnack, setOpenSnack] = useState(false);
  const [messageSuccess, setMessageSuccess] = useState("");
  const [messageError, setMessageError] = useState("");
  const [category, setCategory] = useState("");
  const [error, setError] = useState({});
  const dispatch = useDispatch();
  const validateError = {};

  useEffect(() => {
    dispatch(getProductType());
  }, []);
  const validateData = () => {
    //Validate input value
    if (!category.trim()) {
      validateError.category = "Category is required";
    }
    setError(validateError);
  };
  const onBtnCreateCategory = async (e) => {
    if (e.keyCode == 13) {
      validateData();
      const categoryObj = {
        name: category,
      };
      if (Object.keys(validateError).length === 0) {
        try {
          const response = await axios.post(
            process.env.REACT_APP_FETCH_URL + "/api/producttype/",
            categoryObj,
            {
              headers: {
                "x-access-token": sessionStorage.getItem("AccessToken"),
                "x-refresh-token": sessionStorage.getItem("RefreshToken"),
              },
            },
          );
          const response2 = await axios(
            process.env.REACT_APP_FETCH_URL + "/api/producttype/",
          );
          const data2 = await response2.data.productType;
          const data = await response.data;

          if (response.status == 201) {
            setMessageError("");
            setOpenSnack(true);
            setMessageSuccess(data.message);
            dispatch({
              type: GET_CATEGORY,
              category: data2,
            });
          }
        } catch (error) {
          setOpenSnack(true);
          setMessageError(error.response.data.message);
          if (
            error.response.data.message == "Unauthourized" ||
            error.response.data.message == "Please Login" ||
            error.response.data.message == "Please Login Again"
          ) {
            setTimeout(() => (window.location.href = "/login"), 4000);
          }
        }
      }
    }
  };

  const onBtnDeleteCategory = async (id) => {
    try {
      const response = await axios.delete(
        process.env.REACT_APP_FETCH_URL + "/api/producttype/" + id,
        {
          headers: {
            "x-access-token": sessionStorage.getItem("AccessToken"),
            "x-refresh-token": sessionStorage.getItem("RefreshToken"),
          },
        },
      );
      const response2 = await axios(
        process.env.REACT_APP_FETCH_URL + "/api/producttype/",
      );
      const data2 = await response2.data.productType;
      const data = await response.data;

      if (response.status == 200) {
        setMessageError("");
        setOpenSnack(true);
        setMessageSuccess(data.message);
        dispatch({
          type: GET_CATEGORY,
          category: data2,
        });
      }
    } catch (error) {
      setOpenSnack(true);
      setMessageError(error.response.data.message);
      if (
        error.response.data.message == "Unauthourized" ||
        error.response.data.message == "Please Login" ||
        error.response.data.message == "Please Login Again"
      ) {
        setTimeout(() => (window.location.href = "/login"), 4000);
      }
    }
  };
  return (
    <>
      <Box sx={style} style={{ minWidth: "300px", width: "100%" }}>
        <Grid container spacing={3}>
          <Grid item md={6}>
            <Grid item sm={12}>
              <Typography variant="h5" component="div" marginBottom={3}>
                Create Category
              </Typography>
              <TextField
                label="Category"
                value={category || ""}
                onChange={(e) => setCategory(e.target.value)}
                sx={{
                  "& .Mui-focused": {
                    color: "White",
                  },
                  width: "50%",
                  maxWidth: "20rem",
                }}
                onKeyDown={(e) => onBtnCreateCategory(e)}
              ></TextField>
            </Grid>
            {error.category && (
              <span style={{ color: "red", marginLeft: "17px" }}>
                {error.category}
              </span>
            )}
          </Grid>
          <Grid item md={6}>
            <Grid item sm={12}>
              <Typography
                variant="h5"
                component="div"
                textAlign="center"
                marginBottom={3}
              >
                All Category
              </Typography>
              <TableContainer
                component={Paper}
                sx={{
                  maxHeight: "350px",
                }}
              >
                <Table>
                  <TableHead>
                    <TableRow>
                      <TableCell>Id</TableCell>
                      <TableCell align="center">
                        <b>Category</b>
                      </TableCell>
                      <TableCell align="center">
                        <b>Action</b>
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {categories
                      ? categories.map((category, i) => {
                        return (
                          <TableRow key={category._id}>
                            <TableCell>{i + 1}</TableCell>
                            <TableCell align="center">
                              {category.name}
                            </TableCell>
                            <TableCell align="center">
                              <IconButton
                                onClick={() =>
                                  onBtnDeleteCategory(category._id)
                                }
                              >
                                <DeleteIcon />
                              </IconButton>
                            </TableCell>
                          </TableRow>
                        );
                      })
                      : null}
                  </TableBody>
                </Table>
              </TableContainer>
            </Grid>
          </Grid>
        </Grid>
      </Box>
      <ShopToast
        open={openSnack}
        setOpen={setOpenSnack}
        messageError={messageError}
        messageSuccess={messageSuccess}
      />
    </>
  );
};
