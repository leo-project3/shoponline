import { Select, MenuItem, Grid, FormControl, InputLabel } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { LimitChange } from "../../action/getOrderAction";
export const SelectLimit = () => {
  const { limit } = useSelector((reduxData) => reduxData.tableReducer);
  const dispatch = useDispatch();
  const onBtnChangeLimit = (e) => {
    dispatch(LimitChange(e.target.value));
  };
  return (
    <Grid item width={75} bgcolor="white">
      <FormControl width={75} bgcolor="white">
        <InputLabel>Limit</InputLabel>
        <Select
          label="Limit"
          value={limit}
          fullWidth
          onChange={(e) => onBtnChangeLimit(e)}
          sx={{ width: "75px" }}
        >
          <MenuItem value={5}>5</MenuItem>
          <MenuItem value={10}>10</MenuItem>
          <MenuItem value={15}>15</MenuItem>
        </Select>
      </FormControl>
    </Grid>
  );
};
