import {
  Box,
  Button,
  Grid,
  IconButton,
  Tab,
  Tabs,
  Typography,
  useTheme,
} from "@mui/material";
import Header from "../global/header";
import { tokens } from "../../../theme";
import { useEffect, useState } from "react";
import { TabContext, TabList, TabPanel } from "@mui/lab";
import { CreateProductModal } from "../modal/createModal";
import { CreateProductTab } from "../tabs/createProductTab";
import { CreateCustomerTab } from "../tabs/createCustomerTab";
import { CreateCategoryTab } from "../tabs/createCategoryTab";
import { CreateOrderTab } from "../tabs/createOrderTab";

export const CreateComponent = () => {
  const theme = useTheme();
  const colors = tokens(theme.palette.mode);

  const [value, setValue] = useState("0");
  const [open, setOpen] = useState(false);
  const handleChange = (event, newValue) => {
    setValue(newValue);
    setOpen(true);
  };

  return (
    <>
      <Box m="20px">
        {/* HEADER */}
        <Box display="flex" justifyContent="space-between" alignItems="center">
          <Header
            title="CREATE TESTING"
            subtitle="Welcome to your Create Page"
          />
        </Box>

        <Box sx={{ width: "100%", typography: "body1" }}>
          <TabContext value={value}>
            <Box
              sx={{ borderBottom: 1, borderColor: "divider", color: "White" }}
            >
              <TabList
                onChange={handleChange}
                aria-label="lab API tabs example"
                textColor="inherit"
                sx={{
                  "& .Mui-selected": {
                    borderBottom: "2px solid white",
                    marginBottom: "5px",
                  },

                  "& .MuiButtonBase-root": {
                    color: "white",
                    backgroundColor: "black",
                  },
                }}
              >
                <Tab label="Create Customer" value="0" />
                <Tab label="Create Product" value="1" />
                <Tab label="Create Order" value="2" />
                <Tab label="Create Category" value="3" />
              </TabList>
            </Box>
            <TabPanel value="0">
              <CreateCustomerTab />
            </TabPanel>
            <TabPanel value="1">
              <CreateProductTab />
            </TabPanel>
            <TabPanel value="2">
              <CreateOrderTab />
            </TabPanel>
            <TabPanel value="3">
              <CreateCategoryTab />
            </TabPanel>
          </TabContext>
        </Box>
      </Box>
    </>
  );
};
