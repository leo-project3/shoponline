import Box from "@mui/material/Box";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Menu from "@mui/material/Menu";
import Avatar from "@mui/material/Avatar";
import Tooltip from "@mui/material/Tooltip";
import MenuItem from "@mui/material/MenuItem";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getUser } from "../../../action/userAction";
import { Link, useNavigate } from "react-router-dom";
import { ChangePassWord } from "../../../admin/component/modal/changePassword";

export const AvatarHeader = ({
  handleCloseUserMenu,
  anchorElUser,
  handleOpenUserMenu,
}) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { isAdmin } = useSelector((reduxData) => reduxData.userReducer);
  const settings = ["Profile", "Change Password", "Logout"];
  const id = sessionStorage.getItem("User");
  const [open, setOpen] = useState(false);

  useEffect(() => {
    dispatch(getUser(id));
  }, [dispatch]);

  const onBtnLogout = (setting) => {
    if (setting == "Logout") {
      sessionStorage.clear();
      localStorage.clear();
      navigate("/login");
    }
    if (setting == "Profile") {
      navigate("/profile");
    }
    if (setting == "Change Password") {
      handleCloseUserMenu();
      setOpen(true);
    }
  };
  return (
    <>
      <Box sx={{ flexGrow: 0 }}>
        <Tooltip title="Open settings">
          <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
            <Avatar
              sx={{
                width: { xs: "30px", sm: "40px" },
                height: { xs: "30px", sm: "40px" },
              }}
            />
          </IconButton>
        </Tooltip>
        <Menu
          sx={{ mt: "45px" }}
          id="menu-appbar"
          anchorEl={anchorElUser}
          anchorOrigin={{
            vertical: "top",
            horizontal: "right",
          }}
          keepMounted
          transformOrigin={{
            vertical: "top",
            horizontal: "right",
          }}
          open={Boolean(anchorElUser)}
          onClose={handleCloseUserMenu}
        >
          {isAdmin && (
            <MenuItem>
              <Link
                style={{ textDecoration: "none", color: "black" }}
                to="/admin/dashboard"
              >
                <Typography textAlign="center">Dashboard</Typography>
              </Link>
            </MenuItem>
          )}
          {settings.map((setting) => (
            <MenuItem key={setting} onClick={() => onBtnLogout(setting)}>
              <Typography textAlign="center">{setting}</Typography>
            </MenuItem>
          ))}
        </Menu>
      </Box>
      <ChangePassWord open={open} setOpen={setOpen} />
    </>
  );
};
