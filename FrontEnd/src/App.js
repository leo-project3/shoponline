import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import "./App.css";
import { ShopPage24h } from "./Page/Shop24hPage";
//import { ShopPage24h } from "./Page/Shop24hPage";

function App() {
  return (
    <>
      {/*If accestoken available in Session Storage Page will turn to Shop Page. Logout function to revoke accesstoken is in development */}
      <ShopPage24h />
    </>
  );
}

export default App;
