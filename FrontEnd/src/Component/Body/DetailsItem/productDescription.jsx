import {
  Button,
  Grid,
  Rating,
  Typography,
  OutlinedInput,
  IconButton,
  InputAdornment,
  Fade,
  Divider,
  List,
  ListItem,
  ListItemText,
} from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { useState } from "react";
import Box from "@mui/material/Box";
import Snackbar from "@mui/material/Snackbar";
import AddIcon from "@mui/icons-material/Add";
import Alert from "@mui/material/Alert";
import RemoveIcon from "@mui/icons-material/Remove";
import { addToCart } from "../../../action/addToCartAction";
import { useLocation, useNavigate } from "react-router-dom";
import { ShopToast } from "../../../ToastMessagePopup/toastShop";

const style = {
  py: 0,
  width: "100%",
  maxWidth: 360,
  borderRadius: 2,
  border: "1px solid",
  borderColor: "divider",
};

export const Description = () => {
  const navigate = useNavigate();
  const { item } = useSelector((reduxData) => reduxData.detailsReducer);

  const location = useLocation();
  return (
    <>
      {/* <Snackbar open={open} anchorOrigin={{horizontal: "right", vertical: "top"}} autoHideDuration={6000} onClose={() => setOpen(false)} TransitionComponent={Fade}>
        <Alert
          onClose={() => setOpen(false)}
          severity="success"
          variant="filled"
          sx={{ width: '100%' }}
        >
          Add Product to cart successfully
        </Alert>
      </Snackbar> */}
      <Grid
        container
        spacing={3}
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Grid
          item
          md={8}
          sx={{
            display: "flex",
            justifyContent: "center",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Box width="100%">
            <Typography
              textAlign="center"
              color="error"
              fontWeight="bold"
              variant="h5"
            >
              Features
            </Typography>
            <List
              sx={{
                maxWidth: "100%",
                backgroundColor: "rgb(179 119 119 / 25%)",
                borderRadius: "10px",
              }}
            >
              <ListItem>
                <ListItemText>
                  Interactive widgets. Widgets on your Home Screen, Lock Screen,
                  and StandBy are even more useful with interactivity. Just tap
                  a widget to complete tasks like checking off a to-do item,
                  controlling your living room lights, or playing a new podcast
                  episode
                </ListItemText>
              </ListItem>
              <ListItem>
                <ListItemText>
                  Create a unique screen that other people see when you call
                  them. When you make a phone call, your Contact Poster appears
                  on the screen of the person you're calling.{" "}
                </ListItemText>
              </ListItem>
              <ListItem>
                <ListItemText>
                  {typeof location.state == "object"
                    ? location.state.description
                    : item.description}{" "}
                </ListItemText>
              </ListItem>
              <ListItem>
                <ListItemText>
                  When a call you receive goes to voicemail, see a live
                  transcript of the message as it's being recorded, giving you
                  immediate context for the call. If it’s something you want to
                  address right then, you can pick up the call (U.S. and Canada
                  only){" "}
                </ListItemText>
              </ListItem>
            </List>
          </Box>
        </Grid>
        <Grid
          item
          md={4}
          sx={{
            minWidth: "300px",
            width: "70%",
            display: "flex",
            justifyContent: "center",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Typography variant="h5" fontWeight="bold">
            Technical Information
          </Typography>
          <List
            sx={{ style }}
            style={{
              width: "100%",
              backgroundColor: "rgb(147 147 147 / 25%)",
              borderRadius: "10px",
            }}
          >
            <ListItem sx={{ justifyContent: "space-around" }}>
              <Typography>This is the sample:</Typography>
              <Typography>The Sample</Typography>
            </ListItem>
            <Divider component="li" />
            <ListItem sx={{ justifyContent: "space-around" }}>
              <Typography>This is the sample:</Typography>
              <Typography>The Sample</Typography>
            </ListItem>
            <Divider component="li" />
            <ListItem sx={{ justifyContent: "space-around" }}>
              <Typography>This is the sample:</Typography>
              <Typography>The Sample</Typography>
            </ListItem>
            <Divider component="li" />
            <ListItem sx={{ justifyContent: "space-around" }}>
              <Typography>This is the sample:</Typography>
              <Typography>The Sample</Typography>
            </ListItem>
            <ListItem sx={{ justifyContent: "space-around" }}>
              <Typography>This is the sample:</Typography>
              <Typography>The Sample</Typography>
            </ListItem>
            <Divider component="li" />
            <ListItem sx={{ justifyContent: "space-around" }}>
              <Typography>This is the sample:</Typography>
              <Typography>The Sample</Typography>
            </ListItem>
            <Divider component="li" />
            <ListItem sx={{ justifyContent: "space-around" }}>
              <Typography>This is the sample:</Typography>
              <Typography>The Sample</Typography>
            </ListItem>
            <Divider component="li" />
            <ListItem sx={{ justifyContent: "space-around" }}>
              <Typography>This is the sample:</Typography>
              <Typography>The Sample</Typography>
            </ListItem>
            <Divider component="li" />
            <ListItem sx={{ justifyContent: "space-around" }}>
              <Typography>This is the sample:</Typography>
              <Typography>The Sample</Typography>
            </ListItem>
          </List>
        </Grid>
      </Grid>
    </>
  );
};
