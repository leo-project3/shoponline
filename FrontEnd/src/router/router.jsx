import { ProductPage } from "../Page/ProductPage";
import { ShopPage } from "../Component/ShopPage";
import { DetailsProductPage } from "../Page/DetailsPage";
import { CartPage } from "../Page/CartPage";
import { LoginPage } from "../Page/LoginPage";
import { CheckOutPage } from "../Page/CheckOutPage";
import { Profile } from "../Page/ProfilePage";

//Public Routes
export const router = [
  { path: "/", element: <ShopPage /> },
  { path: "/login", element: <LoginPage /> },
  { path: "/products", element: <ProductPage /> },
  { path: "/products/:id", element: <DetailsProductPage /> },
  { path: "/cart", element: <CartPage /> },
  { path: "/checkout", element: <CheckOutPage /> },
  { path: "/profile", element: <Profile /> },
];
