import { Alert, Box, Modal, Snackbar } from "@mui/material";
import {
  TableRow,
  Divider,
  OutlinedInput,
  InputAdornment,
  IconButton,
  Container,
  Grid,
  TextField,
  Typography,
  Button,
  Select,
  MenuItem,
  InputLabel,
  FormControl,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableFooter,
} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";
import DeleteIcon from "@mui/icons-material/Delete";
import axios from "axios";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { SelectLocation } from "../../../Component/Body/Global/locationSelect";
import { ShopToast } from "../../../ToastMessagePopup/toastShop";
import { addToCart } from "../../../action/addToCartAction";
import { CartComponent } from "../../../Component/Body/Cart/cartComponent";
import { orderDetailFetch } from "../../../action/checkOutAction";
import { orderDetailFetchAdmin } from "../../action/checkOutAdmin";

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};
export const CreateOrderTab = ({ edit, setEdit, id }) => {
  const style = {
    width: "100%",
    borderRadius: "10px",
    bgcolor: "background.paper",
    border: "1px solid #000",
    boxShadow: 24,
    p: "20px",
  };

  const dispatch = useDispatch()

  const [openSnack, setOpenSnack] = useState(false);
  const [messageSuccess, setMessageSuccess] = useState("");
  const [messageError, setMessageError] = useState("");
  const [userNameArr, setUserNameArr] = useState([])
  const [selectedUser, setSelectedUser] = useState({})
  const [product, setAllProduct] = useState([])
  const [userName, setUserName] = useState("")
  const [fullname, setName] = useState("");
  const [phone, setPhone] = useState("");
  const [email, setEmail] = useState("");
  const [address, setAddress] = useState("");
  const [city, setCity] = useState("");
  const [district, setDistrict] = useState("");
  const [ward, setWard] = useState("");
  const [error, setError] = useState({});
  const [quantity, setQuantity] = useState(1);
  const [selectProduct, setSelectProduct] = useState("")
  const [cart, setCart] = useState();

  const validateError = {};
  const { item } = useSelector((reduxData) => reduxData.detailsReducer);
  let subtotal
  let totalPrice
  let shippingFee
  useEffect(() => {
    GetUser()
    getProduct()
  }, [])
  const menuItem = userNameArr.map((userName) => {
    return (
      <MenuItem key={userName._id} value={userName._id}>{userName.fullName}</MenuItem>
    )
  })

  const productItem = product.map((product) => {
    return (
      <MenuItem key={product._id} value={product._id}>{product.name}</MenuItem>
    )
  })
  const cartI = JSON.parse(localStorage.getItem("cart"));

  //Caculate subtotal, shipping fee (order less than 100 million VND), and total price
  if (cartI != null && cartI?.length != 0) {
    subtotal = cartI.reduce(
      (acc, item) => item.promotionPrice * item.quantity + acc,
      0,
    );
    if (subtotal > 100000000) {
      shippingFee = 0;
    } else {
      shippingFee = 1000000;
    }
    totalPrice = subtotal + shippingFee;
  }
  const cartItem = JSON.parse(localStorage.getItem("cart"));
  //Product card for sm screen (>600px)
  const card = cartItem?.map((product, index) => {
    return (
      <TableRow key={product._id} sx={{ display: { sm: "table-row", xs: "none" } }}>
        <TableCell sx={{ display: "flex", alignItems: "center" }}>
          <img
            src={process.env.REACT_APP_FETCH_URL + "/upload/" + product.imageUrl.image[0]}
            alt="product-img"
            style={{ width: "50px", height: "45px" }}
          />
          <Typography sx={{ marginLeft: "50px" }}>{product.name}</Typography>
        </TableCell>
        <TableCell align="center" sx={{ color: "red" }}>
          <b>{product.promotionPrice.toLocaleString()} VND</b>
        </TableCell>
        <TableCell align="center">
          <OutlinedInput
            sx={{ textAlign: "center", width: "100%", minWidth: "95px", maxWidth: "105px", height: "30px", padding: '2px' }}
            inputProps={{ min: 0, style: { textAlign: "center" } }}
            size="small"
            type="text"
            value={product.quantity}
            endAdornment={
              <InputAdornment position="end">
                <IconButton sx={{ width: "20px" }} onClick={() => onBtnAdd(product)}>
                  <Divider orientation="vertical" flexItem sx={{ borderColor: "black" }} />
                  <AddIcon fontSize="small" />
                </IconButton>
              </InputAdornment>
            }
            startAdornment={
              <InputAdornment position="start">
                <IconButton sx={{ width: "20px" }} onClick={() => onBtnMinus(product)}>
                  <RemoveIcon fontSize="small" />
                  <Divider orientation="vertical" flexItem sx={{ borderColor: "black" }} />
                </IconButton>

              </InputAdornment>
            }
          />
        </TableCell>
        <TableCell align="center" sx={{ color: "" }}>
          <b>{(product.promotionPrice * product.quantity).toLocaleString()} VND</b>
        </TableCell>
        <TableCell>
          <DeleteIcon onClick={() => onBtnDelete(product)} />
        </TableCell>
      </TableRow>
    );
  });
  const onBtnUpdateInformationClick = async () => {
    try {
      const response = await axios.post(
        process.env.REACT_APP_FETCH_URL + "/api/customernoorder/"
      );
      const data = await response.data;

      if (response.status == 201) {
        setMessageError("");
        setOpenSnack(true);
        setMessageSuccess(data.message);
        //setTimeout(() => window.location.reload(), 5000)
      }
    } catch (error) {
      setOpenSnack(true);

      setMessageError(error.response.data.message);
    }
  };

  const GetUser = async (e) => {
    const customer = await axios(process.env.REACT_APP_FETCH_URL + '/api/customer/')
    const allCus = await customer.data.Customer
    const response = await allCus.map((cus) => cus.fullName)
    setUserNameArr(allCus)
  }
  const getProduct = async () => {
    const product = await axios(process.env.REACT_APP_FETCH_URL + "/api/product" + process.env.REACT_APP_LIMIT_PRODUCT)
    const allProduct = await product.data.products
    setAllProduct(allProduct)
  }
  const onBtnSelectedUser = (e) => {
    setUserName(e.target.value)
    let response = userNameArr.map((cus) => {
      if (e.target.value == cus._id) {
        setSelectedUser(cus)
        setPhone(cus.phone)
        setEmail(cus.email)
        setAddress(cus.address)
        setCity(cus.city)
        setDistrict(cus.district)
        setWard(cus.ward)
      }
    })
  }

  const onBtnAdd = (product) => {
    const item = cartItem.find((item) => item._id === product._id);
    if (item) {
      item.quantity++;
    }
    localStorage.setItem("cart", JSON.stringify(cartItem));
    setCart(cartItem);
  };

  const onBtnMinus = (product) => {
    const item = cartItem.find((item) => item._id === product._id);
    if (item && item.quantity > 0) {
      item.quantity--;
    }
    if (item.quantity == 0) {
      const index = cartItem.findIndex((item) => item._id == product._id);
      if (index != -1) {
        cartItem.splice(index, 1);

      }
    }
    localStorage.setItem("cart", JSON.stringify(cartItem));
    setCart(cartItem);
  };

  const onBtnDelete = (product) => {
    const index = cartItem.findIndex((item) => item._id == product._id);
    if (index != -1) {
      cartItem.splice(index, 1);

    }
    localStorage.setItem("cart", JSON.stringify(cartItem));
    setCart(cartItem);
  };

  const onBtnSelectProduct = (e) => {
    let response = product.map((product) => {
      if (e.target.value == product._id) {
        setSelectProduct(product._id)
        dispatch(addToCart(product, quantity))
      }
    })
  }

  const validateData = () => {
    if (!userName) {
      validateError.userName = "Username is required";
    }
    setError(validateError);
  }
  const onBtnOrder = () => {
    validateData()
    if (Object.keys(validateError).length === 0 && localStorage.getItem("cart") != null) {
      if (JSON.parse(localStorage.getItem("cart")).length !== 0) {
        try {
          orderDetailFetchAdmin(totalPrice, selectedUser, setMessageError, setMessageSuccess, setOpenSnack)
        } catch (error) {
          setMessageError(error)
        }
      } else {
        setOpenSnack(true)
        setMessageError("Empty cart")
      }
    }
  }
  return (
    <>
      <Box sx={style} style={{ minWidth: "300px", width: "100%" }}>
        <Typography
          variant="h4"
          component="div"
          textAlign="center"
          marginBottom={3}
        >
          Create Order
        </Typography>

        <Grid container spacing={2}>
          <Grid item sm={6} sx={{ display: "flex", flexDirection: "column" }}>
            <FormControl>
              <InputLabel sx={{
                "&.Mui-focused": {
                  color: "White",
                }
              }}>User Name</InputLabel>
              <Select label="User Name" value={userName || ""} onChange={(e) => onBtnSelectedUser(e)} fullWidth MenuProps={MenuProps} >
                {menuItem}
              </Select>
            </FormControl>
            {error.userName && (
              <span style={{ color: "red" }}>{error.userName}</span>
            )}
          </Grid>
          <Grid item sm={6}>
            <TextField
              disabled
              label="Phone"
              value={phone || ""}
              onChange={(e) => setPhone(e.target.value)}
              fullWidth
              sx={{
                "&& .Mui-focused": {
                  color: "White",
                },
              }}
            ></TextField>

          </Grid>
        </Grid>
        <Grid container spacing={2} sx={{ mt: 1 }}>
          <Grid item sm={6}>
            <TextField
              disabled
              label="Email"
              value={email || ""}
              onChange={(e) => setEmail(e.target.value)}
              fullWidth
              sx={{
                "&& .Mui-focused": {
                  color: "White",
                },
              }}
            ></TextField>

          </Grid>
          <Grid item sm={6}>
            <TextField
              disabled
              label="Address"
              value={address || ""}
              onChange={(e) => setAddress(e.target.value)}
              fullWidth
              sx={{
                "&& .Mui-focused": {
                  color: "White",
                },
              }}
            ></TextField>
          </Grid>
        </Grid>
        <Grid container spacing={2} sx={{ mt: 1, alignItems: "center" }}>
          <Grid item sm={6}>
            <TextField
              disabled
              label="City"
              value={city || ""}
              onChange={(e) => setAddress(e.target.value)}
              fullWidth
              sx={{
                "&& .Mui-focused": {
                  color: "White",
                },
              }}
            ></TextField>

          </Grid>
          <Grid item sm={6}>
            <TextField
              disabled
              label="District"
              value={district || ""}
              onChange={(e) => setAddress(e.target.value)}
              fullWidth
              sx={{
                "&& .Mui-focused": {
                  color: "White",
                },
              }}
            ></TextField>

          </Grid>
          <Grid item sm={6}>
            <TextField
              disabled
              label="Ward"
              value={ward || ""}
              onChange={(e) => setAddress(e.target.value)}
              fullWidth
              sx={{
                "&& .Mui-focused": {
                  color: "White",
                },
              }}
            ></TextField>
          </Grid>
          <Grid item sm={6} sx={{ display: "flex", flexDirection: "column" }}>
            <FormControl>
              <InputLabel sx={{
                "&.Mui-focused": {
                  color: "White",
                }
              }}>Product</InputLabel>
              <Select label="Product" fullWidth MenuProps={MenuProps} value={selectProduct || ""} onChange={(e) => onBtnSelectProduct(e)}>
                {productItem}
              </Select>
            </FormControl>
          </Grid>

        </Grid>
        <Grid container spacing={2} sx={{ mt: 1, alignItems: "center" }}>
          <Grid item sm={6} sx={{ marginTop: "20px" }}>
            {shippingFee != undefined ? (
              <Container sx={{ height: "200px", overflowY: "scroll" }}>
                {card}

                <TableFooter sx={{ display: { xs: "flex", sm: "table-row-group" }, flexDirection: "column" }}>

                  <TableRow>
                    <TableCell>Shipping Fee:</TableCell>
                    <TableCell sx={{ color: "Red", fontWeight: "Bold" }}>
                      {shippingFee} VND
                    </TableCell>
                  </TableRow>

                  <TableRow>
                    <TableCell>Total Price:</TableCell>
                    <TableCell sx={{ color: "Red", fontWeight: "Bold" }}>
                      {totalPrice != null && totalPrice} VND
                    </TableCell>
                  </TableRow>
                </TableFooter>

              </Container>) : null}
          </Grid>
          <Grid item sm={6} sx={{ marginTop: "20px" }}>
            <Button
              size="large"
              variant="contained"
              color="success"

              onClick={() => onBtnOrder()}
            >
              Create Order
            </Button>
          </Grid>
        </Grid>

      </Box>
      <ShopToast
        open={openSnack}
        setOpen={setOpenSnack}
        messageError={messageError}
        messageSuccess={messageSuccess}
      />
    </>
  );
};
