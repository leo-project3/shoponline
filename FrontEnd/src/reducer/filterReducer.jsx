import {
  PENDING,
  CHANGE_PAGE,
  CHANGE_CATEGORY,
  FETCH_FILTER_DATA,
  FETCH_RELATED_PRODUCT,
} from "../contants/data";

const initialState = {
  item: [],
  pending: true,
  relatedProduct: [],
  noPageFilter: 0,
  noPageFillter2: 0,
  pageFilter: 1,
  limit: 8,
};
// Ex-filter reducer. Now I dont use it but let it stay here for me to review.
export const filterReducer = (state = initialState, action) => {
  switch (action.type) {
    case PENDING:
      state.pending = true;
      break;
    case CHANGE_CATEGORY:
      state.category = action.payload;
      break;
    case FETCH_FILTER_DATA:
      state.pending = false;
      state.item = action.item;
      state.noPageFilter = Math.ceil(action.totalProduct / state.limit);
      state.noPageFillter2 = Math.ceil(action.totalFilterProduct / state.limit);
      break;
    case CHANGE_PAGE:
      state.pageFilter = action.page;
      break;
    case FETCH_RELATED_PRODUCT:
      state.relatedProduct = action.item;
      break;
    default:
      break;
  }
  return { ...state };
};
