import { Button } from "@mui/material";
import { Link } from "react-router-dom";
import { Nav } from "reactstrap";
export const LoginNav = () => {
  return (
    <>
      <Nav
        className="m-auto"
        style={{ fontSize: 16, display: "flex", alignItems: "center" }}
      >
        <Link to="/login">
          <Button variant="contained" color="success">
            Login
          </Button>
        </Link>
      </Nav>
    </>
  );
};
