const userModel = require("../model/userModel")
const bcrypt = require("bcrypt")
const jwt = require("jsonwebtoken")
const refreshTokenModel = require("../model/refreshToken")
const refreshTokenService = require("../service/refreshTokenService")

const SignUp = async (req, res) => {
  try {
    const {
      username,
      password
    } = req.body
    const existedAccount = await userModel.findOne({ username: { $regex: username, $options: "i" } })
    if (existedAccount) {
      return res.status(400).json({
        message: "Username existed"
      })
    }
    const user = new userModel({
      username: username,
      password: bcrypt.hashSync(password, 8),
      isAdmin: false
    }).save()
    return res.status(200).json({
      message: "Create account successfully",
      user: user
    })
  } catch (error) {
    console.error(error)
    return res.status(500).json({
      Message: "error " + error.message
    })
  }
}

const SignUpAdmin = async (req, res) => {
  try {
    const {
      username,
      password,
    } = req.body
    const existedAccount = await userModel.findOne({ username: { $regex: username, $options: "i" } })
    if (existedAccount) {
      return res.status(400).json({
        message: "Username existed"
      })
    }
    const user = new userModel({
      username: username.toLowerCase(),
      password: bcrypt.hashSync(password, 8),
      isAdmin: true
    }).save()
    return res.status(200).json({
      message: "Create account successfully",
      user: user
    })
  } catch (error) {
    console.error(error)
    return res.status(500).json({
      Message: "error " + error.message
    })
  }
}

const SignIn = async (req, res) => {
  try {
    const {
      username,
      password
    } = req.body

    const existedAccount = await userModel.findOne({ username: { $regex: username, $options: "i" } })
    if (!existedAccount) {
      return res.status(404).json({
        message: "Username is invalid"
      })
    }
    var passwordIsValid = bcrypt.compareSync(password, existedAccount.password)
    if (!passwordIsValid) {
      return res.status(401).json({
        message: "Password is not correct"
      })
    }
    const secretKey = process.env.JWT_SECRET;
    const expireTime = process.env.JWT_EXPIRED

    const accessToken = jwt.sign(
      {
        id: existedAccount._id
      },
      secretKey,
      {
        algorithm: "HS256",
        allowInsecureKeySizes: true,
        expiresIn: expireTime
      }
    );

    const refreshToken = await refreshTokenService.createToken(existedAccount)
    return res.status(200).json({
      user: existedAccount._id,
      accessToken: accessToken,
      refreshToken: refreshToken
    })
  } catch (error) {
    console.error(error)
    return res.status(500).json({
      Message: "error " + error.message
    })
  }
}

const updatePassword = async (req, res) => {
  try {
    const {
      userId,
      oldPassWord,
      newPassWord
    } = req.body
    const account = await userModel.findById(userId)

    const passwordMatch = await bcrypt.compareSync(oldPassWord, account.password)

    if (!passwordMatch) {
      return res.status(404).json({
        message: "Old Password is not correct"
      })
    } else {
      const changePassword = await userModel.findByIdAndUpdate(userId, { password: bcrypt.hashSync(newPassWord, 8) })
      return res.status(200).json({
        message: "Update password successfully"
      })
    }
  } catch (error) {
    res.status(500).json({
      message: "Internal Error" + error.message
    })
  }
}

const GetUser = async (req, res) => {
  const { id } = req.params

  try {
    const result = await userModel.findById(id)
    return res.status(200).json({
      message: "get user",
      data: result
    })
  } catch (error) {
    console.error(error)
    return res.status(500).json({
      Message: "error " + error.message
    })
  }
}


const GetUserByName = async (req, res) => {
  const { userName } = req.body
  let condition = {}
  if (userName) {
    condition.username = userName
  }
  console.log(condition)
  try {
    const result = await userModel.find(condition)
    return res.status(200).json({
      message: "get user",
      data: result
    })
  } catch (error) {
    console.error(error)
    return res.status(500).json({
      Message: "error " + error.message
    })
  }
}

const refreshToken = async (req, res) => {
  const refreshToken = req.body.refreshToken;
  if (refreshToken == null) {
    return res.status(403).json({
      message: "Refresh Token is required"
    })
  }
  try {
    const refreshTokenObj = await refreshTokenModel.findOne({ token: refreshToken })

    if (!refreshToken) {
      return res.status(404).json({
        message: "Refresh Token is not found"
      })
    }
    if (refreshTokenObj.expiredDate.getTime() < new Date().getTime()) {
      await refreshTokenModel.findByIdAndDelete(refreshTokenObj._id)
      return res.status(403).json({
        message: "Refresh Token is expired"
      })
    }
    const serectKey = process.env.JWT_SECRET
    const expireTime = process.env.JWT_EXPIRED
    const newAccessToken = jwt.sign(
      {
        id: refreshTokenObj.user
      },
      serectKey,
      {
        algorithm: "HS256",
        allowInsecureKeySizes: true,
        expiresIn: expireTime
      }
    );
    console.log(newAccessToken)
    return res.status(200).json({
      accesToken: newAccessToken,
      refreshToken: refreshTokenObj.token
    })
  } catch (error) {
    return res.status(500).json({
      message: "error" + error.meesage
    })
  }
}
module.exports = { SignUp, SignUpAdmin, SignIn, updatePassword, refreshToken, GetUser, GetUserByName }