import Product from "../Component/Body/Homepage/LastestProductBody";
import Header from "../Component/Header/header";
import Footer from "../Component/Footer/footer";
import { BreadCrumbs } from "./BreadCrumbs";
import { FilterBar } from "../Component/Body/Filter/FilterProduct";
import { Container } from "@mui/material"
import {
  PaginationFilter,
  PaginationProduct,
} from "../Component/Body/Global/pagination";
import { useSelector } from "react-redux";
import { FilterCard } from "../Component/Body/Filter/filterCard";
import { NoProductFound } from "../Component/Body/No Found Component/noProductFound";
export const ProductPage = () => {
  const { items, noPageFilter } = useSelector((reduxData) => reduxData.dataReducer);
  return (
    <>

      <Header />
      <div
        style={{ marginTop: "120px" }}
        className="container bg-white p-4 rounded-4"
      >
        <BreadCrumbs />
        <div className="text-center">
          <h4 className="text-dark">
            <b>LATEST PRODUCT</b>
          </h4>
          <div className="container mt-3 mb-2">
            <FilterBar />
          </div>
          {items.length == 0 ? <NoProductFound /> : <Product />}

        </div>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            marginTop: "20px",
          }}
        >
          {items.length == 0 ? null : noPageFilter != 0 ? <PaginationFilter /> : <PaginationProduct />}
        </div>
      </div>
      {items.length == 0 ?
        (
          <Container
            maxWidth
            style={{ width: "100%", padding: 0 }}
            sx={{ height: { xs: "50%", sm: "auto", position: "absolute", bottom: '-165px' } }}
          >
            <Footer />
          </Container>) : <Footer />}

    </>
  );
};
