const express = require("express")
const router = express.Router()

const {getAllContact, createContactUs, softDeleteContact} = require("../controller/contactUsController")

router.get("/contact", getAllContact)
router.post("/contact", createContactUs)
router.delete("/contact/:id", softDeleteContact)
module.exports = router