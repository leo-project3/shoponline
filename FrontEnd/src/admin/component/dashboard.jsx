import {
  Box,
  Button,
  CircularProgress,
  Grid,
  IconButton,
  Modal,
  Typography,
  useTheme,
} from "@mui/material";
import { tokens } from "../../theme";

import Header from "./global/header";
import axios from "axios";
import { useEffect, useState } from "react";
import LineChart from "./chart/lineChart";
import { useDispatch, useSelector } from "react-redux";
import { getDetailsProduct, getOrder } from "../action/getOrderAction";
import { ViewDetailsProduct } from "./modal/viewDetailsProduct";
import { PieChart } from "./chart/pieChart";
import { ContactModel } from "./modal/contactModel";
import DeleteIcon from "@mui/icons-material/DeleteOutlined";
import { updateOrder } from "../action/updateAction";
import { ShopToast } from "../../ToastMessagePopup/toastShop";

const Dashboard = () => {
  const dispatch = useDispatch();

  const { success } = useSelector((reduxData) => reduxData.productReducer);
  const { page, noPage, limit, ordersData } = useSelector(
    (reduxData) => reduxData.tableReducer,
  );
  const [data, setData] = useState([]);
  const [contact, setContact] = useState([]);
  const [customer, setCustomer] = useState("");
  const [open, setOpen] = useState(false);
  const [name, setName] = useState("");
  const [message, setMessage] = useState("");
  const [openContact, setOpenContact] = useState(false);
  const [recentData, setRecentData] = useState([]);
  const [openToast, setOpenToast] = useState(false);
  const [messageError, setMessageError] = useState("")
  const [messageSuccess, setMessageSuccess] = useState("")

  const theme = useTheme();
  const colors = tokens(theme.palette.mode);
  const [totalValue, setTotalValue] = useState([]);
  const months = [
    "JAN",
    "FEB",
    "MAR",
    "APR",
    "MAY",
    "JUN",
    "JUL",
    "AUG",
    "SEP",
    "OCT",
    "NOV",
    "DEC",
  ];

  useEffect(() => {
    getOrder2();
  }, []);
  useEffect(() => {
    getContact();
  }, []);
  const getOrder2 = async () => {
    const response = await axios(
      process.env.REACT_APP_FETCH_URL + "/api/order",
    );
    const data = await response.data.orders;
    setData(
      data
        .filter((order) => order.status == "Confirmed")
        .map((item) => {
          return item;
        }),
    );
    setRecentData(
      data
        .filter((order) => order.status == "Pending")
        .sort((a, b) => {
          return new Date(b.orderDate) - new Date(a.orderDate);
        }),
    );
  };
  let totalPrice = 0;
  if (data.length > 0) {
    const totalPriceData = data.map((order) => order.cost);
    totalPrice = totalPriceData.reduce((a, b) => {
      return a + b;
    });
  }
  let labelValue = [];
  if (data.length > 0) {
    labelValue = data.reduce((acc, curr) => {
      const idx = Number(curr.orderDate.split("-")[1] - 1);
      acc[idx] += curr.cost;
      return acc;
    }, new Array(12).fill(0));
  }

  const getContact = async () => {
    const response = await axios(
      process.env.REACT_APP_FETCH_URL + "/api/contact",
    );
    const data = await response.data.data;
    if (data) {
      setContact(
        data
          .filter((contact) => contact.isDelete == false)
          .sort((a, b) => {
            return new Date(b.createdAt) - new Date(a.createdAt);
          }),
      );
    }
  };

  const onBntDeleteContact = async (id) => {
    const response = await axios.delete(
      process.env.REACT_APP_FETCH_URL + "/api/contact/" + id,
    );
    if (response) {
      getContact();
    }
  };

  const onBtnConfirmOrder = async (id) => {
    dispatch(updateOrder(id, { status: "Confirmed" }, setMessageError, setMessageSuccess, setOpenToast));
    setTimeout(() => {
      getOrder2();
    }, 1500);

  };
  const onBtnCancelOrder = async (id) => {
    dispatch(updateOrder(id, { status: "Canceled" }, setMessageError, setMessageSuccess, setOpenToast));
    setTimeout(() => {
      getOrder2();
    }, 1500);

  };
  const onBtnClickCheckProduct = async (value, status) => {
    try {
      const response = await axios(
        process.env.REACT_APP_FETCH_URL + "/api/order/customer/" + value,
      );
      const data = await response.data.Customer[0];
      setCustomer(data.email);
      dispatch(getDetailsProduct(value, setOpenToast));
      setOpen(true);
    } catch (error) {
      dispatch(updateOrder(value, { isDelete: true }, setMessageError, setMessageSuccess));
      setOpenToast(true);
      setTimeout(() => {
        getOrder2();
      }, 1500);
    }

  };

  let dataChart = {
    labels: months,
    datasets: [
      {
        label: "Revenue",
        data: labelValue,
        fill: true,
        backgroundColor: ["rgb(173, 216, 230)"],
        borderColor: ["red"],
        borderWidth: 2,
        tension: 0.1,
      },
    ],
  };

  return (
    <>
      <Box m="20px">
        {/* HEADER */}
        <Box display="flex" justifyContent="space-between" alignItems="center">
          <Header title="DASHBOARD" subtitle="Welcome to your dashboard" />
        </Box>

        {/* GRID & CHARTS */}
        <Box
          display="grid"
          gridTemplateColumns="repeat(12, 1fr)"
          gridAutoRows="140px"
          gap="25px"
        >
          {/* ROW 2 */}
          <Box
            gridColumn="span 8"
            gridRow="span 2"
            backgroundColor={colors.primary[400]}
          >
            <Box
              mt="25px"
              p="0 30px"
              display="flex "
              justifyContent="space-between"
              alignItems="center"
            >
              <Box>
                <Typography
                  variant="h5"
                  fontWeight="600"
                  color={colors.grey[100]}
                >
                  Sales Revenue
                </Typography>
                <Typography
                  variant="h3"
                  fontWeight="bold"
                  color={colors.greenAccent[500]}
                >
                  {totalPrice.toLocaleString()} VND
                </Typography>
              </Box>
            </Box>
            <Box>
              <LineChart data={dataChart} />
            </Box>
          </Box>
          <Box
            gridColumn="span 4"
            gridRow="span 2"
            backgroundColor={colors.primary[400]}
          >
            <Box
              mt="25px"
              p="0 30px"
              display="flex"
              flexDirection="column"
              justifyContent="center"
              alignItems="center"
            >
              <Box>
                <Typography
                  variant="h5"
                  fontWeight="600"
                  color={colors.grey[100]}
                >
                  Most Order Customer
                </Typography>
              </Box>

              <Box width="100%">
                <PieChart />
              </Box>
            </Box>
          </Box>
          <Box
            gridColumn="span 8"
            gridRow="span 2"
            backgroundColor={colors.primary[400]}
            overflow="auto"
          >
            <Box
              display="flex"
              justifyContent="space-between"
              alignItems="center"
              borderBottom={`4px solid ${colors.primary[500]}`}
              colors={colors.grey[100]}
              p="15px"
            >
              <Typography
                color={colors.grey[100]}
                variant="h5"
                fontWeight="600"
              >
                Recent Transactions
              </Typography>
            </Box>

            {recentData.map((transaction, i) => (
              <Box
                key={`${transaction._id}-${i}`}
                display="flex"
                justifyContent="space-between"
                alignItems="center"
                borderBottom={`4px solid ${colors.primary[500]}`}
                p="15px"
              >
                <Box onClick={() => onBtnClickCheckProduct(transaction._id)}>
                  <Typography
                    color={colors.greenAccent[500]}
                    variant="h5"
                    fontWeight="600"
                  >
                    {transaction.orderCode}
                  </Typography>
                  <Typography color={colors.grey[100]}>
                    {transaction.status}
                  </Typography>
                </Box>
                <Box color={colors.grey[100]}>
                  {new Date(transaction.orderDate).toLocaleDateString("en-GB")}
                </Box>
                <Box
                  backgroundColor={colors.greenAccent[500]}
                  p="5px 10px"
                  borderRadius="4px"
                >
                  {transaction.cost.toLocaleString()} VND
                </Box>
                <Box>
                  <Button
                    variant="outlined"
                    color="success"
                    onClick={() => onBtnConfirmOrder(transaction._id)}
                  >
                    Confirm
                  </Button>{" "}
                  <Button
                    variant="outlined"
                    color="error"
                    onClick={() => onBtnCancelOrder(transaction._id)}
                  >
                    Cancel
                  </Button>
                </Box>
              </Box>
            ))}
          </Box>

          {/* ROW 3 */}
          <Box
            gridColumn="span 4"
            gridRow="span 2"
            backgroundColor={colors.primary[400]}
            p="30px"
            overflow="auto"
          >
            <Box
              display="flex"
              justifyContent="space-between"
              alignItems="center"
              colors={colors.grey[100]}
            >
              <Typography
                color={colors.grey[100]}
                variant="h5"
                fontWeight="600"
              >
                Help Need
              </Typography>
            </Box>
            {contact.map((contact, i) => (
              <Box
                key={contact._id}
                display="flex"
                justifyContent="space-between"
                alignItems="center"
                borderBottom={`2px solid ${colors.primary[500]}`}
                p="15px"
              >
                <Box>
                  <Typography
                    color={colors.redAccent[500]}
                    variant="h5"
                    fontWeight="600"
                    onClick={() => {
                      setOpenContact(true);
                      setMessage(contact.message);
                      setName(contact.email);
                    }}
                  >
                    {contact.email}
                  </Typography>
                </Box>
                <Box>
                  <IconButton onClick={() => onBntDeleteContact(contact._id)}>
                    <DeleteIcon />
                  </IconButton>
                </Box>
              </Box>
            ))}
          </Box>
        </Box>
      </Box>

      <ContactModel
        setOpen={setOpenContact}
        message={message}
        name={name}
        open={openContact}
      />
      <ViewDetailsProduct open={open} setOpen={setOpen} customer={customer} />
      <ShopToast
        open={openToast}
        setOpen={setOpenToast}
        messageError={messageError}
        messageSuccess={messageSuccess}
      />

    </>
  );
};

export default Dashboard;
