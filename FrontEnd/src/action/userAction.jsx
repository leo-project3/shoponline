import axios from "axios";
import { ERROR, GET_USER } from "../contants/data";

export const getUser = (id) => {
  return async (dispatch) => {
    try {
      const response = await axios(
        process.env.REACT_APP_FETCH_URL + "/api/user/" + id,
      );
      const data = response.data.data;
      return dispatch({
        type: GET_USER,
        name: data.username,
        admin: data.isAdmin,
      });
    } catch (error) {
      return dispatch({
        type: ERROR,
      });
    }
  };
};
