import axios from "axios";
import { ERROR } from "../../contants/data";
import { CREATE, GET_PRODUCT } from "../contants/data";
import { SetMealSharp } from "@mui/icons-material";

//Create new product action
export const createProduct = (obj, setOpenToast, setMessageSuccess, setMessageError) => {
  return async (dispatch) => {
    try {
      const response = await axios.post(
        process.env.REACT_APP_FETCH_URL + "/api/product",
        obj,
        {
          headers: {
            "x-access-token": sessionStorage.getItem("AccessToken"),
            "x-refresh-token": sessionStorage.getItem("RefreshToken"),
          },
        },
      );
      const data = await response.data;
      const response2 = await axios(
        process.env.REACT_APP_FETCH_URL + "/api/product",
      );
      const data2 = await response2.data.products;
      setMessageError("")
      setMessageSuccess(data.message)
      setOpenToast(true)
      dispatch({
        type: GET_PRODUCT,
        payload: data2,
      });
    } catch (error) {
      setMessageSuccess("")
      setMessageError(error.response.data.message)
      setOpenToast(true)
      if (
        error.response.data.message == "Unauthourized" ||
        error.response.data.message == "Please Login" ||
        error.response.data.message == "Please Login Again"
      ) {
        sessionStorage.clear();
        setTimeout(
          () => (window.location.href = process.env.REACT_APP_MAIN_URL + "/login"),
          4000,
        );
      }
    }
  };
};
