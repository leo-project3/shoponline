import * as React from "react";
import {
  Container,
  Grid,
  TextField,
  Typography,
  Button,
  Select,
  InputLabel,
  Modal,
  Box,
} from "@mui/material";
import axios from "axios";
import { ShopToast } from "../../../ToastMessagePopup/toastShop";
const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  textAlign: "Center",
  borderRadius: "15px",
  boxShadow: 24,
  p: 4,
};

export const ChangePassWord = ({ open, setOpen }) => {
  const userId = sessionStorage.getItem("User");
  const [oldPassword, setOldPassword] = React.useState("");
  const [newPassword, setNewPassword] = React.useState("");
  const [confirmedPassword, setConfirmedPassword] = React.useState("");
  const [messageError, setMessageError] = React.useState("");
  const [messageSuccess, setMessageSuccess] = React.useState("");
  const [openToast, setOpenToast] = React.useState(false);
  const [error, setError] = React.useState({});
  const validateError = {};
  const validateData = () => {
    if (!oldPassword) {
      validateError.password = "Password is required";
    } else if (oldPassword.length < 8) {
      validateError.password = "Password must be 8 letters";
    }
    if (!newPassword) {
      validateError.newPassword = "Password is required";
    } else if (newPassword.length < 8) {
      validateError.newPassword = "Password must be 8 letters";
    }
    if (newPassword != confirmedPassword) {
      validateError.confirmedPassword = "Confirmed Password is not match";
    }
    setError(validateError);
  };

  const onBtnChangePassWord = async () => {
    validateData();
    try {
      if (Object.keys(validateError).length === 0) {
        const response = await axios.put(
          process.env.REACT_APP_FETCH_URL + "/api/changepassword",
          {
            userId: userId,
            oldPassWord: oldPassword,
            newPassWord: newPassword,
          },
        );
        const data = await response.data.message;
        setMessageError("");
        setMessageSuccess(data);
        setOpenToast(true);
        setOpen(false);
      }
    } catch (error) {
      setMessageSuccess("");
      setMessageError(error.response.data.message);
      setOpenToast(true);
    }
  };
  return (
    <>
      <Modal
        open={open}
        onClose={() => {
          setOpen(false);
        }}
      >
        <Box
          sx={style}
          style={{ minWidth: "300px", maxWidth: "400px", width: "100%" }}
        >
          <Typography variant="h5">Change Password</Typography>
          <TextField
            label="Old Password"
            type="password"
            value={oldPassword || ""}
            onChange={(e) => setOldPassword(e.target.value)}
            sx={{ mt: 1, mb: 1 }}
            fullWidth
          ></TextField>
          {error.password && (
            <span style={{ color: "red" }}>{error.password}</span>
          )}
          {error.passwordError && (
            <span style={{ color: "red" }}>{error.passwordError}</span>
          )}
          <TextField
            label="New Password"
            type="password"
            value={newPassword || ""}
            onChange={(e) => setNewPassword(e.target.value)}
            fullWidth
          ></TextField>
          {error.newPassword && (
            <span style={{ color: "red" }}>{error.newPassword}</span>
          )}
          <TextField
            label="Confirmed New Password"
            type="password"
            value={confirmedPassword || ""}
            onChange={(e) => setConfirmedPassword(e.target.value)}
            sx={{ mt: 1, mb: 1 }}
            fullWidth
          ></TextField>
          {error.confirmedPassword && (
            <span style={{ color: "red" }}>{error.confirmedPassword}</span>
          )}
          <Button
            variant="contained"
            color="success"
            onClick={() => onBtnChangePassWord()}
          >
            Change Password
          </Button>
        </Box>
      </Modal>
      <ShopToast
        messageSuccess={messageSuccess}
        messageError={messageError}
        setOpen={setOpenToast}
        open={openToast}
      />
    </>
  );
};
