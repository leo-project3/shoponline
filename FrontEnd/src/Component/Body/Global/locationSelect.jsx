import { FormControl, InputLabel, MenuItem, Select, Autocomplete, TextField } from "@mui/material";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getCity } from "../../../action/locationAction";


const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};
// Location selection for city, district, ward
export const SelectLocation = ({
  error,
  customers,
  setCustomers,
  setCity,
  setDistrict,
  city,
  district,
  ward,
  setWard,
  color
}) => {
  const dispatch = useDispatch();
  const [districts, setDistricts] = useState([]);
  const [wards, setWards] = useState([]);
  const { cities } = useSelector((reduxData) => reduxData.locationReducer);
  useEffect(() => {
    dispatch(getCity());
  }, []);

  const cityCard = cities.map((item, index) => {
    return (
      <MenuItem value={item.Name} key={item}>
        {item.Name}
      </MenuItem>
    );
  });
  const onChangeCity = (e) => {
    if (customers) {
      setCustomers({ ...customers, city: e.target.value })
    }
    setCity(e.target.value);

    const selectCity = cities.find((item) => item.Name === e.target.value);
    setDistricts(selectCity.Districts);
    setDistrict("");
    setWard("");
  };
  const onChangeDistrict = (e) => {
    if (customers) {
      setCustomers({ ...customers, district: e.target.value })
    }
    setDistrict(e.target.value);
    const selectDistrict = districts.find(
      (item) => item.Name === e.target.value,
    );
    setWards(selectDistrict.Wards);
    setWard("");
  };
  const onChangeWard = (e) => {
    if (customers) {
      setCustomers({ ...customers, ward: e.target.value })
    }
    setWard(e.target.value);

  };
  return (
    <>
      <FormControl sx={{ mt: 1 }} fullWidth>
        <InputLabel sx={{
          "&.Mui-focused": {
            color: color,
          }
        }}>City</InputLabel>
        <Select
          MenuProps={MenuProps}
          sx={{ textAlign: "left" }}
          label="City"
          labelId="City"
          value={city || ""}
          defaultValue=""
          onChange={(e) => onChangeCity(e)}
        >
          <MenuItem value="" disabled>
            Choose City
          </MenuItem>
          {cityCard}
        </Select>
        {error.city && <span style={{ color: "red" }}>{error.city}</span>}
      </FormControl>

      <FormControl sx={{ mt: 1, mb: 1 }} fullWidth>
        <InputLabel sx={{
          "&.Mui-focused": {
            color: color,
          }
        }}>District</InputLabel>
        <Select
          MenuProps={MenuProps}
          sx={{ textAlign: "left" }}
          label="District"
          labelId="District"
          defaultValue=""
          value={district}
          onChange={(e) => onChangeDistrict(e)}
        >
          <MenuItem value="" disabled>
            Choose District
          </MenuItem>
          {cities.length != 0 &&
            districts.map((item, index) => {
              return (
                <MenuItem key={item._id} value={item.Name}>
                  {item.Name}
                </MenuItem>
              );
            })}
        </Select>
        {error.district && (
          <span style={{ color: "red" }}>{error.district}</span>
        )}
      </FormControl>

      <FormControl sx={{ mb: 1 }} fullWidth>
        <InputLabel sx={{
          "&.Mui-focused": {
            color: color,
          }
        }}>Ward</InputLabel>
        <Select
          MenuProps={MenuProps}
          sx={{ textAlign: "left" }}
          value={ward || ""}
          label="Ward"
          defaultValue=""
          labelId="ward"
          onChange={(e) => onChangeWard(e)}
        >
          <MenuItem value="" disabled>
            Choose Ward
          </MenuItem>
          {wards.length != 0 &&
            wards.map((item, index) => {
              return (
                <MenuItem key={item._id} value={item.Name}>
                  {item.Name}
                </MenuItem>
              );
            })}
        </Select>
        {error.ward && <span style={{ color: "red" }}>{error.ward}</span>}
      </FormControl>
    </>
  );
};
