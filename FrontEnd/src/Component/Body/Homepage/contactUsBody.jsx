import { Button, Grid, TextField, Typography } from "@mui/material";
import axios from "axios";
import { useState } from "react";
import { ShopToast } from "../../../ToastMessagePopup/toastShop";


//Contact form 
export const ContactUs = () => {
  const [email, setEmail] = useState();
  const [message, setMessage] = useState();
  const [openToast, setOpenToast] = useState(false);
  const [errorMsg, setErrorMsg] = useState("");
  const [messageSuccess, setMessageSuccess] = useState("");
  const [error, setError] = useState({});
  const validateError = {};

  //Validate data before send message
  const validateData = () => {
    const isValidEmail = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g.test(email);
    //Validate input value
    if (!email.trim()) {
      validateError.email = "Email is required";
    } else if (!isValidEmail) {
      validateError.email = "Email is invalid";
    }
    setError(validateError);
  };
  const onBtnSend = async () => {
    validateData();
    const contactObj = {
      email: email,
      message: message,
    };
    if (Object.keys(validateError).length === 0) {
      try {
        const response = await axios.post(
          process.env.REACT_APP_FETCH_URL + "/api/contact",
          contactObj,
        );
        const data = await response.data;
        if (response.status == 201) {
          setMessageSuccess("Successful");
          setErrorMsg("");
          setOpenToast(true);
        }
      } catch (error) {
        setMessageSuccess("");
        setErrorMsg("");
        setOpenToast(true);
      }
    }
  };
  return (
    <>
      <Grid
        padding={4}
        spacing={3}
        container
        sx={{ display: "flex", justifyContent: "center" }}
        id="Contact"
      >
        <Typography variant="h5" component="div" fontWeight="bold">
          Contact Us
        </Typography>
        <Grid item xs={12} textAlign="center">
          <TextField
            fullWidth
            label="Your Email"
            sx={{ maxWidth: "500px" }}
            onChange={(e) => setEmail(e.target.value)}
            value={email}
          />
        </Grid>
        {error.email && <span style={{ color: "red" }}>{error.email}</span>}

        <Grid item xs={12} textAlign="center">
          <TextField
            fullWidth
            sx={{ maxWidth: "500px" }}
            multiline
            label="Your message"
            rows={3}
            onChange={(e) => setMessage(e.target.value)}
            value={message}
          />
        </Grid>
        <Grid item xs={12} textAlign="center">
          <Button
            variant="contained"
            color="secondary"
            onClick={() => onBtnSend()}
          >
            Contact Us
          </Button>
        </Grid>
      </Grid>
      <ShopToast
        open={openToast}
        setOpen={setOpenToast}
        messageSuccess={messageSuccess}
        messageError={errorMsg}
      />
    </>
  );
};
