const contactUsModel = require("../model/contactUsModel");
const mongoose = require("mongoose");


const createContactUs = async (req, res) => {
  const {
    email,
    message
  } = req.body
  if(!email){
    return res.status(400).json({
      message: "Email is required"
    })
  }
  try {
    const newContact = await contactUsModel.create({
      email,
      message
    }) 
    if(newContact){
      return res.status(201).json({
        message: "Create contact successfully"
      })
    }
  } catch (error) {
    return res.status(500).json({
      message:"Internal Error" + error.message
    })
  }
}

const getAllContact = async (req, res) => {
  try {
    const result = await contactUsModel.find()
    return res.status(200).json({
      message: "Get all contact us",
      data: result
    })
  } catch (error) {
    return res.status(500).json({
      message:"Internal Error" + error.message
    })
  }
}

const softDeleteContact = async (req, res) => {
  const id = req.params.id
  if(!mongoose.Types.ObjectId.isValid(id)){
    return res.status(404).json({
      message: "Id is not valid"
    })
  }
  try {
    const result = await contactUsModel.findByIdAndUpdate(id, {isDelete: true})
    return res.status(200).json({
      message: "Delete successfully"
    })
  } catch (error) {
    return res.status(500).json({
      message: "Internal Error" + error.message
    })
  }
}
module.exports = {
  getAllContact,
  createContactUs,
  softDeleteContact
}