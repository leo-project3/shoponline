import {
  CircularProgress,
  Container,
  Grid,
  Typography,
  TableCell,
  TableRow,
  TableContainer,
  Table,
  TableHead,
  TableBody,
  Button,
  Paper,
  Select,
  MenuItem,
  Box,
} from "@mui/material";
import EditNoteIcon from "@mui/icons-material/EditNote";
import { useDispatch, useSelector } from "react-redux";
import { getDetailsProduct, getOrder } from "../../action/getOrderAction";
import { useEffect, useState } from "react";
import { DeleteOrderModal } from "../modal/deleteModal";
import { UpdateOrderModal } from "../modal/updateModal";
import { SelectLimit } from "./selectLimit";
import {
  DataGrid,
  GridToolbar,
  GridActionsCellItem,
  GridRowModes,
  GridRowEditStopReasons,
} from "@mui/x-data-grid";
import { tokens } from "../../../theme";
import { useTheme } from "@mui/material";
import axios from "axios";
import Header from "../global/header";
import { useLocation } from "react-router-dom";
import DeleteIcon from "@mui/icons-material/DeleteOutlined";
import SaveIcon from "@mui/icons-material/Save";
import CancelIcon from "@mui/icons-material/Close";
import EditIcon from "@mui/icons-material/Edit";
import { deleteOrder } from "../../action/deleteAction";
import { updateOrder } from "../../action/updateAction";
import { ViewDetailsProduct } from "../modal/viewDetailsProduct";
import { ShopToast } from "../../../ToastMessagePopup/toastShop"


export const OrderManagement = () => {
  const theme = useTheme();
  const colors = tokens(theme.palette.mode);
  const dispatch = useDispatch();
  let orders;
  let userName;

  const location = useLocation();
  if (location.state) {
    orders = location.state.orders;
    userName = location.state.userName;
  }

  const { pending, page, noPage, limit, ordersData } = useSelector(
    (reduxData) => reduxData.tableReducer,
  );
  const [orderUser, setOrderUser] = useState("");
  const [openModal, setOpenModal] = useState(false);
  const [openUpdateModal, setOpenUpdateModal] = useState(false);
  const [order, setOrder] = useState({})
  const [paramOrderId, setOrderId] = useState({});
  const [paramOrderCode, setOrderCode] = useState();
  const [rowModesModel, setRowModesModel] = useState({});
  const [messageError, setMessageError] = useState("")
  const [messageSuccess, setMessageSuccess] = useState("")
  const [openToast, setOpenToast] = useState(false);
  const [openProductModal, setProductModal] = useState(false);
  const [columnVisibilityModel, setColumnVisibilityModel] = useState({
    _id: false,
    orderCode: true,
    orderDate: true,
    shippedDate: true,
    cost: true,
    status: true,
    username: userName ? true : false,
  });
  useEffect(() => {
    dispatch(getOrder());
  }, []);

  const handleRowEditStop = (params, event) => {
    if (params.reason === GridRowEditStopReasons.rowFocusOut) {
      event.defaultMuiPrevented = true;
    }
  };

  const handleEditClick = (id) => () => {

    axios(process.env.REACT_APP_FETCH_URL + "/api/order/" + id).then(
      (data) => {
        /*  setValues({
         name: data.data.product.name
        }) */
        setOrder(data.data.order);
      },
    );
    setOpenUpdateModal(true)
    // setRowModesModel({ ...rowModesModel, [id]: { mode: GridRowModes.Edit } });
  };

  const handleCancelClick = (id) => () => {
    setRowModesModel({
      ...rowModesModel,
      [id]: { mode: GridRowModes.View, ignoreModifications: true },
    });
  };

  const handleSaveClick = (id) => () => {
    setRowModesModel({ ...rowModesModel, [id]: { mode: GridRowModes.View } });
    setOpenToast(true);
  };

  const handleDeleteClick = (id) => () => {
    setOrderId(id);
    setOpenModal(true);
  };

  const handleRowModesModelChange = (newRowModesModel) => {
    setRowModesModel(newRowModesModel);
  };

  const processRowUpdate = (newRow) => {
    const updatedRow = { ...newRow, isNew: false };

    dispatch(updateOrder(updatedRow._id, updatedRow.status, setOpenToast, setMessageError, setMessageSuccess));
    //handle send data to api
    return updatedRow;
  };

  const onBtnCellClick = async (params) => {
    if (params.field == "orderCode") {
      try {
        dispatch(getDetailsProduct(params.id, setOpenToast));
        const response = await axios(
          process.env.REACT_APP_FETCH_URL +
          "/api/order/customer/" +
          params.row._id,
        );
        const data = await response.data.Customer[0];
        if (data) {
          setOrderUser(data.email);
          setProductModal(true);
        }
      } catch (error) { }

      /*  const response = await axios(process.env.REACT_APP_FETCH_URL + '/api/order/' + params.id)
      const orderDetails = await response.data.order.orderDetails
      const response2 = await axios(process.env.REACT_APP_FETCH_URL + '/api/orderdetails/' + orderDetails)
      const product = await response2.data.orderDetails
      const response3 = await axios(process.env.REACT_APP_FETCH_URL + '/api/product/' + product.product)
      const productName = await response3.data
    */
    }
  };

  const columns = [
    { field: "_id", headerName: "Id", width: 150, hide: true },
    {
      field: "username",
      headerName: "User Name",
      flex: 1,
      renderCell: () => userName,
    },
    { field: "orderCode", headerName: "Order Code", flex: 1 },
    {
      field: "orderDate",
      headerName: "Order Date",
      flex: 1,
      renderCell: (params) => new Date(params.row.orderDate).toLocaleString("en-GB"),
    },
    {
      field: "shippedDate",
      headerName: "Shipping Date",
      flex: 1,
      renderCell: (params) => new Date(params.row.shippedDate).toLocaleString("en-GB"),
    },
    {
      field: "status",
      headerName: "Status",
      flex: 0.5,
      editable: true,
      type: "singleSelect",
      valueOptions: ["Confirmed", "Canceled", "Pending"],
    },
    {
      field: "cost",
      headerName: "Total Price",
      flex: 0.5,
      renderCell: (params) => params.row.cost.toLocaleString(),
    },
    {
      field: "actions",
      type: "actions",
      headerName: "Actions",
      flex: 1,
      cellClassName: "actions",
      getActions: ({ id }) => {
        const isInEditMode = rowModesModel[id]?.mode === GridRowModes.Edit;

        if (isInEditMode) {
          return [
            <GridActionsCellItem
              icon={<SaveIcon />}
              label="Save"
              color="inherit"
              onClick={handleSaveClick(id)}
            />,
            <GridActionsCellItem
              icon={<CancelIcon />}
              label="Cancel"
              className="textPrimary"
              onClick={handleCancelClick(id)}
              color="inherit"
            />,
          ];
        }

        return [
          <GridActionsCellItem
            icon={<EditIcon />}
            label="Edit"
            className="textPrimary"
            onClick={handleEditClick(id)}
            color="inherit"
          />,
          <GridActionsCellItem
            icon={<DeleteIcon />}
            label="Delete"
            onClick={handleDeleteClick(id)}
            color="inherit"
          />,
        ];
      },
    },
  ];

  return (
    <>
      {pending ? (
        <Grid item lg={12} md={12} sm={12} xs={12} textAlign="center">
          <CircularProgress color="secondary" />
        </Grid>
      ) : (
        <Box m="20px">
          <Header
            title="Order"
            subtitle={`Managing the Order of ${userName ? userName : "All Customer"}`}
          />
          <Box
            m="40px 0 0 0"
            height="75vh"
            sx={{
              "& .MuiDataGrid-root": {
                border: "none",
              },
              "& .MuiDataGrid-cell": {
                borderBottom: "none",
              },
              "& .name-column--cell": {
                color: colors.greenAccent[300],
              },
              "& .MuiDataGrid-columnHeaders": {
                backgroundColor: colors.blueAccent[700],
                borderBottom: "none",
              },
              "& .MuiDataGrid-virtualScroller": {
                backgroundColor: colors.primary[400],
              },
              "& .MuiDataGrid-footerContainer": {
                borderTop: "none",
                backgroundColor: colors.blueAccent[700],
              },
              "& .MuiCheckbox-root": {
                color: `${colors.greenAccent[200]} !important`,
              },
              "& .MuiDataGrid-toolbarContainer .MuiButton-text": {
                color: `${colors.grey[100]} !important`,
              },
              "& .MuiTablePagination-displayedRows": {
                margin: 0,
              },
              "& .MuiTablePagination-selectLabel": {
                margin: 0,
              },
            }}
          >
            <DataGrid
              columnVisibilityModel={columnVisibilityModel}
              onColumnVisibilityModelChange={(newModel) => {
                setColumnVisibilityModel(newModel);
              }}
              initialState={{
                pagination: { paginationModel: { pageSize: 5 } },
              }}
              pageSizeOptions={[5, 10, 25]}
              processRowUpdate={processRowUpdate}
              onCellClick={onBtnCellClick}
              editMode="row"
              getRowId={(order) => order._id}
              rows={orders ? orders : ordersData}
              columns={columns}
              rowModesModel={rowModesModel}
              onRowModesModelChange={handleRowModesModelChange}
              onRowEditStop={handleRowEditStop}
              isCellEditable={(params) => params.row.status == "Pending"}
              slots={{ toolbar: GridToolbar }}
            />
          </Box>
          <ViewDetailsProduct
            open={openProductModal}
            setOpen={setProductModal}
            customer={orderUser}
          />
          <DeleteOrderModal
            paramOrderId={paramOrderId}
            openModal={openModal}
            setOpenModal={setOpenModal}
            setOpenToast={setOpenToast}
            setMessageError={setMessageError}
            setMessageSuccess={setMessageSuccess}
          />
          <UpdateOrderModal
            openUpdateModal={openUpdateModal}
            setOpenUpdateModal={setOpenUpdateModal}
            order={order}
          />
          <ShopToast
            open={openToast}
            setOpen={setOpenToast}
            messageError={messageError}
            messageSuccess={messageSuccess}
          />
        </Box>
      )}
    </>
  );
};
{
  /*   <>
        <Grid item xs={12} style={{textAlign:'center'}}>
          <Typography variant="h3">
            Order Management
          </Typography> 
          </Grid>
          <SelectLimit />
          <Grid item xs={12} mt={4}>
            <TableContainer component={Paper}>
              <Table
              sx={{minWidth: 650}}>
                <TableHead>
                  <TableRow>
                  <TableCell>ID</TableCell>
                <TableCell>Order Date</TableCell>
                <TableCell>Estimate Shipping Date</TableCell>
                <TableCell>Order Code</TableCell>
                <TableCell>Stauts</TableCell>
                <TableCell>Total Price</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                {orders.map((order, index) => {
                  return (
                    <TableRow key={index}>
                    <TableCell>{index + 1}</TableCell>
                  <TableCell>{new Date(order.orderDate).toLocaleString()}</TableCell>
                  <TableCell>{new Date(order.shippedDate).toLocaleString()}</TableCell>
                  <TableCell>{order.orderCode}</TableCell>
                  <TableCell>{order.status}</TableCell>
                  <TableCell>{(order.cost).toString()} VND</TableCell>
                  <TableCell>
                    <EditNoteIcon onClick={() => onBtnUpdateClick(index)}/>
                    <UpdateOrderModal order={paramOrder} openUpdateModal={openUpdateModal} setOpenUpdateModal={setOpenUpdateModal}/>
                    <DeleteIcon onClick={() => onBtnDeleteClick(index)} />
                    <DeleteOrderModal order={paramOrder} openModal={openModal} setOpenModal={setOpenModal}/>
                  </TableCell>
                    </TableRow>
                  )
                })}
              
                </TableBody>
              </Table>
            </TableContainer>
           <PaginateComponent onBtnChangePage={onBtnChangePage} page={page} noPage={noPage} />
          </Grid>
      </>*/
}
