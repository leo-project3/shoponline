import { Typography, Container } from "@mui/material"
import { Link } from "react-router-dom"

//When no URL match this component will be displayed
export const NoPage404 = () => {
  return (
    <>
      <div style={{ display: "flex", height: "100vh", width: "100vw" }}>
        <Container sx={{ display: 'flex', justifyContent: "center", alignItems: 'center', flexDirection: "column" }}>
          <Typography sx={{ display: { xs: "none", sm: "block" } }} variant='h3' component='div' color='blue'>
            Devcamp Shop
          </Typography>
          <Typography sx={{ display: { xs: "block", sm: "none" } }} variant='h4' component='div' color='blue'>
            Devcamp Shop
          </Typography>
          <Typography variant='h5' component='div' color="red">
            404 Page has not found
          </Typography>
          <Link to="/" style={{ textDecoration: "none" }}>
            <Typography variant='body' component='div' color="green">
              Back to homepage
            </Typography>
          </Link>
        </Container>
      </div>
    </>
  )
}