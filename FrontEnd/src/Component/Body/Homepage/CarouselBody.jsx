import Carousel from "react-bootstrap/Carousel";
import { useState, useEffect } from "react";
import { Button, Grid, Typography } from "@mui/material";
import { Link } from "react-router-dom";
import axios from "axios";

const CarouselBody = () => {
  const [data, setData] = useState([]);
  //Get 5 products by default
  useEffect(() => {
    axios(process.env.REACT_APP_FETCH_URL + "/api/product").then((res) => {
      setData(res.data.products);
    });
  }, []);
  const slide = data.map((item, index) => (
    <Carousel.Item key={item._id}>
      <Grid
        container
        padding={"25px"}
        sx={{
          display: "flex",
          justifyContent: "space-around",
          alignItems: "center",
          flexDirection: { xs: "column-reverse", md: "row" },
        }}
      >
        <Grid
          item
          md={6}
          className="d-flex justify-content-center align-items-center"
        >
          <div className="text-center">
            <div>
              <h3 className="text-dark text-center">
                <b>{item.name}</b>
              </h3>
              <Typography variant="p" className="text-dark" textAlign="justify">
                {item.description}
              </Typography>
            </div>
            <div className="text-center">
              <Link to={`/products/${item._id}`} state={item.type}>
                <Button
                  variant="contained"
                  sx={{ backgroundColor: "Black", marginTop: "10px" }}
                >
                  Shop Now
                </Button>
              </Link>
            </div>
          </div>
        </Grid>
        <Grid item md={6} textAlign="center">
          <div className="">
            <img
              src={process.env.REACT_APP_FETCH_URL + "/upload/" + item.imageUrl.image[0]}
              alt="product-img"
              style={{
                minWidth: "100px",
                width: "100%",
                maxWidth: "425px",
                height: "100%",
                maxHeight: "350px",
                minHeight: "275px",
                borderRadius: "30px",
              }}
            />
          </div>
        </Grid>
      </Grid>
    </Carousel.Item>
  ));
  return (
    <Carousel
      style={{ padding: "20px", marginTop: "120px" }}
      fade
      data-bs-theme="dark"
      controls={false}
    >
      {slide}
    </Carousel>
  );
};

export default CarouselBody;
