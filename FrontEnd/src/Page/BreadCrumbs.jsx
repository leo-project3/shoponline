import { Breadcrumbs, Typography } from "@mui/material";
import { useSelector } from "react-redux";
import { Link, useLocation } from "react-router-dom";
export const BreadCrumbs = () => {
  return (
    <div role="presentation">
      <Breadcrumbs aria-label="breadcrumb">
        <Link
          underline="hover"
          style={{ textDecoration: "none", color: "black" }}
          to="/"
        >
          Hompage
        </Link>
        <Typography color="text.primary" fontWeight="bold">
          Products
        </Typography>
      </Breadcrumbs>
    </div>
  );
};
export const BreadCrumbs2 = () => {
  const { item } = useSelector((reduxData) => reduxData.detailsReducer);
  const location = useLocation();
  return (
    <div role="presentation">
      <Breadcrumbs aria-label="breadcrumb">
        <Link
          underline="hover"
          style={{ textDecoration: "none", color: "black" }}
          to="/"
        >
          Hompage
        </Link>

        <Link
          color="#000"
          underline="hover"
          style={{ textDecoration: "none", color: "black" }}
          to="/products"
        >
          Products
        </Link>
        <Typography color="text.primary" fontWeight="bold">
          {typeof location.state == "object" ? location.state.name : item.name}
        </Typography>
      </Breadcrumbs>
    </div>
  );
};
