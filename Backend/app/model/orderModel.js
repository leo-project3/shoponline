const mongoose = require("mongoose")
const Schema = mongoose.Schema;
const date = new Date()
const today = new Date(Date.now())
const orderSchema = new Schema({
  _id: mongoose.Types.ObjectId,
  customerId: {
    type: mongoose.Types.ObjectId,
    ref: "customer"
  },
  orderDate: {
    type: Date,
    default: Date.now()
  },
  shippedDate: {
    type: Date,
    default: date.setDate(date.getDate() + 7)
  },
  note: {
    type: String
  },
  orderDetails: [
    {
      type: mongoose.Types.ObjectId,
      ref: "orderDetails"
    }
  ],
  status: {
    type: String,
    default: "Pending",
    enum: ["Pending", "Confirmed", "Canceled"],
  },
  cost: {
    type: Number,
    default: 0
  }
  ,
  orderCode: {
    type: String,
    default: function () {
      var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
      var string_length = 8;
      var randomstring = '';
      for (var i = 0; i < string_length; i++) {
        var rnum = Math.floor(Math.random() * chars.length);
        randomstring += chars[rnum];
      }
      return randomstring;
    }
  },
  isDelete: {
    type: Boolean,
    default: true
  },
  updateAt: {
    type: Date,
    default: null
  },
  expires: {
    type: Date,
    default: today.setSeconds(today.getSeconds() + 172800)
  }
}, { timestamp: true })

orderSchema.index(
  { expires: 1 },
  { expireAfterSeconds: 10 })
module.exports = mongoose.model("order", orderSchema)