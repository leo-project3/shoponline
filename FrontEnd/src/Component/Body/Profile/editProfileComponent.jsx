import { Alert, Box, Modal, Snackbar } from "@mui/material";
import {
  Container,
  Grid,
  TextField,
  Typography,
  Button,
  Select,
  InputLabel,
} from "@mui/material";
import axios from "axios";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { SelectLocation } from "../Global/locationSelect";
import { ShopToast } from "../../../ToastMessagePopup/toastShop";


// Change address component
export const EditProfile = ({ edit, setEdit, id, customers, setCustomers }) => {
  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    bgcolor: "background.paper",
    border: "1px solid #000",
    borderRadius: "15px",
    boxShadow: 24,
    p: 4,
  };
  const [openSnack, setOpenSnack] = useState(false);
  const [messageSuccess, setMessageSuccess] = useState("");
  const [messageError, setMessageError] = useState("");
  const [city, setCity] = useState("");
  const [district, setDistrict] = useState("");
  const [ward, setWard] = useState("");
  const [error, setError] = useState({});
  const validateError = {};

  const validateData = () => {
    if (!city) {
      validateError.city = "City is required";
    }
    if (!district) {
      validateError.district = "District is required";
    }
    if (!ward) {
      validateError.ward = "Ward is required";
    }
    setError(validateError);
  };
  const onBtnUpdateInformationClick = async () => {
    validateData();

    const addressObj = {
      city: city,
      district: district,
      ward: ward,
    };
    if (Object.keys(validateError).length === 0) {
      try {
        const response = await axios.put(
          process.env.REACT_APP_FETCH_URL + "/api/customer/" + id,
          addressObj,
        );
        const data = await response.data;

        if (response.status == 200) {
          setMessageError("");
          setOpenSnack(true);
          setMessageSuccess(data.message);
          // setTimeout(() => window.location.reload(), 3000);
        }
      } catch (error) {
        setOpenSnack(true);

        setMessageError(error.response.data.message);
      }

      /*  try {
         const response = await axios.put(
           process.env.REACT_APP_FETCH_URL + "/api/customer/" + id,
           customerObj,
         );
         const data = await response.data;
   
         if (response.status == 200) {
           setMessageError("");
           setOpenSnack(true);
           setMessageSuccess(data.message);
           setTimeout(() => window.location.reload(), 3000);
         }
       } catch (error) {
         setOpenSnack(true);
   
         setMessageError(error.response.data.message);
       } */
      //  }
    };
  }

  return (
    <>
      <Modal open={edit} onClose={() => setEdit(false)}>
        <Box
          sx={style}
          style={{ minWidth: "300px", maxWidth: "400px", width: "100%" }}
        >
          <Typography variant="h5" component="div" textAlign="center">
            Edit Address
          </Typography>
          <SelectLocation
            error={error}
            setCustomers={setCustomers}
            customers={customers}
            setCity={setCity}
            setDistrict={setDistrict}
            city={city}
            ward={ward}
            setWard={setWard}
            district={district}
          />
          <Button
            fullWidth
            variant="contained"
            color="success"
            onClick={() => onBtnUpdateInformationClick()}
          >
            Update Address Information
          </Button>
        </Box>
      </Modal>
      <ShopToast
        open={openSnack}
        setOpen={setOpenSnack}
        messageError={messageError}
        messageSuccess={messageSuccess}
      />
    </>
  );
};
