import { combineReducers } from "redux";
import { dataReducer } from "./dataListReducer";
import { filterReducer } from "./filterReducer";
import { detailsReducer } from "./detailsItemReducer";
import { cartReducer } from "./cartReducer";
import { userReducer } from "./userReducer";
import { checkOutReducer } from "./checkOutReducer";
import { tableReducer } from "../admin/reducer/tableReducer";
import { customerReducer } from "../admin/reducer/customerReducer";
import { orderReducer } from "../admin/reducer/orderReducer";
import { productReducer } from "../admin/reducer/productReducer";
import { locationReducer } from "./locationReducer";
import { productDetailsReducer } from "../admin/reducer/productDetailsReducer";

//Main reducer for Shop24h Page
export const rootReducer = combineReducers({
  dataReducer,
  filterReducer,
  detailsReducer,
  cartReducer,
  userReducer,
  checkOutReducer,
  tableReducer,
  productReducer,
  customerReducer,
  orderReducer,
  locationReducer,
  productDetailsReducer,
});
