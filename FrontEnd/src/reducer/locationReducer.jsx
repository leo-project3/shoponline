import { GET_CITY } from "../contants/data";

const initialState = {
  cities: [],
};

//Control city selection
export const locationReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_CITY:
      state.cities = action.payload;
      break;
    default:
      break;
  }
  return { ...state };
};
