const express = require("express");
const router = express.Router();
const {
  createProductTypeMiddleware,
  getProductTypeMiddleware,
  getProductTypeByIdMiddleware,
  updateProductTypeMiddleware,
  deleteProductTypeMiddleware,
} = require("../middleware/productTypeMiddleware")
const {
  createProductType,
  getAllProductType,
  getProductTypeById,
  updateProductTypeById,
  deleteProductTypeById
} = require("../controller/productTypeController")
const { verifyToken, checkUser } = require("../middleware/authMiddleware");

router.get("/producttype", getProductTypeMiddleware, getAllProductType)
router.post("/producttype", [createProductTypeMiddleware, verifyToken, checkUser], createProductType)
router.get("/producttype/:id", getProductTypeByIdMiddleware, getProductTypeById)
router.put("/producttype/:id",[updateProductTypeMiddleware, verifyToken, checkUser], updateProductTypeById)
router.delete("/producttype/:id", [deleteProductTypeMiddleware, verifyToken, checkUser],deleteProductTypeById)

module.exports = router