import axios from "axios";
import { ERROR, FETCH_CUSTOMER_ORDERS, FETCH_USER } from "../contants/data";

export const getExistedUser = (user) => {
  return async (dispatch) => {
    try {
      const response = await axios(
        process.env.REACT_APP_FETCH_URL + "/api/customer/" + user,
      );
      const data = await response.data.Customer;
      return dispatch({
        type: FETCH_USER,
        payload: data,
      });
    } catch (error) {
      return {
        type: ERROR,
      };
    }
  };
};

export const getCustomerOrder = (user) => {

  return async (dispatch) => {
    try {
      const response = await axios(
        process.env.REACT_APP_FETCH_URL + "/api/customer/" + user,
      );
      const order = await response.data.Customer[0].orders;

      const response2 = await Promise.all(
        order.map(async (item) => {
          const data = await axios(
            process.env.REACT_APP_FETCH_URL + "/api/order/" + item._id,
          );
          return await data.data.order;
        }),
      );
      const sortData = await response2.sort((a, b) => new Date(b.orderDate) - new Date(a.orderDate))
      return dispatch({
        type: FETCH_CUSTOMER_ORDERS,
        payload: sortData,
      });
    } catch (error) {
      return dispatch({
        type: ERROR,
      });
    }
  };
};
