import { Box, Button, Container, Divider, Typography } from "@mui/material";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { orderDetailFetch } from "../../../action/checkOutAction";

export const TotalPriceContainer = () => {
  const dispatch = useDispatch();
  //Get product from localstorage
  const cart = JSON.parse(localStorage.getItem("cart"));

  //Caculate subtotal, shipping fee (order less than 100 million VND), and total price
  const subtotal = cart.reduce(
    (acc, item) => item.promotionPrice * item.quantity + acc,
    0,
  );
  let shippingFee;
  if (subtotal > 100000000) {
    shippingFee = 0;
  } else {
    shippingFee = 1000000;
  }
  let totalPrice = subtotal + shippingFee;
  const onBtnClick = () => {
    dispatch(orderDetailFetch(totalPrice));
  };
  return (
    <>
      <Box
        sx={{
          minWidth: "320px",
          maxWidth: "401px",
          width: "100%",
        }}
        className="bg-white p-4 rounded-4"
      >
        <Container style={{ padding: 0, paddingLeft: { sm: 0 }, width: "100%" }}>
          <Container sx={{ display: "flex", justifyContent: "flex-start", columnGap: "3px" }}>
            <Typography>Subtotal:</Typography>
            <Typography>{subtotal.toLocaleString()} VND</Typography>
          </Container>
          <Container sx={{ display: "flex", justifyContent: "flex-start", columnGap: "3px" }}>
            <Typography>Shipping:</Typography>
            <Typography>{shippingFee.toLocaleString()} VND</Typography>
          </Container>

          <Divider
            sx={{ margin: "5px 0px 5px 0px", backgroundColor: "black" }}
          />
          <Container sx={{ display: "flex", justifyContent: "flex-start", columnGap: "3px" }}>
            <Typography>Total:</Typography>
            <Typography color="error">
              <b>{totalPrice.toLocaleString()} VND</b>
            </Typography>
          </Container>
          <Container sx={{ textAlign: "center", marginTop: "15px" }}>
            <Link
              to={sessionStorage.getItem("User") ? "/checkout" : "/login"}
              state={{ totalPrice: totalPrice, shippingFee: shippingFee }}
              onClick={() => onBtnClick()}
            >
              <Button fullWidth color="success" variant="contained">
                CHECKOUT
              </Button>
            </Link>
          </Container>
        </Container>
      </Box >
    </>
  );
};
