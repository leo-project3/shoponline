import { ERROR } from "../../contants/data";
import axios from "axios";
import { UPDATE, GET_ORDER, GET_CUSTOMER, GET_PRODUCT } from "../contants/data";
import { useNavigate } from "react-router-dom";

export const updateOrder = (id, status, setMessageError, setMessageSuccess, setOpenToast) => {
  return async (dispatch) => {
    try {
      const response = await axios.put(
        process.env.REACT_APP_FETCH_URL + "/api/order/" + id,

        status
        ,
        {
          headers: {
            "x-access-token": sessionStorage.getItem("AccessToken"),
            "x-refresh-token": sessionStorage.getItem("RefreshToken"),
          },
        },
      );
      const data = await response.data;
      const response2 = await axios(
        process.env.REACT_APP_FETCH_URL + "/api/order/" + id,
      );
      const orderDArr = await response2.data.order.orderDetails;
      const updateOrderDetails = await Promise.all(
        orderDArr.map(async (orderD) => {
          await axios.put(
            process.env.REACT_APP_FETCH_URL + "/api/orderDetails/" + orderD._id,
          );
        }),
      );
      const response3 = await axios(
        process.env.REACT_APP_FETCH_URL + "/api/order",
      );
      const data3 = await response3.data.orders;
      const validOrders = await data3.filter((cus) => cus.isDelete !== true);
      setMessageSuccess(data.message)
      setOpenToast(true)
      dispatch({
        type: GET_ORDER,
        orderData: validOrders,
      });


    } catch (error) {
      setMessageError(error.response.data.message)
      setOpenToast(true)
      if (
        error.response.data.message == "Unauthourized" ||
        error.response.data.message == "Please Login" ||
        error.response.data.message == "Please Login Again"
      ) {
        sessionStorage.clear();
        setTimeout(
          () =>
            (window.location.href = process.env.REACT_APP_MAIN_URL + "/login"),
          4000,
        );
      }
    }
  };
};

export const updateProduct = (id, product, setMessageError, setMessageSuccess, setOpenToast) => {
  return async (dispatch) => {
    try {
      const response = await axios.put(
        process.env.REACT_APP_FETCH_URL + "/api/product/" + id,

        product,

        {
          headers: {
            "x-access-token": sessionStorage.getItem("AccessToken"),
            "x-refresh-token": sessionStorage.getItem("RefreshToken"),
          },
        },
      );
      const data = await response.data;
      const response2 = await axios(
        process.env.REACT_APP_FETCH_URL + "/api/product" + process.env.REACT_APP_LIMIT_PRODUCT,
      );
      const data2 = await response2.data.products;
      const validProduct = await data2.filter((cus) => cus.isDelete !== true);
      setMessageSuccess(data.message)
      setOpenToast(true)
      dispatch({
        type: GET_PRODUCT,
        payload: validProduct,
      });

    } catch (error) {
      setMessageError(error.response.data.message)
      setOpenToast(true)
      if (
        error.response.data.message == "Unauthourized" ||
        error.response.data.message == "Please Login" ||
        error.response.data.message == "Please Login Again"
      ) {
        sessionStorage.clear();
        setTimeout(() => (window.location.href = "/login"), 4000);
      }
    }
  };
};

export const updateCustomer = (id, customer) => {
  return async (dispatch) => {
    try {
      const response = await axios.put(
        process.env.REACT_APP_FETCH_URL + "/api/customer/" + id,
        {
          body: customer,
        },
        {
          headers: {
            "x-access-token": sessionStorage.getItem("AccessToken"),
            "x-refresh-token": sessionStorage.getItem("RefreshToken"),
          },
        },
      );
      const data = await response.data;
      const response2 = await axios(
        process.env.REACT_APP_FETCH_URL + "/api/customer",
      );
      const data2 = await response2.data.Customer;
      const validCustomer = await data2.filter((cus) => cus.isDelete !== true);

      dispatch({
        type: GET_CUSTOMER,
        payload: validCustomer,
      });
      return dispatch({
        type: UPDATE,
        payload: data.message,
      });
    } catch (error) {
      dispatch({
        type: ERROR,
        error: error.response.data.message,
      });
      if (
        error.response.data.message == "Unauthourized" ||
        error.response.data.message == "Please Login" ||
        error.response.data.message == "Please Login Again"
      ) {
        sessionStorage.clear();
        setTimeout(() => (window.location.href = "/login"), 4000);
      }
    }
  };
};
