import axios from "axios";
import { ERROR, PENDING, FETCH_DETAILS_ITEM } from "../contants/data";

export const fetchDetailProduct = (id) => {
  return async (dispatch) => {
    try {
      await dispatch({
        type: PENDING,
      });
      const response = await axios(
        process.env.REACT_APP_FETCH_URL + "/api/product/" + id,
      );
      const data = await response.data;
      return dispatch({
        type: FETCH_DETAILS_ITEM,
        item: data.product,
      });
    } catch (error) {
      return dispatch({
        type: ERROR,
        error: error,
      });
    }
  };
};
