import * as React from "react";
import Box from "@mui/material/Box";
import Slider from "@mui/material/Slider";
import { TextField, Typography } from "@mui/material";
import { useSelector } from "react-redux";
import { useEffect } from "react";
import { useState } from "react";
import axios from "axios";

function valuetext(value) {
  return `${value}VND`;
}

const mark = [
  { value: 10000000, label: "10M" },
  { value: 40000000, label: "40M" },
  { value: 70000000, label: "70M" },
];

export default function RangeSlider({

  value,
  setMnPrice,
  setMxPrice,
  setValue,
}) {
  const [item, setItem] = useState([])
  useEffect(() => {
    axios(process.env.REACT_APP_FETCH_URL + "/api/product" + process.env.REACT_APP_LIMIT_PRODUCT)
      .then((response) => setItem(response.data.products))//setItem(response.data.products)
  }, [])
  const handleChange = (event, newValue) => {
    setValue(newValue);
    setMnPrice(newValue[0]);
    setMxPrice(newValue[1]);
  };
  const price = item.map((item) => item.promotionPrice)
  const maxPrice = Math.max(...price)
  return (
    <Box sx={{ width: "15rem" }}>
      <Typography>Price (VND)</Typography>
      <Slider
        valueLabelFormat={() => value[0].toLocaleString() + ' - ' + value[1].toLocaleString()}
        marks={mark}
        getAriaLabel={() => "Price range"}
        value={value}
        step={100000}
        max={maxPrice}
        onChange={handleChange}
        valueLabelDisplay="auto"
      //  getAriaValueText={valuetext}
      />
    </Box>
  );
}
