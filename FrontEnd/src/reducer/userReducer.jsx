import { GET_USER } from "../contants/data";

const initialState = {
  name: "",
  isAdmin: false,
};

//Control users
export const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_USER:
      state.name = action.name;
      state.isAdmin = action.admin;
      break;
    default:
      break;
  }
  return { ...state };
};
