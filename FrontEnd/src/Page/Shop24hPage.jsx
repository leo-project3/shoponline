import { Route, Routes } from "react-router-dom";
import { router, adminRouter } from "../router/router";
import axios from "axios";
import { useEffect, useState } from "react";
import { AdminPage } from "../admin/page/adminPage";
import { NoPage404 } from "../Component/Body/No Found Component/404page";

export const ShopPage24h = () => {
  const [admin, setAdmin] = useState(false);
  const user = sessionStorage.getItem("User");
  useEffect(() => {
    axios(process.env.REACT_APP_FETCH_URL + "/api/user/" + user)
      .then((data) => setAdmin(data.data.data.isAdmin))
      .catch((error) => setAdmin(false));
  }, [user]);

  return (
    <>
      <Routes>
        {router.map((route, index) => {
          if (route.path) {
            return (
              <Route
                path={route.path}
                key={route.path}
                element={route.element}
              ></Route>
            );
          } else {
            return null;
          }
        })}
        <Route path="/admin/*" element={<AdminPage admin={admin} />}></Route>
        <Route path="*" element={<NoPage404 />}></Route>
      </Routes>
    </>
  );
};
