import IconHeader from "./HeaderNav/IconNavHeader";
import Navigator from "./HeaderNav/HeaderNavigator";
import { LoginNav } from "./HeaderNav/loginNavBar";
import { AppBar, Container, Toolbar, IconButton, Box } from "@mui/material";
import React, { useEffect } from "react";
import ArrowCircleUpIcon from '@mui/icons-material/ArrowCircleUp';

import { useState } from "react";
const Header = () => {
  const accesstoken = sessionStorage.getItem("AccessToken");
  const [anchorElNav, setAnchorElNav] = React.useState(null);

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };
  const [showsScrolBtn, setShowScrolBtn] = useState(false);

  useEffect(() => {
    const handleButtonVisibility = () => {
      window.pageYOffset > 300 ? setShowScrolBtn(true) : setShowScrolBtn(false);
    };

    window.addEventListener("scroll", handleButtonVisibility);
    return () => {
      window.addEventListener("scroll", handleButtonVisibility);
    };
  }, []);

  return (
    <>
      <AppBar position="fixed">
        <Container maxWidth="xl">
          <Toolbar disableGutters>
            <Navigator
              anchorElNav={anchorElNav}
              setAnchorElNav={setAnchorElNav}
              handleOpenNavMenu={handleOpenNavMenu}
              handleCloseNavMenu={handleCloseNavMenu}
            />

            {accesstoken ? <IconHeader /> : <LoginNav />}
          </Toolbar>
        </Container>
      </AppBar>

      {showsScrolBtn &&
        <Box
          id="scrolToTop"
          onClick={() => {
            window.scrollTo({
              top: 0,
              left: 0,
              behavior: "smooth",
            });
          }}
          sx={{
            position: "fixed",
            bottom: "60px",
            right: "15px",
            color: "gray",
            textAlign: "center",
            cursor: "pointer",
            lineHeight: 0,
            textDecoration: "none",
          }}
        >
          <IconButton sx={{ backgroundColor: "#7874746b" }}>
            <ArrowCircleUpIcon sx={{ fontSize: { lg: "35px", md: "30px", sm: "25px" } }} />
          </IconButton>
        </Box>
      }



    </>
  );
};

export default Header;
