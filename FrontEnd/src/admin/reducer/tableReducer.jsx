import {
  PENDING,
  GET_CUSTOMER,
  GET_PRODUCT,
  GET_ORDER,
  ORDER_CHANGE_PAGE,
  CHANGE_LIMIT,
  GET_CATEGORY,
  GET_PRODUCT_DETAILS,
} from "../contants/data";

const initialState = {
  ordersData: [],
  customers: [],
  products: [],
  categories: [],
  noPage: 0,
  page: 1,
  limit: 10,
  pending: false,
};

export const tableReducer = (state = initialState, action) => {
  switch (action.type) {
    case PENDING:
      state.pending = true;
      break;
    case GET_ORDER:
      state.pending = false;
      state.ordersData = action.orderData;
      state.orderCustomer = action.payload;
      state.noPage = Math.ceil(action.totalPage / state.limit);
      break;
    case GET_CATEGORY:
      state.pending = false;
      state.categories = action.category;
      break;
    case GET_CUSTOMER:
      state.pending = false;
      state.customers = action.payload;
      state.noPage = Math.ceil(action.totalPage / state.limit);
      break;
    case GET_PRODUCT:
      state.pending = false;
      state.products = action.payload;
      state.noPage = Math.ceil(action.totalPage / state.limit);
      break;
    case ORDER_CHANGE_PAGE:
      state.page = action.page;
      break;
    case CHANGE_LIMIT:
      state.limit = action.limit;
      break;
    default:
      break;
  }
  return { ...state };
};
