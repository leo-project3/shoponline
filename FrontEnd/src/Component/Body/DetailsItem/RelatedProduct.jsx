import { Card, CardContent, CardMedia, Grid, Typography } from "@mui/material";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchRelatedProduct } from "../../../action/filterAction";
import { Link, useLocation } from "react-router-dom";

export const RelatedProduct = () => {
  const dispatch = useDispatch();
  const { relatedProduct } = useSelector(
    (reduxData) => reduxData.filterReducer,
  );
  let vQueryString = new URLSearchParams();
  const location = useLocation();
  vQueryString.append("type", location.state);
  useEffect(() => {
    dispatch(fetchRelatedProduct(vQueryString));
  }, []);
  //Card for related product
  const card = relatedProduct.map((item, index) => {
    return (
      <Grid item sm={6} md={4} lg={3} key={item._id}>
        <Link
          style={{ textDecoration: "none" }}
          to={`/products/${item._id}`}
          state={item}
        >
          <Card
            sx={{
              maxWidth: 345,
              display: "flex",
              alignItems: "center",
              flexDirection: "column",
            }}
          >
            <CardMedia
              sx={{ height: 280, width: 250 }}
              image={process.env.REACT_APP_FETCH_URL + "/upload/" + item.imageUrl.image[0]}
              title="Product image"
              style={{ borderRadius: "10px", paddingTop: "10px" }}
            />
            <CardContent>
              <Typography>{item.name}</Typography>{" "}
              <Typography textAlign="center" color="error">
                <b>{item.buyPrice}</b>
              </Typography>
            </CardContent>
          </Card>
        </Link>
      </Grid>
    );
  });
  return (
    <>
      <Grid container spacing={2} sx={{ justifyContent: "center" }}>
        <Grid item xs={12} textAlign="center">
          <Typography variant="h4" component="div">
            Related Products
          </Typography>
        </Grid>
        {card}
      </Grid>
    </>
  );
};
