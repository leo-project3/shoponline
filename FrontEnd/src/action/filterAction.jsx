import axios from "axios";
import {
  FETCH_FILTER_DATA,
  ERROR,
  PENDING,
  FETCH_RELATED_PRODUCT,
} from "../contants/data";
export const fetchFilterData = (product, type, mnPrice, mxPrice, pageFilter, limit) => {
  return async (dispatch) => {
    let skip = (pageFilter - 1) * limit
    let vQueryString1 = new URLSearchParams();
    vQueryString1.append("name", product);
    vQueryString1.append("type", type);
    vQueryString1.append("minPrice", mnPrice);
    vQueryString1.append("maxPrice", mxPrice);
    vQueryString1.append("skip", skip)
    vQueryString1.append("limit", limit)
    let vQueryString2 = new URLSearchParams();
    vQueryString2.append("name", product);
    vQueryString2.append("type", type);
    vQueryString2.append("minPrice", mnPrice);
    vQueryString2.append("maxPrice", mxPrice);
    vQueryString2.append("limit", 100)
    try {
      await dispatch({
        type: PENDING,
      });
      const response = await axios(
        process.env.REACT_APP_FETCH_URL + "/api/product?" + vQueryString1.toString()
      );
      const data = await response.data.products;
      const response2 = await axios(
        process.env.REACT_APP_FETCH_URL + "/api/product?" + vQueryString2.toString(),
      );

      const data2 = await response2.data.products;
      const validProduct = await data.filter((cus) => cus.isDelete != true);
      await dispatch({
        type: FETCH_FILTER_DATA,
        item: validProduct,
        totalFilterProduct: data2.length,
      });
    } catch (error) {
      return dispatch({
        type: ERROR,
        error: error,
      });
    }
  };
};

export const fetchRelatedProduct = (type) => {
  return async (dispatch) => {
    try {
      await dispatch({
        type: PENDING,
      });
      const response = await axios(
        process.env.REACT_APP_FETCH_URL + "/api/product?" + type.toString(),
      );
      const data = await response.data;
      const data2 = data.products;
      return dispatch({
        type: FETCH_RELATED_PRODUCT,
        item: data2,
      });
    } catch (error) {
      return dispatch({
        type: ERROR,
        error: error,
      });
    }
  };
};
