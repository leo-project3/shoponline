import { Button, Grid, Typography } from "@mui/material";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import { useDispatch } from "react-redux";
import { useState } from "react";
import { useSelector } from "react-redux";
import {
  deleteCustomer,
  deleteOrder,
  deleteProduct,
} from "../../action/deleteAction";
import { useNavigate } from "react-router-dom";
const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 650,
  borderRadius: "10px",
  bgcolor: "background.paper",
  border: "1px solid #000",
  boxShadow: 24,
  p: 4,
};
export const DeleteOrderModal = ({
  openModal,
  setOpenToast,
  setOpenModal,
  paramOrderId,
  setMessageError,
  setMessageSuccess
}) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { remove, error } = useSelector((reduxData) => reduxData.orderReducer);
  const [success, setSuccess] = useState(false);
  const onBtnDeleteOrder = () => {
    dispatch(deleteOrder(paramOrderId, setMessageError, setMessageSuccess, setOpenToast));
    /* setOpenToast(true); */
    setOpenModal(false);
  };

  return (
    <div>
      <Modal
        open={openModal}
        onClose={() => setOpenModal(false)}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Grid container>
            <Grid item sm={12} textAlign="center">
              <Typography variant="h6">
                Are you sure to delete this order
              </Typography>
            </Grid>
            <Grid item sm={12} textAlign="center" mt={2}>
              <Button
                variant="contained"
                color="error"
                onClick={onBtnDeleteOrder}
              >
                Delete
              </Button>
            </Grid>
          </Grid>
        </Box>
      </Modal>
      {success ? (
        <Modal
          open={success}
          onClose={() => setSuccess(false)}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={style}>
            <Grid container>
              <Grid item sm={12}>
                <Typography variant="h4">
                  {error == 401
                    ? "Error: Your account Unauthorized"
                    : "Delete Successfully"}
                </Typography>
              </Grid>
            </Grid>
          </Box>
        </Modal>
      ) : null}

      {/*error == 401 
      ?    <Modal
      open={success}
      onClose={() => setSuccess(false)}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={style}>
        <Grid container>
            <Grid item sm={12}>
             <Typography variant='h4'></Typography>
            </Grid>
        </Grid>
        </Box>
    </Modal>
      : null*/}
    </div>
  );
};

export const DeleteProductModal = ({
  openDeleteModal,
  setOpenDeleteModal,
  paramProduct,
  setOpenToast,
  setMessageError,
  setMessageSuccess
}) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { remove, error } = useSelector(
    (reduxData) => reduxData.productReducer,
  );
  const [success, setSuccess] = useState(false);
  const onBtnDeleteProduct = () => {
    dispatch(deleteProduct(paramProduct, setMessageError, setMessageSuccess, setOpenToast));
    setOpenDeleteModal(false);
    setOpenToast(true);
  };
  return (
    <div>
      <Modal
        open={openDeleteModal}
        onClose={() => setOpenDeleteModal(false)}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Grid container>
            <Grid item sm={12} textAlign="center">
              <Typography variant="h6">
                Are you sure to delete this product?
              </Typography>
            </Grid>
            <Grid item sm={12} textAlign="center" mt={2}>
              <Button
                variant="contained"
                color="error"
                onClick={onBtnDeleteProduct}
              >
                Delete
              </Button>
            </Grid>
          </Grid>
        </Box>
      </Modal>
      {success ? (
        <Modal
          open={success}
          onClose={() => setSuccess(false)}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={style}>
            <Grid container>
              <Grid item sm={12}>
                <Typography variant="h4">
                  {error == 401
                    ? "Error: Your account Unauthorized"
                    : "Delete Successfully"}
                </Typography>
              </Grid>
            </Grid>
          </Box>
        </Modal>
      ) : null}
    </div>
  );
};

export const DeleteCustomerModal = ({
  open,
  setOpen,
  customerId,
  setOpenToast,
  setMessageError,
  setMessageSuccess
}) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [success, setSuccess] = useState(false);

  const onBtnDeleteCustomer = () => {
    dispatch(deleteCustomer(customerId, setMessageError, setMessageSuccess, setOpenToast));
    setOpen(false);
    setOpenToast(true);
  };
  return (
    <div>
      <Modal
        open={open}
        onClose={() => setOpen(false)}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Grid container>
            <Grid item sm={12} textAlign="center">
              <Typography variant="h6">Are you sure to delete:</Typography>
            </Grid>
            <Grid item sm={12} textAlign="center" mt={2}>
              <Button
                variant="contained"
                color="error"
                onClick={onBtnDeleteCustomer}
              >
                Delete
              </Button>
            </Grid>
          </Grid>
        </Box>
      </Modal>
    </div>
  );
};
