const mongoose = require("mongoose");
const productModel = require("../model/productModel");
const productTypeModel = require("../model/productTypeModel")
const orderDetailsModel = require("../model/orderDetailsModel")

const createProduct = async (req, res) => {
  //const orderDetailsId = req.params.orderDetailsId
  const {
    name,
    description,
    type,
    imageUrl,
    buyPrice,
    promotionPrice,
    amount
  } = req.body
  if (name == "") {
    return res.status(400).json({
      message: "Name cant be blank"
    })
  }
  if (imageUrl == "") {
    return res.status(400).json({
      message: "Image cant be blank"
    })
  }
  if (buyPrice == "" || isNaN(buyPrice)) {
    return res.status(400).json({
      message: "buyPrice cant be blank and must be a number"
    })
  }
  if (promotionPrice == "" || isNaN(promotionPrice)) {
    return res.status(400).json({
      message: "promotion price cant be blank and must be a number"
    })
  }
  const getType = await productTypeModel.findOne({ name: type })
  let addType
  if (getType) {
    addType = getType.name
  }
  const existedProduct = await productModel.findOne({ name: name })
  if (existedProduct) {
    return res.status(400).json({
      message: "Product name is duplicate"
    })
  } else {
    var newProduct = await new productModel({
      _id: new mongoose.Types.ObjectId,
      name,
      description,
      type,
      imageUrl,
      buyPrice,
      promotionPrice,
      amount
    })

    productModel.create(newProduct)
      .then((data) => {
        /* orderDetailsModel.findByIdAndUpdate(orderDetailsId, {
           product: data._id
         })
         .then((result) => {*/
        res.status(201).json({
          message: "Create successfully",
          product: data
        })
      })
      .catch((err) => {
        res.status(500).json({
          message: "Error" + err.message
        })
      })
  }
}
const getAllProduct = async (req, res) => {
  const {
    name,
    type,
    minPrice,
    maxPrice,
    skip,
    limit
  } = req.query
  let condition = {}
  let handleLimitAndSkip = {}
  if (name) {
    condition.name = { $regex: name, $options: "i" }
  }
  if (type) {
    condition.type = type
  }
  if ((minPrice || maxPrice) && (minPrice > 0 && maxPrice > 0)) {
    condition.promotionPrice = { $gte: minPrice, $lte: maxPrice }
  } else if (minPrice == 0 && maxPrice > 0) {
    condition.promotionPrice = { $lte: maxPrice }
  } else if (maxPrice == 0 && minPrice > 0) {
    condition.promotionPrice = { $gte: minPrice }
  } else if ((minPrice < 0 && maxPrice < 0)) {
    condition.promotionPrice = 0
  } else if ((minPrice && maxPrice) && (isNaN(minPrice) && isNaN(maxPrice))) {
    condition.promotionPrice = 0
  }
  if (!limit || isNaN(limit) || limit < 0) {
    handleLimitAndSkip.limit = 5
  } else if (limit) {
    handleLimitAndSkip.limit = limit
  }
  if (!skip || isNaN(skip) || skip < 0) {
    handleLimitAndSkip.skip = 0
  } else if (skip) {
    handleLimitAndSkip.skip = skip
  }
  condition.isDelete = false
  try {
    const result = await productModel.find(condition).skip(handleLimitAndSkip.skip).limit(handleLimitAndSkip.limit).sort({ promotionPrice: 1 }).populate("imageUrl", "image");
    if (result) {
      return res.status(200).json({
        message: "Get All product",
        products: result
      })
    } else {
      return res.status(404).json({
        message: "Something gone wrong"
      })
    }
  } catch (error) {
    return res.status(500).json({
      message: "Error" + error.message
    })
  }
}

const getProductById = async (req, res) => {
  const id = req.params.id;
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).json({
      message: "Id is not valid"
    })
  }
  try {
    const result = await productModel.findById(id).populate("imageUrl", "image")
    if (result) {
      return res.status(200).json({
        message: "Get product",
        product: result
      })
    } else {
      return res.status(404).json({
        message: "Something gone wrong"
      })
    }
  } catch (error) {
    return res.status(500).json({
      message: "Error"
    })
  }
}

const updateProductById = async (req, res) => {
  const id = req.params.id;
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).json({
      message: "Id is not valid"
    })
  }

  // Only allow to change price, promotionprice, amount of the product
  const {
    name,
    imageUrl,
    description,
    buyPrice,
    promotionPrice,
    amount,
    type
  } = req.body
  /* if(buyPrice == "" || isNaN(buyPrice)){
     return res.status(400).json({
       message: "buyPrice cant be blank and must be a number"
     })
   }
   if(promotionPrice == "" || isNaN(promotionPrice)){
     return res.status(400).json({
       message: "promotion price cant be blank and must be a number"
     })
   }*/
  try {
    var updateProduct = {}

    if (name) {
      updateProduct.name = name
    }

    if (imageUrl) {
      updateProduct.imageUrl = imageUrl
    }
    if (buyPrice) {
      updateProduct.buyPrice = buyPrice
    }
    if (promotionPrice) {
      updateProduct.promotionPrice = promotionPrice
    }
    if (amount) {
      updateProduct.amount = amount
    }
    if (type) {
      const getType = await productTypeModel.findOne({ name: type })
      updateProduct.type = getType.name
    }
    if (description) {
      updateProduct.description = description
    }
    const result = await productModel.findByIdAndUpdate(id, updateProduct)
    if (result) {
      console.log(result)
      return res.status(200).json({
        message: "Updated product",
        product: result
      })
    } else {
      return res.status(404).json({
        message: "Something gone wrong"
      })
    }
  } catch (error) {
    return res.status(500).json({
      message: "Error" + error.message
    })

  }

}

const deleteProductById = async (req, res) => {
  //const orderDetailsId = req.params.orderDetailsId
  const id = req.params.id
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).json({
      message: "Id is not valid"
    })
  }
  /*if(!mongoose.Types.ObjectId.isValid(orderDetailsId)){
    return res.status(400).json({
      message: "Id is not valid"
    })
  }*/
  try {
    const result = await productModel.findByIdAndUpdate(id, { isDelete: true });
    if (result) {
      /* await orderDetailsModel.findByIdAndUpdate(orderDetailsId, {
         product: ""
       })*/
      return res.status(200).json({
        message: "Delete Product successfully"
      })
    } else {
      return res.status(404).json({
        message: "No Product id founded"
      })
    }
  } catch (error) {
    return res.status(500).json({
      message: "Error" + error.message
    })
  }
}
module.exports = {
  createProduct,
  getAllProduct,
  getProductById,
  updateProductById,
  deleteProductById
}