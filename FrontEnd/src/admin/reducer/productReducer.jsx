import { ERROR } from "../../contants/data";
import { CREATE, DELETE, UPDATE } from "../contants/data";

const initialState = {
  remove: "",
  success: "",
  create: "",
  error: "",
};

export const productReducer = (state = initialState, action) => {
  state = {
    error: "",
    success: "",
    create: "",
    remove: ""
  }
  switch (action.type) {
    case DELETE:
      state.error = "";
      state.success = "";
      state.create = "";
      state.remove = action.status;
      break;
    case UPDATE:
      state.error = "";
      state.remove = "";
      state.create = "";
      state.success = action.payload;
      break;
    case CREATE:
      state.error = "";
      state.success = "";
      state.remove = "";
      state.create = action.payload;
      break;
    case ERROR:
      state.success = "";
      state.remove = "";
      state.create = "";
      state.error = action.error;

      break;
    default:
      break;
  }
  return { ...state };
};
