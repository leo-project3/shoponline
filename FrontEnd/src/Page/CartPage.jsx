import Header from "../Component/Header/header";
import Footer from "../Component/Footer/footer";
import { CartComponent } from "../Component/Body/Cart/cartComponent";
import { EmptyCart } from "../Component/Body/Cart/EmptyCartComponent";
import { useState } from "react";
import { Container } from "@mui/material";

export const CartPage = () => {
  const [cart, setCart] = useState();
  const cartItem = JSON.parse(localStorage.getItem("cart"));

  const onBtnAdd = (product) => {
    const item = cartItem.find((item) => item._id === product._id);
    if (item) {
      item.quantity++;
    }
    localStorage.setItem("cart", JSON.stringify(cartItem));
    setCart(cartItem);
  };

  const onBtnMinus = (product) => {
    const item = cartItem.find((item) => item._id === product._id);
    if (item && item.quantity > 0) {
      item.quantity--;
    }
    if (item.quantity == 0) {
      const index = cartItem.findIndex((item) => item._id == product._id);
      if (index != -1) {
        cartItem.splice(index, 1);
      }
    }
    localStorage.setItem("cart", JSON.stringify(cartItem));
    setCart(cartItem);
  };

  const onBtnDelete = (product) => {
    const index = cartItem.findIndex((item) => item._id == product._id);
    if (index != -1) {
      cartItem.splice(index, 1);
    }
    localStorage.setItem("cart", JSON.stringify(cartItem));
    setCart(cartItem);
  };
  return (
    <>
      <Header />

      {cartItem == null || cartItem.length == 0 ? (
        <EmptyCart />
      ) : (
        <CartComponent
          cart={cart}
          onBtnAdd={onBtnAdd}
          onBtnMinus={onBtnMinus}
          onBtnDelete={onBtnDelete}
        />
      )}

      <Container
        maxWidth
        style={{ width: "100%", padding: 0 }}
        sx={{ height: { xs: "50%", sm: "auto" }, position: "absolute", bottom: { md: "-120px" }, marginTop: { xs: "100px", md: 0 } }}
      >
        <Footer />
      </Container>




    </>
  );
};
