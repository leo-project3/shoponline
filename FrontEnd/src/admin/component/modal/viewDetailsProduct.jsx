import * as React from "react";
import {
  Box,
  Modal,
  Typography,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableBody,
  TableCell,
  Paper,
  Container,
  Grid,
  Button,
  Divider,
  TableFooter,
} from "@mui/material";
import { useSelector } from "react-redux";
import { Tab } from "bootstrap";
import { useState } from "react";
import axios from "axios"
import { useEffect } from "react";
const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: { xs: "100%", sm: "85%" },
  maxWidth: "1200px",
  overflowY: "scoll",
  borderRadius: "10px"
};


export const ViewDetailsProduct = ({ open, setOpen, customer }) => {
  const { orderDetails, product } = useSelector(
    (reduxData) => reduxData.productDetailsReducer,
  );
  const mapProduct = product.map((item) => item.imageUrl);
  const card = orderDetails.map((item, index) => {
    return (
      <TableRow key={item._id}>
        <TableCell sx={{ display: "flex", alignItems: "center" }}>
          <Typography component='div'>
            {product.map((product) => {
              if (product._id == item.product) {
                return (
                  <img
                    key={index}
                    src={process.env.REACT_APP_FETCH_URL + "/upload/" + product.imageUrl.image[0]}
                    width="75px"
                    height="75px"
                    style={{ marginRight: "10px", borderRadius: "10px" }}
                  />
                );
              }
            })}
          </Typography>
          <Typography component='div'>
            {product.map((product) => {
              if (product._id == item.product) {
                return product.name;
              }
            })}
          </Typography>
        </TableCell>
        {customer ? (
          <TableCell align="center" sx={{ color: "pink" }}>
            <b>{customer}</b>
          </TableCell>
        ) : null}
        <TableCell align="center" sx={{ color: "light-grey" }}>
          <b>{item.quantity}</b>
        </TableCell>
        <TableCell align="center" sx={{ color: "blue" }}>
          <b>
            {product.map((product) => {
              if (product._id == item.product) {
                return product.promotionPrice.toLocaleString();
              }
            })}{" "}
            VND
          </b>
        </TableCell>
        <TableCell align="center" sx={{ color: "red" }}>
          <b>
            {product.map((product) => {
              if (product._id == item.product) {
                return (
                  product.promotionPrice * item.quantity
                ).toLocaleString();
              }
            })}{" "}
            VND
          </b>
        </TableCell>
      </TableRow>
    );
  });
  return (
    <div>
      <Modal
        open={open}
        onClose={() => setOpen(false)}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      // sx={{overflowY: 'scroll'}} disableScrollLock={false}
      >
        <Box sx={style}>
          <TableContainer
            component={Paper}
            sx={{
              maxHeight: "550px",
            }}
          >
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Product</TableCell>
                  {customer ? (
                    <TableCell align="center">Customer</TableCell>
                  ) : null}
                  <TableCell align="center">Quantity</TableCell>
                  <TableCell align="center">Price per Product</TableCell>
                  <TableCell align="center">Total Product Price</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>{card}</TableBody>
            </Table>
          </TableContainer>
        </Box>
      </Modal>
    </div>
  );
};
export const ViewDetailsProductShop = ({ open, setOpen, customer, order }) => {
  const card = order?.orderDetails.map((item, index) => {
    return (
      <TableRow key={item._id}>
        <TableCell sx={{ display: "flex", alignItems: "center" }}>
          <Typography component='div'>

            <img
              key={index}
              src={process.env.REACT_APP_FETCH_URL + "/upload/" + item.product.imageUrl.image[0]}
              width="75px"
              height="75px"
              style={{ marginRight: "10px", borderRadius: "10px" }}
            />

          </Typography>
          <Container>
            <Typography component='div'>
              {item.product.name}
            </Typography>
            <Box sx={{ display: 'flex', alignItems: "center", justifyContent: "space-between" }}>
              <Box>
                <Typography>
                  <b>Qty: {item.quantity}</b>
                </Typography>
                <Typography>
                  <b>
                    {item.product.promotionPrice.toLocaleString()}
                    {" "}
                    VND
                  </b>
                </Typography>
              </Box>
              <Button>Rate</Button>
            </Box>
          </Container>
        </TableCell>



      </TableRow>
    );
  });
  return (
    <div>
      <Modal
        open={open}
        onClose={() => setOpen(false)}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
        sx={{ borderRadius: "10px" }} disableScrollLock={false}
      >
        <Box sx={style}>

          <Grid container item spacing={2} sx={{ justifyContent: "space-between", backgroundColor: "white", borderBottom: "1px solid black", maxWidth: "1200px", margin: 0, width: "100%" }}>
            <Grid item xs={12} style={{
              padding: 0,
            }}>
              <Typography padding={2} textAlign='center' fontWeight='bold' variant="h5">Order Details</Typography>
            </Grid>
            <Grid item xs={12} sm={6} sx={{ backgroundColor: 'whitesmoke', height: "auto", borderRight: { sm: "1px solid black", xs: "none" }, borderBottom: { xs: "1px solid black", sm: "none" } }} style={{
              padding: 0,
            }}>
              <Box sx={{ padding: 2 }}>
                <Typography>Name: {customer && customer[0].fullName}</Typography>
                <Typography>Phone: {customer && customer[0].phone}</Typography>
                <Typography>Email: {customer && customer[0].email}</Typography>
                <Typography>Address: {customer && customer[0].address + ", " + customer && customer[0].ward + ", " + customer && customer[0].district + ", " + customer && customer[0].city}</Typography>

              </Box>
            </Grid>

            <Grid item xs={12} sm={6} style={{
              padding: 0,
            }} sx={{ backgroundColor: 'whitesmoke' }}>
              <Box sx={{ padding: 2 }}>
                <Typography>Order Code: {order?.orderCode}</Typography>
                <Typography>Order Date: {new Date(order?.orderDate).toLocaleDateString()}</Typography>
                <Typography>Estimate Ship Date: {new Date(order?.shippedDate).toLocaleDateString()}</Typography>
                <Typography color={order?.status == "Canceled" ? "red" : order?.status == "Pending" ? "Blue" : "Green"}>Status: {order?.status}</Typography>

              </Box>
            </Grid>
          </Grid>
          <Grid item xs={12} sx={{ backgroundColor: "white" }}>
            <Box sx={{
              height: "fit-content"
            }}>
              <TableContainer
                sx={{
                  borderRadius: "10px",
                  backgroundColor: "whitesmoke",
                  maxHeight: "300px",
                }}
              >
                <Table>
                  <TableBody>{card}</TableBody>
                </Table>
              </TableContainer>
            </Box>
          </Grid>
        </Box>

      </Modal>
    </div >
  );
};
