export const orderDetailFetchAdmin = async (totalPrice, customer, setMessageError, setMessageSuccess, setOpenSnack) => {

  const cart = JSON.parse(localStorage.getItem("cart"));
  let orderDetailsArr = cart.map(async (product) => {
    return await fetch(
      process.env.REACT_APP_FETCH_URL + "/api/orderDetails",
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          productId: product._id,
          quantity: product.quantity,
        }),
      },
    ).then((response) =>
      response.json().then((data) => {
        return data.orderdetails._id;
      }),
    );
  });
  const orderDetail = await Promise.all(orderDetailsArr);
  const orderRes = await fetch(
    process.env.REACT_APP_FETCH_URL + "/api/order",
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        customerId: customer._id,
        orderDetails: orderDetail,
        cost: totalPrice,
      }),
    },
  );
  const orderResult = await orderRes.json();
  const customerFetch = await fetch(
    process.env.REACT_APP_FETCH_URL + "/api/customer",
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        userId: customer.userId,
        fullName: customer.fullName,
        phone: customer.phone,
        email: customer.email,
        orders: orderResult.order._id
      }),
    },
  ).then((data) => {
    localStorage.clear()
    setMessageError("")
    setMessageSuccess("Create successfully")
    setOpenSnack(true)
  })
    .catch((error) => {
      setMessageError(error)
      setMessageSuccess("")
      setOpenSnack(true)
    })

};



