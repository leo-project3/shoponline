import { ProfileComponent } from "../Component/Body/Profile/profileComponent";
import Footer from "../Component/Footer/footer";
import Header from "../Component/Header/header";

export const Profile = () => {
  return (
    <>
      <Header />
      <ProfileComponent />
      <Footer />
    </>
  );
};
