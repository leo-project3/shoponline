const mongoose = require("mongoose");
const imageModel = require("../model/imageModel");
const multer = require("multer")
const fs = require("fs")

const storage = multer.diskStorage({
  destination: "upload",
  filename: (req, file, cb) => {
    cb(null, file.originalname);
  },
})

const upload = multer({
  storage: storage,
  fileFilter: async (req, file, cb) => {
    const whitelist = [
      'image/png',
      'image/jpeg',
      'image/jpg'
    ]
    const response = await imageModel.findOne({ name: req.body.name })
    if (response) {
      response.name == req.body.name && cb(new Error("Duplicate Product Name"))
    } else {
      cb(null, true)
    }
    if (!whitelist.includes(file.mimetype)) {
      cb(new Error("Only PNG, JPEG, JPG is allowed"))
    }
  },
  limits: {
    fileSize: 2048000
  }
}).array('files', 5)

const getImage = async (req, res) => {
  try {
    const result = await imageModel.find()
    return res.status(200).json({
      message: "Get images successfully",
      image: result
    })

  } catch (error) {
    return res.status(500).json({
      message: "Error" + err.message
    })
  }
}

const createImage = (req, res) => {

  upload(req, res, async (err) => {
    if (err) {
      if (err.code == "LIMIT_FILE_SIZE") {
        return res.status(400).json({
          message: "File size must be less than 2MB"
        })
      }
      if (err.code == "LIMIT_UNEXPECTED_FILE") {
        return res.status(400).json({
          message: "Only allow 5 images"
        })
      }
      return res.status(400).json({
        message: err.message
      })
    } else {

      try {

        if (req.files.length == 0) {
          return res.status(400).json({
            message: "No file has been found"
          })
        }
        const response = await imageModel.findOne({ name: req.body.name })
        if (!response) {
          const newImage = await imageModel.create({
            name: req.body.name,
            image: req.files.map((file) => file.filename)
          })
          return res.status(201).json({
            message: "Create successfully",
            img: newImage
          })

        } else {
          return res.status(400).json({
            message: "Duplicate Image"
          })
        }

      } catch (error) {
        return res.status(500).json({
          message: "Error" + error.message
        })
      }

    }
  })
}


/* const {
  url,
  description
} = req.body
console.log(req.body)
if(url == ""){
  return res.status(400).json({
    message: "Url need to be filled"
  })
}
try {
  const newImage = await imageModel.create({
    _id: new mongoose.Types.ObjectId,
    url: url,
    description: description
  })
  return res.status(201).json({
    message: "Create Image",
    image: newImage
  })
} catch (error) {
  return res.status(500).json({
    message: "Error" + error.message
  })
} */

module.exports = {
  getImage,
  createImage
} 