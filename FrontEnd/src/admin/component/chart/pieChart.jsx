import React, { useEffect, useState } from "react";
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from "chart.js";
import { Pie } from "react-chartjs-2";
import axios from "axios";

ChartJS.register(ArcElement, Tooltip, Legend);

export const PieChart = () => {
  const [customer, setCustomer] = useState([]);

  const getCustomer = async () => {
    const response = await axios(
      process.env.REACT_APP_FETCH_URL + "/api/customer",
    );
    const data = await response.data.Customer;
    const loyalCustomer = data.filter(
      (customer) => customer.orders.length != 0,
    );
    const mostOrderCustomer = loyalCustomer
      .map((customer) => {
        const order = customer.orders.filter((or) => or.status == 'Confirmed')
        customer.orders.length = order.length
        return customer
      });
    setCustomer(mostOrderCustomer.slice(0, 4));
  };
  useEffect(() => {
    getCustomer();
  }, []);
  const data = {
    labels: customer.map((cus) => cus.email),
    datasets: [
      {
        label: "Number of Orders",
        data: customer.map((cus) => cus.orders.length),
        backgroundColor: [
          "rgba(255, 99, 132, 0.2)",
          "rgba(54, 162, 235, 0.2)",
          "rgba(255, 206, 86, 0.2)",
          "rgba(75, 192, 192, 0.2)",
          "rgba(153, 102, 255, 0.2)",
          "rgba(255, 159, 64, 0.2)",
        ],
        borderColor: [
          "rgba(255, 99, 132, 1)",
          "rgba(54, 162, 235, 1)",
          "rgba(255, 206, 86, 1)",
          "rgba(75, 192, 192, 1)",
          "rgba(153, 102, 255, 1)",
          "rgba(255, 159, 64, 1)",
        ],
        borderWidth: 1,
        hoverOffset: 4,
      },
    ],
  };

  const options = {
    responsive: true,
    aspectRatio: 2,
    maintainAspectRatio: false,
    plugins: {
      legend: {
        position: "right",
        align: "center",
        labels: {
          textAlign: "left",
          padding: 10,
          boxWidth: 20,
        },
        title: {
          display: false,
        },
      },
    },
  };

  return <Pie data={data} options={options} />;
};
