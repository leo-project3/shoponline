import {
  Grid,
  Typography,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableBody,
  TableCell,
  Paper,
  Container,
  TableFooter,
} from "@mui/material";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";

export const CheckOutPreview = ({ shippingFee, totalPrice }) => {
  const cart = JSON.parse(localStorage.getItem("cart"));
  const navigate = useNavigate();

  useEffect(() => {
    if (cart === null || totalPrice === null) {
      navigate("/");
    }
  });

  //Product card for sm screen (>600px)
  const card =
    cart &&
    cart.map((product, index) => {
      return (
        <TableRow key={product._id} sx={{ display: { xs: "none", sm: "table-row" } }}>
          <TableCell sx={{ display: "flex", alignItems: "center" }}>
            <img
              src={process.env.REACT_APP_FETCH_URL + "/upload/" + product.imageUrl.image[0]}
              alt="product-img"
              style={{ width: "150px", height: "120px" }}
            />
            <Typography sx={{ marginLeft: "50px" }}>{product.name}</Typography>
          </TableCell>
          <TableCell align="center" sx={{ color: "red" }}>
            <b>{product.promotionPrice.toLocaleString()} VND</b>
          </TableCell>
          <TableCell align="center">{product.quantity}</TableCell>
          {/* <TableCell align="center" sx={{ color: "" }}>
            <b>
              {(product.promotionPrice * product.quantity).toLocaleString()} VND
            </b>
          </TableCell> */}
        </TableRow>
      );
    });
  //Product card for xs screen (<600px)
  const smallCard =
    cart &&
    cart.map((product, index) => {
      return (
        <TableRow key={product._id} sx={{ display: { xs: "flex", sm: "none" }, justifyContent: "start" }} >
          <TableCell sx={{ display: "flex", alignItems: "center", justifyContent: "start" }}>
            <img
              src={process.env.REACT_APP_FETCH_URL + "/upload/" + product.imageUrl.image[0]}
              alt="product-img"
              style={{ width: "95px", height: "80px" }}
            />
          </TableCell>
          <TableCell sx={{ width: "100%" }}>
            <Typography sx={{ display: "flex", flexDirection: "column", alignItems: "start", justifyContent: "center" }}>{product.name}</Typography>
            <Container sx={{ display: "flex", justifyContent: "space-between", alignItems: "center", padding: 0, margin: 0, width: "100%", minWidth: '155px' }}>
              <Typography variant="body" component='div'>Qty: {product.quantity}</Typography>

              <Typography variant="body2" component='div'><b>{product.promotionPrice.toLocaleString()} VND</b></Typography>
            </Container>
          </TableCell>

          {/* <TableCell align="center" sx={{ color: "" }}>
            <b>
              {(product.promotionPrice * product.quantity).toLocaleString()} VND
            </b>
          </TableCell> */}
        </TableRow>
      );
    });
  return (
    <>
      {" "}
      <Grid
        item
        sx={{
          display: "flex",
          flexDirection: "Column",
          backgroundColor: "white",
          width: "100%",
          maxWidth: { sm: "600px", lg: "750px" },
          minWidth: "300px",
          maxHeight: "550px",
          borderRadius: "20px",
          padding: "10px",
        }}
      >
        <Typography align="center" variant="h5" component="div">
          Product Preview
        </Typography>

        <TableContainer
          component={Paper}
          className="container bg-white p-4 rounded-4"
          sx={{ width: '100%', overflowX: 'hidden' }}
        >
          <Table>
            <TableHead sx={{ display: { xs: "none", sm: "table-header-group" } }}>
              <TableRow>
                <TableCell>Product</TableCell>
                <TableCell align="center">Price</TableCell>
                <TableCell align="center">Quantity</TableCell>
                {/*   <TableCell align="center">Total</TableCell> */}
              </TableRow>
            </TableHead>
            <TableBody>
              {card}
              {smallCard}
            </TableBody>
            <TableFooter sx={{ display: { xs: "flex", sm: "table-row-group" }, flexDirection: "column" }}>
              {shippingFee != 0 ? (
                <TableRow>
                  <TableCell>Shipping Fee:</TableCell>
                  <TableCell sx={{ color: "Red", fontWeight: "Bold" }}>
                    {shippingFee} VND
                  </TableCell>
                </TableRow>
              ) : null}
              <TableRow>
                <TableCell>Total Price:</TableCell>
                <TableCell sx={{ color: "Red", fontWeight: "Bold" }}>
                  {totalPrice != null && totalPrice} VND
                </TableCell>
              </TableRow>
            </TableFooter>
          </Table>
        </TableContainer>
      </Grid>
    </>
  );
};
