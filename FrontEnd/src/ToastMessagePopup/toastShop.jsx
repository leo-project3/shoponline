import * as React from "react";
import Button from "@mui/material/Button";
import Snackbar from "@mui/material/Snackbar";
import Alert from "@mui/material/Alert";
import { useSelector } from "react-redux";


//Toast handling error, success message from BE
export const ShopToast = ({ messageError, messageSuccess, open, setOpen }) => {
  const handleClick = () => {
    setOpen(true);
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  return (
    <div>
      <Snackbar
        open={open}
        autoHideDuration={6000}
        onClose={() => setOpen(false)}
        anchorOrigin={{ vertical: "top", horizontal: "right" }}
      >
        <Alert
          onClose={handleClose}
          severity={messageSuccess ? "success" : "error"}
          sx={{ width: "100%" }}
        >
          {messageError && messageError}
          {messageSuccess && messageSuccess}
        </Alert>
      </Snackbar>
    </div>
  );
};
