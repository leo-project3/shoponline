import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import {
  Grid,
  InputLabel,
  TextField,
  Select,
  MenuItem,
  Button,
  Typography,
} from "@mui/material";
import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { createProduct } from "../../action/createAction";
import { InputImage } from "./inputImage";
import { ShopToast } from "../../../ToastMessagePopup/toastShop";
const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 650,
  borderRadius: "10px",
  bgcolor: "background.paper",
  border: "1px solid #000",
  boxShadow: 24,
  p: 4,
};

export const CreateProductModal = ({
  setOpenToast,
  openCreateModal,
  setOpenCreateModal,
  setMessageError,
  setMessageSuccess
}) => {
  const dispatch = useDispatch();
  const [success, setSuccess] = useState(false);
  const [data, setData] = useState([]);
  const [name, setName] = useState();
  const [type, setType] = useState();
  const [description, setDescript] = useState();
  const [price, setPrice] = useState(0);
  const [promoPrice, setPromoPrice] = useState(0);
  const [stock, setStock] = useState(0);
  const [image, setImage] = useState("");
  const [error, setError] = useState({});

  useEffect(() => {
    fetch(process.env.REACT_APP_FETCH_URL + "/api/producttype")
      .then((res) => res.json())
      .then((data) => setData(data.productType));
  }, []);
  const typeI = data.map((item) => item.name);
  const filterItem = typeI.filter(
    (item, index) => typeI.indexOf(item) === index,
  );
  const typeItem = typeI.map((item, index) => {
    return (
      <MenuItem key={item} value={item}>
        {item}
      </MenuItem>
    );
  });

  const validateError = {};
  const validateInput = () => {
    if (!name) {
      validateError.name = "Product Name is required";
    }
    if (!type) {
      validateError.type = "Product Type is required";
    }
    if (price <= 0 && !isNaN(price)) {
      validateError.price = "Origin Price must greater than 0";
    }
    if (promoPrice <= 0 && !isNaN(promoPrice)) {
      validateError.promoPrice = "Promo Price must greater than 0";
    } else if (parseInt(promoPrice) > parseInt(price)) {
      validateError.promoPrice = "Promo Price must be less than Origin Price";
    }
    /*  if(!image){
      validateError.image = "Image is required"
    } else if(!/\.(jpg|jpeg|png|webp|avif|gif|svg)$/.test(image)){
      validateError.image = "Image is invalid"
    } */
    setError(validateError);
  };
  const onBtnCreateClick = () => {
    validateInput();
    const product = {
      name: name,
      description: description,
      type: type,
      imageUrl: image,
      buyPrice: price,
      promotionPrice: promoPrice,
      amount: stock,
    };

    if (Object.keys(validateError).length === 0) {
      dispatch(createProduct(product, setOpenToast, setMessageSuccess, setMessageError));
      setOpenToast(true);
      setOpenCreateModal(false);
    }
  };
  return (
    <div>
      <Modal
        open={openCreateModal}
        onClose={() => setOpenCreateModal(false)}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography variant="h4" mb={5} align="center">
            Create New Product
          </Typography>
          <Grid container>
            <Grid item sm={4}>
              <InputLabel>Name</InputLabel>
            </Grid>
            <Grid item sm={8}>
              <TextField
                value={name || ""}
                fullWidth
                onChange={(e) => setName(e.target.value)}
              />
              {error.name && <span style={{ color: "red" }}>{error.name}</span>}
            </Grid>
          </Grid>
          <Grid container sx={{ mt: 2 }}>
            <Grid item sm={4}>
              <InputLabel>Image</InputLabel>
            </Grid>
            <Grid item sm={8}>
              <InputImage product={{ name: name }} error={error} image={image} setImage={setImage} />
            </Grid>
          </Grid>
          <Grid container sx={{ mt: 2 }}>
            <Grid item sm={4}>
              <InputLabel>Type</InputLabel>
            </Grid>
            <Grid item sm={8}>
              <Select
                value={type || ""}
                fullWidth
                onChange={(e) => setType(e.target.value)}
              >
                {typeItem}
              </Select>
              {error.type && <span style={{ color: "red" }}>{error.type}</span>}
            </Grid>
          </Grid>
          <Grid container sx={{ mt: 2 }}>
            <Grid item sm={4}>
              <InputLabel>Origin Price</InputLabel>
            </Grid>
            <Grid item sm={8}>
              <TextField
                type="number"
                value={price || ""}
                fullWidth
                onChange={(e) => setPrice(e.target.value)}
              />
              {error.price && (
                <span style={{ color: "red" }}>{error.price}</span>
              )}
            </Grid>
          </Grid>
          <Grid container sx={{ mt: 2 }}>
            <Grid item sm={4}>
              <InputLabel>Promotion Price</InputLabel>
            </Grid>
            <Grid item sm={8}>
              <TextField
                type="number"
                value={promoPrice || ""}
                fullWidth
                onChange={(e) => setPromoPrice(e.target.value)}
              />
              {error.promoPrice && (
                <span style={{ color: "red" }}>{error.promoPrice}</span>
              )}
            </Grid>
          </Grid>
          <Grid container sx={{ mt: 2 }}>
            <Grid item sm={4}>
              <InputLabel>Stock</InputLabel>
            </Grid>
            <Grid item sm={8}>
              <TextField
                value={stock || ""}
                fullWidth
                onChange={(e) => setStock(e.target.value)}
              />
            </Grid>
          </Grid>
          <Grid container sx={{ mt: 2 }}>
            <Grid item sm={4}>
              <InputLabel>Description</InputLabel>
            </Grid>
            <Grid item sm={8}>
              <TextField
                multiline
                rows={5}
                value={description || ""}
                fullWidth
                onChange={(e) => setDescript(e.target.value)}
              />
            </Grid>
          </Grid>

          <Button
            variant="contained"
            color="secondary"
            sx={{ float: "right", marginTop: "10px" }}
            onClick={() => onBtnCreateClick()}
          >
            Create Product
          </Button>
        </Box>
      </Modal>

      {success ? (
        <Modal
          open={success}
          onClose={() => setSuccess(false)}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={style}>
            <Grid container>
              <Grid item sm={12}>
                <Typography variant="h4">Update Successfully</Typography>
              </Grid>
            </Grid>
          </Box>
        </Modal>
      ) : null}
    </div>
  );
};

export const UpdateOrderModal = ({
  openCreateModal,
  setOpenCreateModal,
  order,
}) => {
  const dispatch = useDispatch();
  const [success, setSuccess] = useState(false);
  const [status, setStatus] = useState();
  const onBtnChangeStatus = (e) => {
    setStatus(e.target.value);
  };

  return (
    <div>
      <Modal
        open={openCreateModal}
        onClose={() => setOpenCreateModal(false)}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Grid container>
            <Grid item sm={4}>
              <InputLabel>Order Code</InputLabel>
            </Grid>
            <Grid item sm={8}>
              <TextField disabled value={order.orderCode || ""} fullWidth />
            </Grid>
          </Grid>
          <Grid container sx={{ mt: 2 }}>
            <Grid item sm={4}>
              <InputLabel>Order Date</InputLabel>
            </Grid>
            <Grid item sm={8}>
              <TextField type="date" disabled value={order.orderDate || ""} fullWidth />
            </Grid>
          </Grid>
          <Grid container sx={{ mt: 2 }}>
            <Grid item sm={4}>
              <InputLabel>Estimate Shipping Date</InputLabel>
            </Grid>
            <Grid item sm={8}>
              <TextField type="date" disabled value={order.shippedDate || ""} fullWidth />
            </Grid>
          </Grid>
          <Grid container sx={{ mt: 2 }}>
            <Grid item sm={4}>
              <InputLabel>Total Price</InputLabel>
            </Grid>
            <Grid item sm={8}>
              <TextField type="number" disabled value={order.cost || ""} fullWidth />
            </Grid>
          </Grid>
          <Grid container sx={{ mt: 2 }}>
            <Grid item sm={4}>
              <InputLabel>Status</InputLabel>
            </Grid>
            <Grid item sm={8}>
              <Select
                disabled={order?.status == "Pending" ? false : true}
                value={status ? status : order.status}
                onChange={(e) => setStatus(e.target.value)}
                fullWidth
              >
                <MenuItem value="Pending">Pending</MenuItem>
                <MenuItem value="Canceled">Cancel</MenuItem>
                <MenuItem value="Confirmed">Confirmed</MenuItem>
              </Select>
            </Grid>
          </Grid>
          <Button
            variant="contained"
            color="success"
            sx={{ float: "right", marginTop: "10px" }}
          >
            Create Order Status
          </Button>
        </Box>
      </Modal>

      {success ? (
        <Modal
          open={success}
          onClose={() => setSuccess(false)}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={style}>
            <Grid container>
              <Grid item sm={12}>
                <Typography variant="h4">Create Successfully</Typography>
              </Grid>
            </Grid>
          </Box>
        </Modal>
      ) : null}
    </div>
  );
};
