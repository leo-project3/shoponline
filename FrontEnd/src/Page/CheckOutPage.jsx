import { CheckOut } from "../Component/Body/CheckOut/checkOutComponent";
import Footer from "../Component/Footer/footer";
import Header from "../Component/Header/header";

export const CheckOutPage = () => {
  return (
    <>
      <Header />
      <CheckOut />
      <Footer />
    </>
  );
};
