import {
  FETCH_CUSTOMER_ORDERS,
  FETCH_ORDER,
  FETCH_USER,
  ORDER_DETAILS,
} from "../contants/data";

const initialState = {
  orderDetail: [],
  order: "",
  orderCode: "",
  customer: {},
  orderArr: [],
};

//Control order when checkout button called
export const checkOutReducer = (state = initialState, action) => {
  switch (action.type) {
    case ORDER_DETAILS:
      state.orderDetail = action.payload;
      break;
    case FETCH_ORDER:
      state.order = action.payload.order._id;
      state.orderCode = action.payload.order.orderCode;
      break;
    case FETCH_CUSTOMER_ORDERS:
      state.orderArr = action.payload;
      break;
    case FETCH_USER:
      state.customer = action.payload;
      break;
    default:
      break;
  }
  return { ...state };
};
