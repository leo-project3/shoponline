export const PENDING = "PENDING";
export const GET_ORDER = "GET_ORDER";
export const ORDER_CHANGE_PAGE = "ORDER_CHANGE_PAGE";
export const GET_CUSTOMER = "GET_CUSTOMER";
export const GET_PRODUCT = "GET_PRODUCT";
export const CHANGE_LIMIT = "CHANGE_LIMIT";
export const DELETE = "DELETE";
export const UPDATE = "UPDATE";
export const CREATE = "CREATE";
export const GET_PRODUCT_DETAILS = "GET_PRODUCT_DETAILS";
export const GET_CATEGORY = "GET_CATEGORY";
