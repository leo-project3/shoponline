import {
  GET_DATA_SUCCESS,
  ERROR,
  PENDING,
  CHANGE_PAGE,
  CHANGE_PAGE_FILTER
} from "../contants/data";
import axios from "axios";

//Get all products
export const getDataItem = (page, limit) => {
  return async (dispatch) => {
    try {
      await dispatch({
        type: PENDING,
      });
      const skip = (page - 1) * limit;
      const limit2 = limit;
      const vQueryString = new URLSearchParams();
      vQueryString.append("limit", limit2);
      vQueryString.append("skip", skip);
      const response = await axios(
        process.env.REACT_APP_FETCH_URL +
        "/api/product?" +
        vQueryString.toString(),
      );
      const response2 = await axios(
        process.env.REACT_APP_FETCH_URL + "/api/product" + process.env.REACT_APP_LIMIT_PRODUCT,
      );

      const data = await response.data.products;
      const totalProduct = await response2.data.products;
      // const validProduct = await data.filter((product) => product.isDelete != true)
      const validProduct2 = await totalProduct.filter(
        (item) => item.isDelete != true,
      );
      return dispatch({
        type: GET_DATA_SUCCESS,
        totalPage: validProduct2.length,
        items: data,
      });
    } catch (error) {
      return dispatch({
        type: ERROR,
        error: error,
      });
    }
  };
};

export const PageChange = (page) => {
  return {
    type: CHANGE_PAGE,
    page: page,
  };
};
export const PageChangeFilter = (page) => {
  return {
    type: CHANGE_PAGE_FILTER,
    page: page,
  };
};
