import {
  Container,
  InputAdornment,
  IconButton,
  Grid,
  TextField,
  Typography,
  Button,
  Link,
  OutlinedInput,
} from "@mui/material";
import { Visibility, VisibilityOff } from "@mui/icons-material";
import { useState } from "react";
import { CreateAccount } from "./CreateAccountComponent";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { ShopToast } from "../../ToastMessagePopup/toastShop";

export const SignInForm = () => {
  const navigate = useNavigate();
  //Show password
  const [showPassword, setShowPassword] = useState(false);

  const handleClickShowPassword = () => setShowPassword((show) => !show);
  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };
  //Open create account Modal
  const [open, setOpen] = useState(false);
  const [openToast, setOpenToast] = useState(false);

  const onBtnCreateAccount = () => {
    setOpen(true);
  };

  //Error Alert
  const [error, setError] = useState({});

  //Get input value
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [messageError, setMessageError] = useState("");
  const [messageSuccess, setMessageSuccess] = useState("");
  //Login API and validate input data
  const onBtnLogin = (e) => {
    const validateError = {};
    //validate Input
    if (!username.trim()) {
      validateError.username = "Username is required!";
    }
    if (!password.trim()) {
      validateError.password = "Password is required!";
    } else if (password.length < 8) {
      validateError.password = "Password must be a least 8 character";
    }
    setError(validateError);

    if (Object.keys(validateError).length === 0) {
      const userObj = {
        username: username,
        password: password,
      };
      axios
        .post(process.env.REACT_APP_FETCH_URL + "/api/signin", userObj)
        .then((response) => {
          sessionStorage.setItem("User", response.data.user);
          sessionStorage.setItem("AccessToken", response.data.accessToken);
          sessionStorage.setItem("RefreshToken", response.data.refreshToken);
          window.location.href = "/";
        })
        .catch((err) => {
          setMessageError(err.response.data.message);
          setOpenToast(true);
        });
    }
  };
  const onBtnKeyDown = (e) => {
    if (e.keyCode == 13) {
      onBtnLogin();
    }
  };
  return (
    <>
      <Container
        maxWidth="xs"
        sx={{
          height: "100vh",
          width: "100%",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
        onKeyDown={(e) => onBtnKeyDown(e)}
      >
        <Grid
          container
          spacing={1}
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: "#FFF",
            padding: "40px",
            borderRadius: "8px",
          }}
        >
          <Typography variant="h4" component="div" sx={{ fontWeight: "bold" }}>
            Login
          </Typography>
          <Grid item xs={12}>
            <TextField
              fullWidth
              label="Username"
              variant="outlined"
              width="100%"
              id="username"
              name="username"
              onChange={(e) => setUsername(e.target.value)}
            />
          </Grid>
          {error.username && (
            <span style={{ color: "red" }}>{error.username}</span>
          )}
          <Grid item xs={12}>
            <OutlinedInput
              fullWidth
              variant="outlined"
              id="password"
              name="password"
              type={showPassword ? "text" : "password"}
              placeholder="Password"
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                    onMouseDown={handleMouseDownPassword}
                    edge="end"
                  >
                    {showPassword ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              }
              onChange={(e) => setPassword(e.target.value)}
            />
          </Grid>
          {error.password && (
            <span style={{ color: "red" }}>{error.password}</span>
          )}
          <Grid item xs={12} mb={1}>
            <Button
              variant="contained"
              size="large"
              fullWidth
              onClick={(e) => onBtnLogin(e)}
            >
              Log In
            </Button>
          </Grid>
          <Link component="div" color="#1877f2" underline="none">
            Forgot password?
          </Link>
          <Grid item xs={12} mt={2} sx={{ textAlign: "center" }}>
            <Button
              variant="contained"
              color="success"
              onClick={() => onBtnCreateAccount()}
            >
              Create New Account
            </Button>
          </Grid>
        </Grid>
        <CreateAccount open={open} setOpen={setOpen} />
      </Container>
      <ShopToast
        messageError={messageError}
        setOpen={setOpenToast}
        open={openToast}
      />
    </>
  );
};
