const express = require("express")
const cors = require('cors')
const mongoose = require('mongoose')

require("dotenv").config();
const app = express()
const port = process.env.SERVER_PORT

app.use("/upload", express.static('upload'))
app.use(express.json())


mongoose.connect(process.env.MONGODB_URL)
        .then(() => {
          console.log("Connect to database successfully")
        })
        .catch((err) => console.log(err))


app.use("/api/", cors(), require("./app/route/signRouter"))
app.use("/api/", cors(), require("./app/route/productRouter"))
app.use("/api/", cors(), require("./app/route/productTypeRouter"))
app.use("/api/", cors(), require("./app/route/orderDetailsRouter"))
app.use("/api/", cors(), require("./app/route/orderRouter"))
app.use("/api/", cors(), require("./app/route/customerRouter"))
app.use("/api/", cors(), require('./app/route/imageRouter'))
app.use("/api/", cors(), require('./app/route/contactUsRouter'))
app.listen(port, () => {
  console.log("App with listening on", port)
})