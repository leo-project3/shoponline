import { ResponsiveLine } from "@nivo/line";
import { useTheme } from "@mui/material";
import { tokens } from "../../../theme";
import { Line } from "react-chartjs-2";
import { Chart as ChartJS } from 'chart.js/auto'
import { Chart } from 'react-chartjs-2'
const LineChart = ({ data }) => {
  const theme = useTheme();
  const colors = tokens(theme.palette.mode);

  const options = {
    responsive: true,
    aspectRatio: 2,
    maintainAspectRatio: false,
  };
  return (
    <div>
      <Line width="100vw" options={options} data={data} />
    </div>
  );
};

export default LineChart;
