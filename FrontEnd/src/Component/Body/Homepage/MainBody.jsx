import SeeAll from "./SeeAllProduct";
import Product from "./LastestProductBody";
import CarouselBody from "./CarouselBody";
import { Banner } from "./bannerBody";
import { ContactUs } from "./contactUsBody";
const Body = () => {
  return (
    <>
      <div className="container bg-light body-margin rounded-5">
        <CarouselBody />
      </div>
      <div className="container mt-4 bg-secondary p-4 rounded-4" id="banner">
        <Banner />
      </div>
      <div className="container mt-4 bg-white p-4 rounded-4" id="Products">
        <div className="text-center">
          <h4 className="text-dark">
            <b>LATEST PRODUCT</b>
          </h4>
          <Product />
        </div>
        <SeeAll />
      </div>
      <div className="container mt-4 bg-white p-4 rounded-4" id="contact-us">
        <ContactUs />
      </div>
    </>
  );
};

export default Body;
