import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Col } from "reactstrap";
import {
  faFacebook,
  faInstagram,
  faTwitter,
  faYoutube,
} from "@fortawesome/free-brands-svg-icons";
import { Grid } from "@mui/material";

const IconFooter = () => {
  return (
    <Grid item md={3}>
      <h3>
        <b>DevCamp</b>
      </h3>
      <FontAwesomeIcon className="px-2" icon={faFacebook} />
      <FontAwesomeIcon className="px-2" icon={faInstagram} />
      <FontAwesomeIcon className="px-2" icon={faYoutube} />
      <FontAwesomeIcon className="px-2" icon={faTwitter} />
    </Grid>
  );
};

export default IconFooter;
