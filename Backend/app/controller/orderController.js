const orderModel = require("../model/orderModel")
const customerModel = require("../model/customerModel")
const mongoose = require("mongoose")
const orderDetailsModel = require("../model/orderDetailsModel")
const productModel = require("../model/productModel")

const createOrder = (req, res) => {
  /* const customerId = req.params.customerId
   if (!mongoose.Types.ObjectId.isValid(customerId)) {
     return res.status(400).json({
       message: "Id is not valid"
     })
   }*/
  const {
    customerId,
    shippedDate,
    note,
    orderDetails,
    status,
    cost,
    expires
  } = req.body

  var newOrder = new orderModel({
    _id: new mongoose.Types.ObjectId,
    customerId,
    shippedDate,
    note,
    orderDetails,
    status,
    cost,
    expires
  })
  orderModel.create(newOrder)
    .then((data) => {
      /* customerModel.findByIdAndUpdate(customerId, {
        $push: { orders: data._id }
        
       })
        .then((success) => {*/
      return res.status(201).json({
        message: "Create order successfully",
        order: data
      })
    })
    //})
    .catch((error) => {
      return res.status(500).json({
        message: "Error" + error.message
      })
    })
}

const getAllOrder = async (req, res) => {
  let condition = {}
  condition.isDelete = false
  try {
    const result = await orderModel.find(condition).populate({
      path: "orderDetails", populate: [{
        path: "product",
        populate: { path: "imageUrl" }
      }]
    })

    return res.status(200).json({
      message: "Get all orders",
      orders: result
    })
  } catch (error) {
    return res.status(500).json({
      message: "Error" + error.message
    })
  }
}

const getOrderById = async (req, res) => {
  const id = req.params.id
  if (!mongoose.Types.ObjectId) {
    return res.status(404).json({
      message: "Id is not valid"
    })
  }
  try {
    const result = await orderModel.findById(id).populate({
      path: "orderDetails", populate: [{
        path: "product",
        populate: { path: "imageUrl" }
      }]
    })
    if (result) {
      return res.status(200).json({
        message: `Get order id ${id} successfully`,
        order: result
      })
    } else {
      const response = await customerModel.findOneAndUpdate({ orders: { $in: id } }, {
        $pull: { orders: id }
      })
    }
  } catch (error) {
    return res.status(500).json({
      message: "Error" + error.message
    })
  }
}

const updateOrderById = async (req, res) => {
  const id = req.params.id
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(404).json({
      message: "Id is not valid"
    })
  }
  const {
    shippedDate,
    note,
    cost,
    status,
    isDelete
  } = req.body
  try {
    var updateOrder = {};
    if (shippedDate) {
      updateOrder.shippedDate = shippedDate
    }
    if (note) {
      updateOrder.note = note
    }
    if (cost) {
      updateOrder.cost = cost
    }
    if (status) {
      updateOrder.status = status
    }
    if (isDelete) {
      updateOrder.isDelete = isDelete
    }
    updateOrder.updateAt = Date.now()
    updateOrder.expires = null
    /* if(status == "Canceled"){
      const customer = await customerModel.findOneAndUpdate({orders: {$in: [id]}}, {
        $pull: {orders: id}
      })
    } */
    const result = await orderModel.findByIdAndUpdate(id, updateOrder)
    if (result) {
      if (status == "Canceled") {
        const order = await orderModel.findOne({ _id: id })
        let orderDetail = await order.orderDetails
        let orderDetails = await orderDetailsModel.find({ _id: { $in: orderDetail } })
        for (const product of orderDetails) {
          let productId = await productModel.findOneAndUpdate({ _id: product.product }, {
            $inc: { amount: (0 + product.quantity) }
          })
        }
      }
      return await res.status(200).json({
        message: `Update order ${result.orderCode} successfully`,
        order: result
      })
    } else {
      return res.status(400).json({
        message: "Id is not found"
      })
    }
  } catch (error) {
    return res.status(500).json({
      message: "Error" + error.message
    })
  }
}

const deleteOrderById = async (req, res) => {
  const id = req.params.id

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(404).json({
      message: "Id is not valid"
    })
  }
  /* if(!mongoose.Types.ObjectId.isValid(userId)){
     return res.status(404).json({
       message: "Id is not valid"
     })
   } */

  try {
    const result = await orderModel.findByIdAndUpdate(id, { isDelete: true })
    if (result) {
      await customerModel.findOneAndUpdate({ orders: id }, {
        $pull: { orders: id }
      })
      return res.status(200).json({
        message: `Delete order successfully`,
      })
    } else {
      return res.status(400).json({
        message: "No id found"
      })
    }
  } catch (error) {
    return res.status(500).json({
      message: "Error" + error.message
    })
  }
}
module.exports = {
  createOrder,
  getAllOrder,
  getOrderById,
  updateOrderById,
  deleteOrderById
}