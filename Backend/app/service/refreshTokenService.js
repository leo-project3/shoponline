const { v4: uuidv4 } = require("uuid")
const refreshTokenModel = require("../model/refreshToken")
require("dotenv").config();

const createToken = async (user) => {
  let expiredAt = new Date();
  expiredAt.getSeconds(
    expiredAt.getTime() + process.env.JWT_REFRESHTOKEN
  )
  let expired2 = expiredAt.setHours(expiredAt.getHours() + 2)
  console.log(expired2)
  let token = uuidv4();

  let refreshTokenObj = new refreshTokenModel({
    token: token,
    user: user._id,
    expiredDate: expired2
  })
 
  const refreshToken = await refreshTokenObj.save()
  console.log("refresh expired" + refreshToken.expiredDate)
  console.log("Issue at" + refreshToken.createdAt)
  return refreshToken.token
}

module.exports = {
  createToken
}