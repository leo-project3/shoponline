import {
  CircularProgress,
  Container,
  Grid,
  Box,
  Typography,
  TableCell,
  TableRow,
  TableContainer,
  Table,
  TableHead,
  TableBody,
  Button,
  Paper,
} from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { getCustomer } from "../../action/getOrderAction";
import { useEffect, useState } from "react";
import { DeleteCustomerModal } from "../modal/deleteModal";
import { SelectLimit } from "./selectLimit";
import {
  DataGrid,
  GridToolbar,
  GridActionsCellItem,
  GridRowModes,
  GridRowEditStopReasons,
} from "@mui/x-data-grid";
import { tokens } from "../../../theme";
import { useTheme } from "@mui/material";
import axios from "axios";
import Header from "../global/header";
import { useNavigate } from "react-router-dom";
import DeleteIcon from "@mui/icons-material/DeleteOutlined";
import SaveIcon from "@mui/icons-material/Save";
import CancelIcon from "@mui/icons-material/Close";
import EditIcon from "@mui/icons-material/Edit";
import CloseIcon from '@mui/icons-material/Close';
import DoneIcon from '@mui/icons-material/Done';
import { ShopToast } from "../../../ToastMessagePopup/toastShop"
import { UpdateCustomerModal } from "../modal/updateModal";
export const CustomerManagement = ({ onBtnChangePage }) => {
  const theme = useTheme();
  const colors = tokens(theme.palette.mode);

  const navigate = useNavigate();
  const { customers, pending, page, noPage, limit } = useSelector(
    (reduxData) => reduxData.tableReducer,
  );
  const [open, setOpen] = useState(false);
  const [messageError, setMessageError] = useState("")
  const [messageSuccess, setMessageSuccess] = useState("")
  const [customerData, setCustomerId] = useState("");
  const [paramCustomer, setPCustomer] = useState({});
  const [openToast, setOpenToast] = useState(false);
  const [orderId, setOrderId] = useState([]);
  const [rowModesModel, setRowModesModel] = useState({});
  const [openUpdateModal, setOpenUpdateModal] = useState(false)
  const [customerUpdate, setCustomerUpdate] = useState({})
  const [userDetails, setUserDetails] = useState({})
  const [columnVisibilityModel, setColumnVisibilityModel] = useState({
    _id: false,
    fullName: true,
    email: true,
    phone: true,
    order: true,
    address: true,
    city: true,
    district: true,
    ward: true,
  });

  const handleEditClick = (id) => () => {

    axios(process.env.REACT_APP_FETCH_URL + "/api/customer/" + id)
      .then((data) => setCustomerUpdate(data.data.Customer[0]))
      .then((data) => setOpenUpdateModal(true))
    // setRowModesModel({ ...rowModesModel, [id]: { mode: GridRowModes.Edit } });
  };
  const handleCancelClick = (id) => () => {
    setRowModesModel({
      ...rowModesModel,
      [id]: { mode: GridRowModes.View, ignoreModifications: true },
    });
  };

  const handleSaveClick = (id) => () => {
    setRowModesModel({ ...rowModesModel, [id]: { mode: GridRowModes.View } });
    //setOpenToast(true)
  };

  const handleDeleteClick = (id) => () => {
    setCustomerId(id);
    setOpen(true);
  };

  const processRowUpdate = (newRow) => {
    const updatedRow = { ...newRow, isNew: false };
    //handle send data to api
    return updatedRow;
  };

  let orderArr = [];

  const columns = [
    { field: "_id", headerName: "Id", width: 150, hide: true },
    { field: "fullName", headerName: "Full Name", flex: 1 },
    { field: "email", headerName: "Email", flex: 1 },
    { field: "phone", headerName: "Phone", flex: 1 },
    {
      field: "orders",
      headerName: "Order",
      flex: 1,
      renderCell: (params) => params.row.orders.length,
    },
    { field: "address", headerName: "Address", flex: 1 },
    { field: "city", headerName: "City", flex: 1 },
    { field: "district", headerName: "District", flex: 1 },
    { field: "ward", headerName: "Ward", flex: 1 },
    { field: 'userId', headerName: "isAdmin", align: 'center', headerAlign: 'center', flex: 1, renderCell: (params) => !params.row.userId ? null : params.row.userId.isAdmin ? <DoneIcon /> : <CloseIcon /> },
    {
      field: "actions",
      type: "actions",
      headerName: "Actions",
      flex: 1,
      cellClassName: "actions",
      getActions: ({ id }) => {
        const isInEditMode = rowModesModel[id]?.mode === GridRowModes.Edit;

        if (isInEditMode) {
          return [
            <GridActionsCellItem
              icon={<SaveIcon />}
              label="Save"
              color="inherit"
              onClick={handleSaveClick(id)}
            />,
            <GridActionsCellItem
              icon={<CancelIcon />}
              label="Cancel"
              className="textPrimary"
              onClick={handleCancelClick(id)}
              color="inherit"
            />,
          ];
        }

        return [
          <GridActionsCellItem
            icon={<EditIcon />}
            label="Edit"
            className="textPrimary"
            onClick={handleEditClick(id)}
            color="inherit"
          />,
          <GridActionsCellItem
            icon={<DeleteIcon />}
            label="Delete"
            onClick={handleDeleteClick(id)}
            color="inherit"
          />,
        ];
      },
    },
  ];
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getCustomer());
  }, []);

  const onBtnCellClick = (params) => {
    if (typeof params.formattedValue == "object") {
      try {
        params.formattedValue.map(async (order) => {
          const response = await axios(
            process.env.REACT_APP_FETCH_URL + "/api/order/" + order._id,
          );
          orderArr.push(response.data.order);
          setOrderId(orderArr);
        });
        if (orderId.length != 0) {
          navigate("/admin/order", {
            state: { userName: params.row.fullName, orders: orderId },
          });
        }
      } catch (error) { }
    }
  };
  return (
    <>
      {pending ? (
        <Grid item lg={12} md={12} sm={12} xs={12} textAlign="center">
          <CircularProgress color="secondary" />
        </Grid>
      ) : (
        <>
          <Box m="20px">
            <Header title="Customer" subtitle="Managing the Customers" />
            <Box
              m="40px 0 0 0"
              height="75vh"
              sx={{
                "& .MuiDataGrid-root": {
                  border: "none",
                },
                "& .MuiDataGrid-cell": {
                  borderBottom: "none",
                },
                "& .name-column--cell": {
                  color: colors.greenAccent[300],
                },
                "& .MuiDataGrid-columnHeaders": {
                  backgroundColor: colors.blueAccent[700],
                  borderBottom: "none",
                },
                "& .MuiDataGrid-virtualScroller": {
                  backgroundColor: colors.primary[400],
                },
                "& .MuiDataGrid-footerContainer": {
                  borderTop: "none",
                  backgroundColor: colors.blueAccent[700],
                },
                "& .MuiCheckbox-root": {
                  color: `${colors.greenAccent[200]} !important`,
                },
                "& .MuiDataGrid-toolbarContainer .MuiButton-text": {
                  color: `${colors.grey[100]} !important`,
                },
                "& .MuiTablePagination-displayedRows": {
                  margin: 0,
                },
                "& .MuiTablePagination-selectLabel": {
                  margin: 0,
                },
              }}
            >
              <DataGrid
                columnVisibilityModel={columnVisibilityModel}
                onColumnVisibilityModelChange={(newModel) => {
                  setColumnVisibilityModel(newModel);
                }}
                initialState={{
                  pagination: { paginationModel: { pageSize: 5 } },
                }}
                pageSizeOptions={[5, 10, 25]}
                onCellClick={onBtnCellClick}
                getRowId={(customer) => customer._id}
                rows={customers}
                columns={columns}
                slots={{ toolbar: GridToolbar }}
              />
            </Box>
          </Box>
          <DeleteCustomerModal
            open={open}
            setOpen={setOpen}
            customerId={customerData}
            setOpenToast={setOpenToast}
            setMessageError={setMessageError}
            setMessageSuccess={setMessageSuccess}
          />
          <ShopToast
            open={openToast}
            setOpen={setOpenToast}
            messageError={messageError}
            messageSuccess={messageSuccess}
          />
        </>
      )}
      <UpdateCustomerModal openUpdateModal={openUpdateModal} setOpenUpdateModal={setOpenUpdateModal} customer={customerUpdate} />
    </>
  );
};
{
  /*<Grid item xs={12} style={{textAlign:'center'}}>
          <Typography variant="h3">
            Customer Management
          </Typography>
          </Grid>
          <SelectLimit />
          <Grid item xs={12} mt={4}>
            <TableContainer component={Paper}>
              <Table
              sx={{minWidth: 650}}>
                <TableHead>
                  <TableRow>
                <TableCell>Fullname</TableCell>
                <TableCell>Phone</TableCell>
                <TableCell>Email</TableCell>
                <TableCell>Address</TableCell>
                <TableCell>City</TableCell>
                <TableCell>District</TableCell>
                <TableCell>Ward</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                {customers.map((customer, index) => {
                  return (
                    <TableRow key={index}>
                    <TableCell>{customer.fullName}</TableCell>
                  <TableCell>{customer.phone}</TableCell>
                  <TableCell>{customer.email}</TableCell>
                  <TableCell>{customer.address}</TableCell>
                  <TableCell>{customer.city}</TableCell>
                  <TableCell>{customer.district}</TableCell>
                  <TableCell>{customer.ward}</TableCell>
                  <TableCell>
                    <EditNoteIcon/>
                  
                    <DeleteIcon onClick={() => onBtnDeleteClick(index)}/>
                    <DeleteCustomerModal customer={paramCustomer} openModal={openModal} setOpenModal={setOpenModal}/>
                  </TableCell>
                    </TableRow>
                  )
                })}
              
                </TableBody>
              </Table>
            </TableContainer>
           <PaginateComponent onBtnChangePage={onBtnChangePage} page={page} noPage={noPage} />
          </Grid>*/
}
