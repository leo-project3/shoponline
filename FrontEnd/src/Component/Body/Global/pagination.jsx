import { Pagination } from "@mui/material";
import { PageChange, PageChangeFilter } from "../../../action/dataListAction";
import { useDispatch, useSelector } from "react-redux";
import { fetchFilterData } from "../../../action/filterAction";

//Pagination when get all products
export const PaginationProduct = () => {
  const dispatch = useDispatch();
  const { page, noPage } = useSelector((reduxData) => reduxData.dataReducer);
  const onBtnPageChange = (Event, value) => {
    dispatch(PageChange(value));
  };
  return (
    <>
      <Pagination
        count={noPage}
        page={page}
        onChange={onBtnPageChange}
      ></Pagination>
    </>
  );
};

// Pagination when get filtered products
export const PaginationFilter = () => {
  const dispatch = useDispatch();
  const { noPageFilter, pageFilter } = useSelector(
    (reduxData) => reduxData.dataReducer,
  );
  const onBtnPageChange = (Event, value) => {
    dispatch(PageChangeFilter(value));

  };
  return (
    <>
      <Pagination
        count={noPageFilter}
        page={pageFilter}
        onChange={onBtnPageChange}
      ></Pagination>
    </>
  );
};
