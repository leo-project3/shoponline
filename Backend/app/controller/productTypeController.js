
const mongoose = require("mongoose")
const productTypeModel = require("../model/productTypeModel")
const productModel = require("../model/productModel")

const createProductType = (req, res) => {
  //const  productId = req.params.productId
  const {
    name,
    description,
  } = req.body

  if (name == "") {
    res.status(400).json({
      message: "Name should be filled"
    })
  }
  /*if(!mongoose.Types.ObjectId.isValid(productId)){
    return res.status(400).json({
      message: "Course Id is not valid"
    })
  }*/
  var newProductType = new productTypeModel({
    _id: new mongoose.Types.ObjectId,
    name,
    description
  })

  productTypeModel.create(newProductType)
    .then((data) => {
      /* productModel.findByIdAndUpdate(productId, {
         type: data._id
       })
       
     .then((success) => {*/
      res.status(201).json({
        message: "Create successfully",
        productType: data
      })
    })

    .catch((error) => {
      res.status(500).json({
        message: "Error" + error.message
      })
    })
}

const getAllProductType = async (req, res) => {
  try {
    const result = await productTypeModel.find();
    return res.status(200).json({
      message: "Get productType successfully",
      productType: result
    })
  } catch (error) {
    return res.status(500).json({
      message: "Error"
    })
  }
}

const getProductTypeById = async (req, res) => {
  const id = req.params.id;
  if (!mongoose.Types.ObjectId.isValid(id)) {
    res.status(400).json({
      message: "Id is not valid"
    })
  }
  try {
    const result = await productTypeModel.findById(id);
    if (result) {
      return res.status(200).json({
        message: "Get product type by id successfully",
        productType: result
      })
    } else {
      return res.status(400).json({
        message: "Not found id"
      })
    }
  } catch (error) {
    return res.status(500).json({
      message: "Error"
    })
  }
}

const updateProductTypeById = async (req, res) => {
  const id = req.params.id
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).json({
      message: "Id is not valid"
    })
  }
  const {
    name,
    description
  } = req.body

  if (name == "") {
    res.status(400).json({
      message: "Name cant be blank"
    })
  }
  try {
    var updateProductType = {};
    if (name) {
      updateProductType.name = name
    }
    if (description) {
      updateProductType.description = description
    }
    const result = await productTypeModel.findByIdAndUpdate(id, updateProductType)
    if (result) {
      return res.status(200).json({
        message: "Update productType successful",
        productType: result
      })
    } else {
      return res.status(400).json({
        message: "productType Id can not found"
      })
    }
  } catch (error) {
    return res.status(500).json({
      message: "Error"
    })
  }

}

const deleteProductTypeById = async (req, res) => {
  const id = req.params.id
  // const productId = req.params.productId
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).json({
      message: "Id is not valid"
    })
  }
  /*if (!mongoose.Types.ObjectId.isValid(productId)) {
    return res.status(400).json({
      message: "Id is not valid"
    })
  }*/

  try {
    const result = await productTypeModel.findByIdAndDelete(id);
    if (result) {
      /* await productModel.findByIdAndUpdate(productId, {
         type: ""
       })*/
      return res.status(200).json({
        message: "Delete productType successfully"
      })
    } else {
      return res.status(404).json({
        message: "No productType id founded"
      })
    }
  } catch (error) {
    return res.status(500).json({
      message: "Error"
    })
  }
}
module.exports = {
  createProductType,
  getAllProductType,
  getProductTypeById,
  updateProductTypeById,
  deleteProductTypeById
}