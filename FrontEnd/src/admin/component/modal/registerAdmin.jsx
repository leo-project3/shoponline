import {
  Box,
  Modal,
  Typography,
  Button,
  Grid,
  TextField,
  IconButton,
  InputAdornment,
  OutlinedInput,
} from "@mui/material";
import { Visibility, VisibilityOff } from "@mui/icons-material";
import { useState } from "react";
import axios from "axios";
const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "1px solid #000",
  boxShadow: 24,
  p: 4,
  borderRadius: "8px",
};
export const CreateAdminAccount = ({ open, setOpen }) => {
  //Success Create Account
  const [success, setSucess] = useState(false);
  //Close modal when click outside of modal
  const onHandlerClose = () => {
    setOpen(false);
  };
  //Show password
  const [showPassword, setShowPassword] = useState(false);

  const handleClickShowPassword = () => setShowPassword((show) => !show);
  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };
  //Get value from Input
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [confirmpassword, setConfirmpassword] = useState("");

  //Error Alert
  const [error, setError] = useState({});

  //Button create new account
  const onBtnCreateNewAccount = () => {
    const validateError = {};

    //validate Input
    if (!username.trim()) {
      validateError.username = "Username is required!";
    }
    if (!password.trim()) {
      validateError.password = "Password is required!";
    } else if (password.length < 8) {
      validateError.password = "Password must be a least 8 character";
    }
    if (!confirmpassword) {
      validateError.confirmpassword = "Confirm Password is required";
    } else if (confirmpassword != password) {
      validateError.confirmpassword = "Confirm Password is not match";
    }
    setError(validateError);
    if (Object.keys(validateError).length === 0) {
      const userObj = {
        username: username,
        password: password,
        confirmpassword: confirmpassword,
      };

      axios
        .post(process.env.REACT_APP_FETCH_URL + "/api/registeradmin", userObj, {
          headers: {
            "x-access-token": sessionStorage.getItem("AccessToken"),
            "x-refresh-token": sessionStorage.getItem("RefreshToken"),
          },
        })
        .then((response) => {
          setSucess(true);
          setInterval(() => window.location.reload(), 1000);
        })
        .catch((err) => {
          setError({ error: err.response.data.message });
          if (err.response.data.message == "Unauthourized") {
            sessionStorage.clear();
            setTimeout(
              (window.location.href =
                process.env.REACT_APP_MAIN_URL + "/login"),
              20000,
            );
          }
        });
    }
  };
  return (
    <div>
      <Modal
        open={open}
        onClose={onHandlerClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        {success ? (
          <Box sx={style}>
            <Typography id="modal-modal-title" variant="h5" component="h2">
              Create Admin Account Successfully
            </Typography>
          </Box>
        ) : (
          <Box sx={style}>
            <Grid container spacing={2}>
              <Typography
                variant="h6"
                component="div"
                sx={{ fontWeight: "bold" }}
              >
                Create New Admin Account
              </Typography>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  variant="outlined"
                  placeholder="Username"
                  name="username"
                  onChange={(e) => setUsername(e.target.value)}
                />
                {error.username && (
                  <span style={{ color: "red" }}>{error.username}</span>
                )}
              </Grid>

              <Grid item xs={12}>
                <OutlinedInput
                  fullWidth
                  placeholder="Password"
                  name="password"
                  type={showPassword ? "text" : "password"}
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                        edge="end"
                      >
                        {showPassword ? <VisibilityOff /> : <Visibility />}
                      </IconButton>
                    </InputAdornment>
                  }
                  onChange={(e) => setPassword(e.target.value)}
                />
                {error.password && (
                  <span style={{ color: "red" }}>{error.password}</span>
                )}
              </Grid>

              <Grid item xs={12}>
                <TextField
                  fullWidth
                  variant="outlined"
                  type="password"
                  placeholder="Confirm Password"
                  name="confirmpassword"
                  onChange={(e) => setConfirmpassword(e.target.value)}
                />
                {error.confirmpassword && (
                  <span style={{ color: "red" }}>{error.confirmpassword}</span>
                )}
                {error.error && (
                  <span style={{ color: "red", textAlign: "center" }}>
                    {error.error}
                  </span>
                )}
              </Grid>

              <Grid item xs={12} sx={{ textAlign: "center" }}>
                <Button
                  variant="contained"
                  color="success"
                  size="large"
                  onClick={onBtnCreateNewAccount}
                >
                  Create Account
                </Button>
              </Grid>
            </Grid>
          </Box>
        )}
      </Modal>
    </div>
  );
};
