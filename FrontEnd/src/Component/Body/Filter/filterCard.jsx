import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import {
  Card,
  Grid,
  Typography,
  CardContent,
  CardMedia,
  Button,
  CircularProgress,
} from "@mui/material";

export const FilterCard = () => {
  const { item, pending } = useSelector((reduxData) => reduxData.filterReducer);
  //Product card
  const card = item.map((product, index) => (
    <Grid item md={4} lg={3} xs={12} sm={6} key={product._id} className="my-2">
      <Link
        to={`/products/${product._id}`}
        style={{
          textDecoration: "none",
        }}
        state={product.type}
      >
        <Card sx={{ maxWidth: 345, padding: "10px" }}>
          <CardMedia
            sx={{ height: 250 }}
            image={process.env.REACT_APP_FETCH_URL + "/upload/" + product.imageUrl.image[0]}
            title="product-img"
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="div">
              {product.name}
            </Typography>
            <Typography variant="body2" color="text.secondary">
              Price:{" "}
              <b style={{ color: "red" }}>
                {product.promotionPrice.toLocaleString() + "VND"}
              </b>
            </Typography>
          </CardContent>

          {/*<Button align="center" variant='contained' size="small">Add to Cart</Button>*/}
        </Card>
      </Link>
    </Grid>
  ));
  return (
    <>
      {pending ? (
        <Grid item lg={12} md={12} sm={12} xs={12} textAlign="center">
          <CircularProgress color="secondary" />
        </Grid>
      ) : (
        <Grid
          spacing={3}
          container
          align={"center"}
          sx={{ alignItems: "center", justifyContent: "center" }}
        >
          {card}
        </Grid>
      )}
    </>
  );
};
