import { Link } from "react-router-dom";

const SeeAll = () => {
  return (
    <div className="text-center mt-3">
      <Link to="/products">
        <button className="btn btn-dark">See All</button>
      </Link>
    </div>
  );
};

export default SeeAll;
