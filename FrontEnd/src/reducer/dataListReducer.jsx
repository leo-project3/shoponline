import { CHANGE_PAGE, FETCH_FILTER_DATA, GET_DATA_SUCCESS, PENDING, CHANGE_PAGE_FILTER, RESET_PAGE_FILTER } from "../contants/data";

const initialState = {
  page: 1,
  noPage: 0,
  limit: 8,
  pending: false,
  items: [],
  itemsFilter: [],
  noPageFilter: 0,
  pageFilter: 1
};

// Control all product, page, filter product.
export const dataReducer = (state = initialState, action) => {
  switch (action.type) {
    case PENDING:
      state.pending = true;
      break;
    case GET_DATA_SUCCESS:
      state.pending = false;
      state.noPage = Math.ceil(action.totalPage / state.limit);
      state.items = action.items;
      state.itemsFilter = []
      state.noPageFilter = 0
      break;
    case RESET_PAGE_FILTER:
      state.pageFilter = action.pageFilter
      break
    case FETCH_FILTER_DATA:
      state.pending = false;
      state.items = action.item
      state.itemsFilter = action.item
      state.noPageFilter = Math.ceil(action.totalFilterProduct / state.limit);
      break
    case CHANGE_PAGE:
      state.page = action.page;
      break;
    case CHANGE_PAGE_FILTER:
      state.pageFilter = action.page;
      break;
    default:
      break;
  }

  return { ...state };
};
