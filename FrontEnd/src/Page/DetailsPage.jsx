import Header from "../Component/Header/header";
import Footer from "../Component/Footer/footer";
import { BreadCrumbs2 } from "./BreadCrumbs";
import { useDispatch } from "react-redux";
import { DetailItem } from "../Component/Body/DetailsItem/detailsItemComponent";
import { useParams } from "react-router-dom";
import { useEffect } from "react";
import { fetchDetailProduct } from "../action/detailsItemAction";
import { RelatedProduct } from "../Component/Body/DetailsItem/RelatedProduct";
import { Description } from "../Component/Body/DetailsItem/productDescription";
export const DetailsProductPage = () => {
  const dispatch = useDispatch();
  const { id } = useParams();
  useEffect(() => {
    dispatch(fetchDetailProduct(id));
  }, []);
  return (
    <>
      <Header />
      <div
        style={{ marginTop: "100px" }}
        className="container bg-white p-4 rounded-4"
      >
        <BreadCrumbs2 />
        <DetailItem />
      </div>
      <div
        style={{ marginTop: "50px" }}
        className="container bg-white p-4 rounded-4"
      >
        <Description />
      </div>
      <div
        style={{ marginTop: "50px" }}
        className="container bg-white p-4 rounded-4"
      >
        <RelatedProduct />
      </div>
      <Footer />
    </>
  );
};
