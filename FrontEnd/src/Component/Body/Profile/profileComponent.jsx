import {
  Container,
  Grid,
  TextField,
  Typography,
  Button,
  Select,
  InputLabel,
} from "@mui/material";
import { ProfileInforComponent } from "./profileInformationComponent";
import { OrderSuccess } from "./orderSuccessProfile";

export const ProfileComponent = () => {
  return (
    <>
      <Container>
        <Grid
          container
          style={{ marginTop: "120px", }}
          spacing={{ xs: 0, lg: 4 }}
          sx={{
            display: "flex",
            alignItems: 'flex-start',
            justifyContent: { xs: "center", md: "space-evenly" },
            marginLeft: 0,
          }}
        >
          <OrderSuccess />
          <ProfileInforComponent />

        </Grid>
      </Container>
    </>
  );
};
