import { ERROR, ORDER_DETAILS, PENDING, FETCH_ORDER } from "../contants/data";
export const orderDetailFetch = (totalPrice) => {
  return async (dispatch) => {
    try {
      await dispatch({
        type: PENDING,
      });
      const cart = JSON.parse(localStorage.getItem("cart"));
      const customerId = sessionStorage.getItem("User");
      let orderDetailsArr = cart.map(async (product) => {
        return await fetch(
          process.env.REACT_APP_FETCH_URL + "/api/orderDetails",
          {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              productId: product._id,
              quantity: product.quantity,
            }),
          },
        ).then((response) =>
          response.json().then((data) => {
            return data.orderdetails._id;
          }),
        );
      });
      const orderDetail = await Promise.all(orderDetailsArr);
      dispatch({
        type: ORDER_DETAILS,
        payload: orderDetail,
      });
      const orderRes = await fetch(
        process.env.REACT_APP_FETCH_URL + "/api/order",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            customerId: customerId,
            orderDetails: orderDetail,
            cost: totalPrice,
          }),
        },
      );
      const orderResult = await orderRes.json();

      return dispatch({
        type: FETCH_ORDER,
        payload: orderResult,
      });
    } catch (error) {
      return dispatch({
        type: ERROR,
        error: error,
      });
    }
  };
};

export const orderFetch = (orderDetail) => {
  return async (dispatch) => {
    try {
      await dispatch({
        type: PENDING,
      });
      const response = await fetch(
        process.env.REACT_APP_FETCH_URL + "/api/order",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            orderDetails: orderDetail,
          }),
        },
      );
      const result = await response.json();
    } catch (error) {
      return dispatch({
        type: ERROR,
        error: error,
      });
    }
  };
};
