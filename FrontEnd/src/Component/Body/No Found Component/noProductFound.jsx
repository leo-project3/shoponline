import { Typography, Box } from "@mui/material"
import { Link } from "react-router-dom"

//When filter return no product, this component will be displayed
export const NoProductFound = () => {
  return (
    <>
      <Box sx={{ marginTop: "50px" }}>
        <Typography variant="h3" component='div' fontWeight='bold' color="error">
          404!!! No Product found
        </Typography>
      </Box>
    </>
  )
}