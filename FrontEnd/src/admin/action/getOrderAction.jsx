import axios from "axios";
import { ERROR } from "../../contants/data";
import {
  PENDING,
  GET_ORDER,
  ORDER_CHANGE_PAGE,
  GET_CUSTOMER,
  GET_PRODUCT,
  CHANGE_LIMIT,
  GET_PRODUCT_DETAILS,
  UPDATE,
  GET_CATEGORY,
} from "../contants/data";

export const getOrder = () => {
  return async (dispatch) => {
    try {
      await dispatch({
        type: PENDING,
      });
      const response2 = await axios(
        process.env.REACT_APP_FETCH_URL + "/api/order",
      );
      const data2 = await response2.data.orders;
      const validOrders = await data2.filter((cus) => cus.isDelete !== true);
      /*  const response = await Promise.all(data2.map( async (order) => {
        const response = await axios(process.env.REACT_APP_FETCH_URL + "/api/customer/" + order.customerId )
        return await response.data.Customer.fullName
      })) */
      //const data = await response.data.orders

      return dispatch({
        type: GET_ORDER,
        //payload: response,
        orderData: validOrders,
        totalPage: data2.length,
      });
    } catch (error) {
      return dispatch({
        type: ERROR,
      });
    }
  };
};

export const getCustomer = (page, limit) => {
  return async (dispatch) => {
    try {
      await dispatch({
        type: PENDING,
      });
      const skip = (page - 1) * limit;
      const limit2 = limit;
      const vQueryString = new URLSearchParams();
      vQueryString.append("limit", limit2);
      vQueryString.append("skip", skip);
      const response = await axios(
        process.env.REACT_APP_FETCH_URL + "/api/customer",
      );
      const response2 = await axios(
        process.env.REACT_APP_FETCH_URL + "/api/customer",
      );
      const data2 = await response2.data.Customer;
      const data = await response.data.Customer;
      const validCustomer = await data.filter((cus) => cus.isDelete !== true);
      return dispatch({
        type: GET_CUSTOMER,
        payload: validCustomer,
        totalPage: data2.length,
      });
    } catch (error) {
      return dispatch({
        type: ERROR,
      });
    }
  };
};

export const getProductId = (id) => {
  return async (dispatch) => {
    try {
      const response = await axios(
        process.env.REACT_APP_FETCH_URL + "/api/product/" + id,
      );
      const data = await response.data.product;

      return dispatch({
        type: GET_PRODUCT_DETAILS,
        payload: data,
      });
    } catch (error) {
      return dispatch({
        type: ERROR,
      });
    }
  };
};

export const getProduct = () => {
  return async (dispatch) => {
    try {
      await dispatch({
        type: PENDING,
      });

      const response = await axios(
        process.env.REACT_APP_FETCH_URL + "/api/product?",
      );
      const response2 = await axios(
        process.env.REACT_APP_FETCH_URL + "/api/product" + process.env.REACT_APP_LIMIT_PRODUCT,
      );
      const data2 = await response2.data.products;
      const data = await response.data.products;
      const validProduct = await data2.filter((cus) => cus.isDelete !== true);
      return dispatch({
        type: GET_PRODUCT,
        payload: validProduct,
        totalPage: data2.length,
      });
    } catch (error) {
      return dispatch({
        type: ERROR,
      });
    }
  };
};

export const getDetailsProduct = (orderCode, setOpenToast) => {

  return async (dispatch) => {
    try {
      const response = await axios(
        process.env.REACT_APP_FETCH_URL + "/api/order/" + orderCode,
      );
      const orderDetails = await response.data.order.orderDetails;

      const getorderDArr = await Promise.all(
        orderDetails.map(async (orderD) => {
          const response2 = await axios(
            process.env.REACT_APP_FETCH_URL + "/api/orderdetails/" + orderD._id,
          );
          return await response2.data.orderDetails;
        }),
      );
      const getAllProduct = await Promise.all(
        getorderDArr.map(async (product) => {
          const response = await axios(
            process.env.REACT_APP_FETCH_URL + "/api/product/" + product.product,
          );
          return await response.data.product;
        }),
      );
      /*  const product = await response2.data.orderDetails
      const response3 = await axios(process.env.REACT_APP_FETCH_URL + '/api/product/' + product.product)
      const productName = await response3.data  */
      dispatch({
        type: GET_PRODUCT_DETAILS,
        product: getAllProduct,
        orderDetails: getorderDArr,
      });
      dispatch({
        type: UPDATE,
      });
    } catch (error) {
      setOpenToast(true);
      dispatch({
        type: ERROR,
        error: error.response.data.message,
      });
    }
  };
};

export const getProductType = () => {
  return async (dispatch) => {
    try {
      const response = await axios(
        process.env.REACT_APP_FETCH_URL + "/api/producttype/",
      );
      const data = await response.data.productType;
      dispatch({
        type: GET_CATEGORY,
        category: data,
      });
    } catch (error) {
      dispatch({
        type: ERROR,
        error: error.response.data.message,
      });
    }
  };
};
export const PageChange = (page) => {
  return {
    type: ORDER_CHANGE_PAGE,
    page: page,
  };
};

export const LimitChange = (limit) => {
  return {
    type: CHANGE_LIMIT,
    limit: limit,
  };
};
